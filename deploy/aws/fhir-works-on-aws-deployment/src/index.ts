/*
 *  Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  SPDX-License-Identifier: Apache-2.0
 */

import serverless from 'serverless-http';
import { CorsOptions } from 'cors';
import { generateServerlessRouter } from 'fhir-works-on-aws-routing';
import { fhirConfig, genericResources } from './config';

const corsOptions: CorsOptions = {
    optionsSuccessStatus: 204,
    methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
    origin: '*',
    preflightContinue: false,
};

const serverlessHandler = serverless(generateServerlessRouter(fhirConfig, genericResources, corsOptions), {
    request(request: any, event: any) {
        request.user = event.user;
    },
});

export default async (event: any = {}, context: any = {}): Promise<any> => {
    return serverlessHandler(event, context);
};
