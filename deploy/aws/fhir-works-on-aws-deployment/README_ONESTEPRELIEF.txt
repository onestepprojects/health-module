********************************************************************************
ONESTEPRELIEF - HEALTH MODULE
FHIR Works on AWS - Customized Deployment
********************************************************************************

INTRODUCTION:
=============
1.  This repository is cloned from the original AWS project:
    https://github.com/awslabs/fhir-works-on-aws-deployment
2.  The default solutions template does not support CORS.
    To support CORS, we have to do some customization.
    Refer: https://github.com/awslabs/fhir-works-on-aws-deployment/blob/mainline/CUSTOMIZE.md#api-customization
    or: CUSTOMIZE.md file in this folder
    Read the 'CORS customization' section.


CHANGES:
========
1.  File: 'src/index.ts'
    Need to create a 'CorsOptions' object to support CORS and pass it in to the
    'generateServerlessRouter' method.
2.  File: 'serverless.yaml'
    Add the flag 'cors: true' to all HTTP endpoints.
3.  AWS API Gateway Console
    Select the API and enable CORS.
    Refer: https://docs.aws.amazon.com/apigateway/latest/developerguide/how-to-cors-console.html


DEPLOYMENT:
===========
1.  The easiest way to deploy is using docker.
    Follow the link:
    https://github.com/awslabs/fhir-works-on-aws-deployment/blob/mainline/INSTALL.md#docker-installation
2.  The docker container will build the projects and deploy the complete solution.

   



