# Cognito Setup

## **Health Module**
We have deployed [FHIR Works on AWS ](https://docs.aws.amazon.com/solutions/latest/fhir-works-on-aws/welcome.html) for the Health Module. The default 'FHIR Works on AWS' solutions currently supports 3 IAM groups:

![IAM Groups](./images/fhir_cognito_groups.png)

1. **auditor** - Someone who needs read, v_read and search access on patients. Precedence 2.
2. **non-practitioner** - This is a member of the hospital staff who needs access to non-medical record. Precedence 1.
3. **practitioner** - This is a member of the hospital staff, who directly helps patients. Precedence 0.

#### **To be done by Team 2**
1. Create the above mentioned IAM groups in the common Cognito user pool and assign sample 2. users to one of these groups.
2. Share the full ARN of the user pool with Team 4.

#### **To be done by Team 4**
1. Once the groups are created and users are added to the groups, the next step is to configure the cross-account Cognito Authorizer. The API Gateway for FHIR Works needs a new Authorizer. You'll need the **full ARN** of the user pool in common Cognito account. Refer the [official documentation](https://docs.aws.amazon.com/apigateway/latest/developerguide/apigateway-cross-account-cognito-authorizer.html) how to configure this.

![FHIR API Authorizer](./images/fhir_api_authorizer.png)

2. The **Health Project Management API** will use the same procedure by adding an Authorizer to use the common Cognito user pool. Cognito Groups who can access the Health Projects is to be defined by Team 2.

***

## **Solutions Module**

For the Solutions Module, we are using Gentics Mesh as our backend CMS. The roles who can access the Solutions needs to be defined by Team 2.

#### **To be done by Team 2**

1. Make sure that the user pool has the attributes as shown below. *NOTE:* A new user pool might need to be created if the attributes were not selected during the creation. The attributes are importent as they'll be used by Gentics Mesh.

![User pool attributes](./images/gmesh_userpool_attributes.png)

2. Create an App Client with the following configuration. Make sure the 'Generate client secret' option is not selected.

![User pool attributes](./images/gmesh_app_client.png)

3. Share the Cognito Domain name with Team 4.

![Cognito Domain Name](./images/cognito_domain_name.png)

4. In the App Client Settings section, use the following settings for the client created in Step 2.

![App Client Settings](./images/cognito_app_settings.png)

5. Get the public token from Cognito and copy the the contents to a file named **public-keys.json**. Provide the file to Team 4.
```shell
https://cognito-idp.{region}.amazonaws.com/{userPoolId}/.well-known/jwks.json
```

#### **To be done by Team 4**
1. The Roles that are defined in the common Cognito user pool needs to be created in the Gentics Mesh using admin portal.

![Gentics Mesh Roles](./images/gmesh_roles.png)

2. Create a new Docker image with the given **public-keys.json** and push it to ECR.


Now the users defined in Cognito will be able to access Gentics Mesh through the Solutions Module.


***

## **Survey Module**

SPRINT 1:

Precondition: Project Membership Required
- Owner (Create, Read, Update, Delete)
- Collector (Read, Update) <- can only update SurveyJSResponse
- Authenticated User (Read)

Precondition: Project Membership Not Required
Precondition: SurveyPublic = True AND SurveyStatus = Accepting
- Anyone (Read, Update) <- can only update SurveyJSResponse

FUTURE SPRINT:

Precondition: Project Membership Required
Precondition: Survey = Designated
- Designee (Read, Update) <- can only update SurveyJSDefinition
- Co-owner (Read, Update, Delete) <- can only update Description, update SurveyJSDefinition
