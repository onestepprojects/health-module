package org.onestep.relief.healthcare.dao;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import org.onestep.relief.healthcare.model.HealthcareProject;

import java.util.List;
import java.util.UUID;


public class HealthcareProjectDaoImpl implements HealthcareProjectDao {

    private AmazonDynamoDB client;
    private DynamoDBMapper mapper;
    private DynamoDBMapperConfig dynamoDBMapperConfig;

    public HealthcareProjectDaoImpl() {
        this.client = AmazonDynamoDBClientBuilder.standard().build();
        this.dynamoDBMapperConfig = new DynamoDBMapperConfig.Builder()
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES)
                .build();
        this.mapper = new DynamoDBMapper(this.client, this.dynamoDBMapperConfig);
    }

    public List<HealthcareProject> getProjects() {
        return this.mapper.scan(HealthcareProject.class, new DynamoDBScanExpression());
    }

    public HealthcareProject getProject(String id) {
        return this.mapper.load(HealthcareProject.class, id);
    }

    public void saveProject(HealthcareProject project) {
        this.mapper.save(project);
    }

    public void deleteProject(String id) {
        HealthcareProject project = this.getProject(id);
        if(project != null) {
            this.mapper.delete(project);
        }
    }
}
