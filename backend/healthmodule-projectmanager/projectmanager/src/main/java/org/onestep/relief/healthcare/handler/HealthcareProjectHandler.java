package org.onestep.relief.healthcare.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.onestep.relief.healthcare.dao.HealthcareProjectDao;
import org.onestep.relief.healthcare.dao.HealthcareProjectDaoImpl;
import org.onestep.relief.healthcare.model.FhirResourceInfo;
import org.onestep.relief.healthcare.model.HealthcareProject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
    API for handling Healthcare projects
 */
public class HealthcareProjectHandler {

    private HealthcareProjectDao projectDao;

    public HealthcareProjectDao getProjectDao() {
        if (this.projectDao == null) {
            this.projectDao = new HealthcareProjectDaoImpl();
        }
        return this.projectDao;
    }

    private Map<String, String> getDefaultHeaders() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("Access-Control-Allow-Origin", "*");
        return headers;
    }

    public void setProjectDao(HealthcareProjectDao projectDao) { this.projectDao = projectDao; };

    public APIGatewayProxyResponseEvent getProjects(APIGatewayProxyRequestEvent request, Context context) {
        Map<String, String> headers = getDefaultHeaders();
        String jsonInString = "";

        try {
            List<HealthcareProject> projects = this.getProjectDao().getProjects();
            ObjectMapper mapper = new ObjectMapper();
            jsonInString = mapper.writeValueAsString(projects);

        } catch(JsonProcessingException e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        }

        return new APIGatewayProxyResponseEvent()
                .withStatusCode(HttpStatus.SC_OK)
                .withHeaders(headers)
                .withBody(jsonInString);
    }

    public APIGatewayProxyResponseEvent createProject(APIGatewayProxyRequestEvent request, Context context) {
        Map<String, String> headers = getDefaultHeaders();
        String jsonInString = "";

        try {
            String body = request.getBody();
            ObjectMapper mapper = new ObjectMapper();
            HealthcareProject project;

            project = mapper.readValue(body, HealthcareProject.class);
            this.getProjectDao().saveProject(project);
            jsonInString = mapper.writeValueAsString(project);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        }

        return new APIGatewayProxyResponseEvent()
                .withStatusCode(HttpStatus.SC_CREATED)
                .withHeaders(headers)
                .withBody(jsonInString);
    }

    public APIGatewayProxyResponseEvent getProject(APIGatewayProxyRequestEvent request, Context context) {
        Map<String, String> headers = getDefaultHeaders();
        String jsonInString = "";

        try {
            String projectId = request.getPathParameters().get("id");
            HealthcareProject project = this.getProjectDao().getProject(projectId);

            if(project == null) {
                return new APIGatewayProxyResponseEvent()
                        .withStatusCode(HttpStatus.SC_NOT_FOUND)
                        .withHeaders(headers)
                        .withBody(String.format("Project with Id '%s' is not found!", project.getId()));
            }

            ObjectMapper mapper = new ObjectMapper();
            jsonInString = mapper.writeValueAsString(project);

        } catch(JsonProcessingException e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        }

        return new APIGatewayProxyResponseEvent()
                .withStatusCode(HttpStatus.SC_OK)
                .withHeaders(headers)
                .withBody(jsonInString);
    }

    public APIGatewayProxyResponseEvent updateProject(APIGatewayProxyRequestEvent request, Context context) {
        Map<String, String> headers = getDefaultHeaders();
        String jsonInString = "";

        try {
            String body = request.getBody();
            ObjectMapper mapper = new ObjectMapper();

            String projectId = request.getPathParameters().get("id");
            HealthcareProject projectToUpdate = this.getProjectDao().getProject(projectId);
            if(projectToUpdate == null) {
                return new APIGatewayProxyResponseEvent()
                        .withStatusCode(HttpStatus.SC_NOT_FOUND)
                        .withHeaders(headers)
                        .withBody(String.format("Project with Id '%s' is not found!", projectId));
            }

            HealthcareProject project = mapper.readValue(body, HealthcareProject.class);
            if (project.getId() == null) {
                project.setId(projectId);
            }

            this.getProjectDao().saveProject(project);
            jsonInString = String.format("Project with Id '%s' has been updated successfully!", project.getId());

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString() + e.getStackTrace().toString());
        }

        return new APIGatewayProxyResponseEvent()
                .withStatusCode(HttpStatus.SC_OK)
                .withHeaders(headers)
                .withBody(jsonInString);
    }

    public APIGatewayProxyResponseEvent deleteProject(APIGatewayProxyRequestEvent request, Context context) {
        Map<String, String> headers = getDefaultHeaders();
        String jsonInString = "";

        try {
            String projectId = request.getPathParameters().get("id");
            this.getProjectDao().deleteProject(projectId);
            jsonInString = String.format("Project with Id '%s' has been deleted successfully!", projectId);
        } catch (Exception e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        }

        return new APIGatewayProxyResponseEvent()
                .withStatusCode(HttpStatus.SC_OK)
                .withHeaders(headers)
                .withBody(jsonInString);
    }

    public APIGatewayProxyResponseEvent getProjectResources(APIGatewayProxyRequestEvent request, Context context) {
        Map<String, String> headers = getDefaultHeaders();
        String jsonInString = "";

        try {
            // get the project
            String projectId = request.getPathParameters().get("id");
            HealthcareProject project = this.getProjectDao().getProject(projectId);
            if(project == null) {
                return new APIGatewayProxyResponseEvent()
                        .withStatusCode(HttpStatus.SC_NOT_FOUND)
                        .withHeaders(headers)
                        .withBody(String.format("Project with Id '%s' is not found!", projectId));
            }

            // check for filter
            String resourceType = null;
            if (request.getQueryStringParameters() != null) {
                resourceType = request.getQueryStringParameters()
                        .getOrDefault("resourceType", "all");
            }

            // get resources
            ObjectMapper mapper = new ObjectMapper();
            jsonInString = mapper.writeValueAsString(project.getFhirResourcesByType(resourceType));

        } catch(JsonProcessingException e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        }

        return new APIGatewayProxyResponseEvent()
                .withStatusCode(HttpStatus.SC_OK)
                .withHeaders(headers)
                .withBody(jsonInString);
    }

    public APIGatewayProxyResponseEvent addProjectResource(APIGatewayProxyRequestEvent request, Context context) {
        Map<String, String> headers = getDefaultHeaders();
        String jsonInString = "";

        try {
            // get the project
            String projectId = request.getPathParameters().get("id");
            HealthcareProject project = this.getProjectDao().getProject(projectId);
            if(project == null) {
                return new APIGatewayProxyResponseEvent()
                        .withStatusCode(HttpStatus.SC_NOT_FOUND)
                        .withHeaders(headers)
                        .withBody(String.format("Project with Id '%s' is not found!", projectId));
            }

            // get the resource to be added
            String body = request.getBody();
            ObjectMapper mapper = new ObjectMapper();
            FhirResourceInfo resourceInfo = mapper.readValue(body, FhirResourceInfo.class);
            if (project.getFhirResources().contains(resourceInfo)) {
                return new APIGatewayProxyResponseEvent()
                        .withStatusCode(HttpStatus.SC_CONFLICT)
                        .withHeaders(headers)
                        .withBody("A resource with the given id and type already exists in the project");
            }

            // try to add the resource
            if (project.addResource(resourceInfo)) {
                this.getProjectDao().saveProject(project);
            }
            else {
                return new APIGatewayProxyResponseEvent()
                        .withStatusCode(HttpStatus.SC_CONFLICT)
                        .withHeaders(headers)
                        .withBody("Failed to add resource to the project");
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        }

        return new APIGatewayProxyResponseEvent()
                .withStatusCode(HttpStatus.SC_OK)
                .withHeaders(headers)
                .withBody(jsonInString);
    }

    public APIGatewayProxyResponseEvent removeProjectResource(APIGatewayProxyRequestEvent request, Context context) {
        Map<String, String> headers = getDefaultHeaders();
        String jsonInString = "";

        try {
            // get the project
            String projectId = request.getPathParameters().get("id");
            HealthcareProject project = this.getProjectDao().getProject(projectId);
            if(project == null) {
                return new APIGatewayProxyResponseEvent()
                        .withStatusCode(HttpStatus.SC_NOT_FOUND)
                        .withHeaders(headers)
                        .withBody(String.format("Project with Id '%s' is not found!", projectId));
            }

            // get the resource to be added
            String body = request.getBody();
            ObjectMapper mapper = new ObjectMapper();
            FhirResourceInfo resourceInfo = mapper.readValue(body, FhirResourceInfo.class);
            if (!project.getFhirResources().contains(resourceInfo)) {
                return new APIGatewayProxyResponseEvent()
                        .withStatusCode(HttpStatus.SC_CONFLICT)
                        .withHeaders(headers)
                        .withBody("The project does not contain the given resource.");
            }

            // try to add the resource
            if (project.removeResource(resourceInfo)) {
                this.getProjectDao().saveProject(project);
            }
            else {
                return new APIGatewayProxyResponseEvent()
                        .withStatusCode(HttpStatus.SC_CONFLICT)
                        .withHeaders(headers)
                        .withBody("Failed to add resource to the project");
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                    .withHeaders(headers)
                    .withBody(e.toString());
        }

        return new APIGatewayProxyResponseEvent()
                .withStatusCode(HttpStatus.SC_OK)
                .withHeaders(headers)
                .withBody(jsonInString);
    }
}
