package org.onestep.relief.healthcare.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;

import java.util.Objects;

@DynamoDBDocument
public class FhirResourceInfo {
    private String resourceId;
    private String resourceType;

    @DynamoDBAttribute(attributeName="resourceId")
    public String getResourceId() {
        return resourceId;
    }
    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    @DynamoDBAttribute(attributeName="resourceType")
    public String getResourceType() {
        return resourceType;
    }
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj == null || !(obj instanceof FhirResourceInfo)) {
            return false;
        }

        FhirResourceInfo otherResource = (FhirResourceInfo) obj;
        return (this.resourceId == otherResource.resourceId) &&
                (this.resourceType == otherResource.resourceType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resourceId, resourceType);
    }
}
