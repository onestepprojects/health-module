package org.onestep.relief.healthcare.dao;

import org.onestep.relief.healthcare.model.*;

import java.util.List;


public interface HealthcareProjectDao {

    public List<HealthcareProject> getProjects();

    public HealthcareProject getProject(String id);

    public void saveProject(HealthcareProject project);

    public void deleteProject(String id);

}
