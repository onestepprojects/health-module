process.env.TZ = 'UTC';

export default async (jestArgs) => {
  if (!global.__DYNALITE__) return;

  const watching = jestArgs.watch || jestArgs.watchAll;

  if (!watching) {
    await global.__DYNALITE__.close();
  }
};
