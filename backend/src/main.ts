import { NestFactory } from '@nestjs/core';
import { Logger, NestApplicationOptions } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AppModule } from './app.module';
import { EnvironmentVariables } from './types';

async function bootstrap() {
  const logger = new Logger('main');

  const domain = 'localhost';
  const options: NestApplicationOptions = {};
  if (process.env.NODE_ENV !== 'production') {
    const devcert = await import('devcert');
    if (devcert.hasCertificateFor(domain)) {
      options.httpsOptions = await devcert.certificateFor(domain);
    }
  }

  const app = await NestFactory.create(AppModule, options);

  const configService = app.get(ConfigService<EnvironmentVariables>);
  const port = configService.get('PORT', 3102, { infer: true });

  const hostname = '0.0.0.0';

  app.enableCors({ maxAge: 86400 });

  await app.listen(port, hostname, () => {
    const protocol = options.httpsOptions ? 'https' : 'http';
    const origin = `${protocol}://${protocol === 'https' ? domain : hostname}:${port}`;
    logger.log(`Server listening on ${origin}`);
  });
}

bootstrap();
