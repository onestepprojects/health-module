import { R4 } from '@ahryman40k/ts-fhir-types';

export { R4 } from '@ahryman40k/ts-fhir-types';

/**
 * A container for a collection of resources.
 *
 * @see https://www.hl7.org/fhir/bundle.html
 */
export interface IBundle<T = R4.IResourceList> extends R4.IBundle {
  entry?: (R4.IBundle_Entry & { resource?: T })[];
}

/**
 * Demographics and other administrative information about an individual or animal receiving care or other health-related services.
 *
 * @see https://www.hl7.org/fhir/patient.html
 */
export type IPatient = R4.IPatient;

/**
 * The Care Team includes all the people and organizations who plan to participate in the coordination and delivery of care for a patient.
 *
 * @see https://www.hl7.org/fhir/careteam.html
 */
export type ICareTeam = R4.ICareTeam;

/**
 * An interaction between a patient and healthcare provider(s) for the purpose of providing healthcare service(s) or assessing the health status of a patient.
 *
 * Also known as `Visit` on UI.
 *
 * @see https://www.hl7.org/fhir/encounter.html
 */
export type IEncounter = R4.IEncounter;

/**
 * This resource is primarily used for the identification and definition of a medication for the purposes of prescribing, dispensing, and administering a medication as well as for making statements about medication use.
 *
 * Also known as `Medication` on UI.
 *
 * @see https://www.hl7.org/fhir/medicationrequest.html
 * @see https://www.hl7.org/fhir/medicationstatement.html
 */
export type IMedicationRequest = R4.IMedicationRequest;
export type IMedicationStatement = R4.IMedicationStatement;

/**
 * Risk of harmful or undesirable, physiological response which is unique to an individual and associated with exposure to a substance.
 *
 * @see https://www.hl7.org/fhir/allergyintolerance.html
 */
export type IAllergyIntolerance = R4.IAllergyIntolerance;

/**
 * Describes the event of a patient being administered a vaccine or a record of an immunization as reported by a patient, a clinician or another party.
 *
 * @see https://www.hl7.org/fhir/immunization.html
 */
export type IImmunization = R4.IImmunization;

/**
 * Describes the intention of how one or more practitioners intend to deliver care for a particular patient, group or community for a period of time, possibly limited to care for a specific condition or set of conditions.
 *
 * @see https://www.hl7.org/fhir/careplan.html
 */
export type ICarePlan = R4.ICarePlan;

/**
 * Measurements and simple assertions made about a patient, device or other subject.
 *
 * Also known as `Medical History` on UI.
 *
 * @see https://www.hl7.org/fhir/observation.html
 */
export type IObservation = R4.IObservation;
