import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Logger,
} from '@nestjs/common';
import { Roles } from '../auth';
import {
  FindAllergyIntolerancesDto,
  FindCarePlansDto,
  FindEncountersDto,
  FindImmunizationsDto,
  FindMedicationRequestsDto,
  FindMedicationStatementsDto,
  FindObservationsDto,
  FindPatientsDto,
} from './dto';
import { FhirService } from './fhir.service';
import {
  IAllergyIntolerance,
  ICarePlan,
  IEncounter,
  IObservation,
  IPatient,
  IMedicationRequest,
  IMedicationStatement,
  IImmunization,
} from './interface';

@Controller('fhir')
@Roles('user')
export class FhirController {
  constructor(private fhirService: FhirService) {}

  private logger = new Logger(FhirController.name);

  @Get('patient')
  findPatients(@Query() query: FindPatientsDto) {
    return this.fhirService.findPatients(query);
  }

  @Get('patient/:id')
  findOnePatient(@Param('id') id: string) {
    return this.fhirService.findOnePatient(id);
  }

  @Post('patient')
  createPatient(@Body() data: IPatient) {
    return this.fhirService.createPatient(data)
  }

  @Put('patient/:id')
  updatePatient(@Param('id') id: string, @Body() patient: IPatient) {
    return this.fhirService.updatePatient({ ...patient, id });
  }

  @Get('encounter')
  findEncounters(@Query() query: FindEncountersDto) {
    return this.fhirService.findEncounters(query);
  }

  @Post('encounter')
  createEncounter(@Body() data: IEncounter) {
    return this.fhirService.createEncounter(data);
  }

  @Get('medicationrequest/:id')
  getMedicationRequest(@Param('id') id: string) {
    return this.fhirService.getMedicationRequest(id);
  }

  @Post('medicationrequest')
  createMedicationRequest(@Body() data: IMedicationRequest) {
    return this.fhirService.createMedicationRequest(data);
  }

  @Delete('medicationrequest/:id')
  deleteMedicationRequest(@Param('id') id: string) {
    return this.fhirService.deleteMedicationRequest(id);
  }

  @Get('medicationstatement')
  findMedicationStatements(@Query() query: FindMedicationStatementsDto) {
    return this.fhirService.findMedicationStatements(query);
  }

  @Post('medicationstatement')
  createMedicationStatement(@Body() data: IMedicationStatement) {
    return this.fhirService.createMedicationStatement(data);
  }

  @Delete('medicationstatement/:id')
  deleteMedicationStatement(@Param('id') id: string) {
    return this.fhirService.deleteMedicationStatement(id);
  }

  @Get('allergyintolerance')
  findAllergyIntolerances(@Query() query: FindAllergyIntolerancesDto) {
    return this.fhirService.findAllergyIntolerances(query);
  }

  @Post('allergyintolerance')
  createAllergyIntolerance(@Body() data: IAllergyIntolerance) {
    return this.fhirService.createAllergyIntolerance(data);
  }

  
  @Put('allergyintolerance/:id')
  updateAllergyIntolerance(@Param('id') id: string, @Body() data: IAllergyIntolerance) {
    return this.fhirService.updateAllergyIntolerance({ ...data, id });
  }

  @Delete('allergyintolerance/:id')
  deleteAllergyIntolerance(@Param('id') id: string) {
    return this.fhirService.deleteAllergyIntolerance(id);
  }

  @Get('immunization')
  findImmunizations(@Query() query: FindImmunizationsDto) {
    return this.fhirService.findImmunizations(query);
  }

  @Post('immunization')
  createImmunization(@Body() data: IImmunization) {
    return this.fhirService.createImmunization(data);
  }

  @Delete('immunization/:id')
  deleteImmunization(@Param('id') id: string) {
    return this.fhirService.deleteImmunization(id);
  }

  @Get('careplan')
  findCarePlans(@Query() query: FindCarePlansDto) {
    return this.fhirService.findCarePlans(query);
  }

  @Post('careplan')
  createCarePlan(@Body() data: ICarePlan) {
    return this.fhirService.createCarePlan(data);
  }

  @Put('careplan/:id')
  updateCarePlan(@Param('id') id: string, @Body() data: ICarePlan) {
    return this.fhirService.updateCarePlan({ ...data, id });
  }

  @Delete('careplan/:id')
  deleteCarePlan(@Param('id') id: string) {
    return this.fhirService.deleteCarePlan(id);
  }

  @Get('observation')
  findObservations(@Query() query: FindObservationsDto) {
    return this.fhirService.findObservations(query);
  }

  @Post('observation')
  createObservation(@Body() data: IObservation) {
    return this.fhirService.createObservation(data);
  }

  @Delete('observation/:id')
  deleteObservation(@Param('id') id: string) {
    return this.fhirService.deleteObservation(id);
  }
}
