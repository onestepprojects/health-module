import { IsInt, IsString } from 'class-validator';

export class FindEncountersDto {
  @IsInt()
  _count?: number;

  @IsString()
  subject?: string;
}
