import { IsInt, IsString } from 'class-validator';

export class FindImmunizationsDto {
  @IsInt()
  _count?: number;

  @IsString()
  subject?: string;
}
