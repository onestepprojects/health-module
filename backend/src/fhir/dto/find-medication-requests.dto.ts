import { IsInt, IsString } from 'class-validator';

export class FindMedicationRequestsDto {
  @IsInt()
  _count?: number;

  @IsString()
  id?: string;
}
