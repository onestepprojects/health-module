import { IsInt, IsString } from 'class-validator';

export class FindMedicationStatementsDto {
  @IsInt()
  _count?: number;

  @IsString()
  subject?: string;
}
