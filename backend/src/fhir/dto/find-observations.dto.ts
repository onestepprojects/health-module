import { IsInt, IsString } from 'class-validator';

export class FindObservationsDto {
  @IsInt()
  _count?: number;

  @IsString()
  subject?: string;
}
