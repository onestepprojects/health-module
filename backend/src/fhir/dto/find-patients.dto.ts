import { IsString } from 'class-validator';

export class FindPatientsDto {
  _count?: number;

  @IsString()
  name: string;
}
