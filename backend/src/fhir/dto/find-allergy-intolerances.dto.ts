import { IsInt, IsString } from 'class-validator';

export class FindAllergyIntolerancesDto {
  @IsInt()
  _count?: number;

  @IsString()
  subject?: string;
}
