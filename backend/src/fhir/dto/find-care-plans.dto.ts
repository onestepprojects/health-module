import { IsInt, IsString } from 'class-validator';

export class FindCarePlansDto {
  @IsInt()
  _count?: number;

  @IsString()
  subject?: string;
}
