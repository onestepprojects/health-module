import { URLSearchParams } from 'url';
import { Injectable, Logger } from '@nestjs/common';
import { HealthLakeHttpService } from '../aws';
import {
  FindAllergyIntolerancesDto,
  FindCarePlansDto,
  FindEncountersDto,
  FindImmunizationsDto,
  FindMedicationRequestsDto,
  FindMedicationStatementsDto,
  FindObservationsDto,
  FindPatientsDto,
} from './dto';
import {
  IAllergyIntolerance,
  IBundle,
  ICarePlan,
  IEncounter,
  IImmunization,
  IMedicationRequest,
  IMedicationStatement,
  IObservation,
  IPatient,
} from './interface';

@Injectable()
export class FhirService {
  constructor(private healthlakeHttpService: HealthLakeHttpService) {}

  private logger = new Logger(FhirService.name);

  createPatient(data: IPatient) {
    return this.healthlakeHttpService.post<IPatient>(
      `/Patient`,
      {
        ...data,
        resourceType: 'Patient',
      },
    );
  }

  findPatients(data: FindPatientsDto) {
    return this.healthlakeHttpService.get<IBundle<IPatient>>(
      `/Patient?${new URLSearchParams(data as any)}`,
    );
  }

  findOnePatient(id: string) {
    return this.healthlakeHttpService.get<IPatient>(`/Patient/${id}`);
  }

  updatePatient(patient: IPatient) {
    return this.healthlakeHttpService.put<IPatient>(
      `/Patient/${patient.id}`,
      patient,
    );
  }

  findEncounters(data: FindEncountersDto) {
    return this.healthlakeHttpService.get<IBundle<IEncounter>>(
      `/Encounter?${new URLSearchParams(data as any)}`,
    );
  }

  createEncounter(data: IEncounter) {
    return this.healthlakeHttpService.post<IEncounter>(`/Encounter`, {
      ...data,
      resourceType: 'Encounter',
    });
  }

  getMedicationRequest(id) {
    return this.healthlakeHttpService.get<IMedicationRequest>(
      `/MedicationRequest/${id}`,
    );
  }

  createMedicationRequest(data: IMedicationRequest) {
    return this.healthlakeHttpService.post<IMedicationRequest>(
      `/MedicationRequest`,
      {
        ...data,
        resourceType: 'MedicationRequest',
      },
    );
  }

  deleteMedicationRequest(id: string) {
    return this.healthlakeHttpService.delete<void>(`/MedicationRequest/${id}`);
  }

  findMedicationStatements(data: FindMedicationStatementsDto) {
    return this.healthlakeHttpService.get<IBundle<IMedicationStatement>>(
      `/MedicationStatement?${new URLSearchParams(data as any)}`,
    );
  }

  createMedicationStatement(data: IMedicationStatement) {
    return this.healthlakeHttpService.post<IMedicationStatement>(
      `/MedicationStatement`,
      {
        ...data,
        resourceType: 'MedicationStatement',
      },
    );
  }

  deleteMedicationStatement(id: string) {
    return this.healthlakeHttpService.delete<void>(
      `/MedicationStatement/${id}`,
    );
  }

  findAllergyIntolerances(data: FindAllergyIntolerancesDto) {
    return this.healthlakeHttpService.get<IBundle<IAllergyIntolerance>>(
      `/AllergyIntolerance?${new URLSearchParams(data as any)}`,
    );
  }

  createAllergyIntolerance(data: IAllergyIntolerance) {
    return this.healthlakeHttpService.post<IAllergyIntolerance>(
      `/AllergyIntolerance`,
      {
        ...data,
        resourceType: 'AllergyIntolerance',
      },
    );
  }

  
  updateAllergyIntolerance(data: IAllergyIntolerance) {
    return this.healthlakeHttpService.put<IAllergyIntolerance>(`/AllergyIntolerance/${data.id}`, {
      ...data,
      resourceType: 'AllergyIntolerance',
    });
  }

  deleteAllergyIntolerance(id: string) {
    return this.healthlakeHttpService.delete<void>(`/AllergyIntolerance/${id}`);
  }

  findImmunizations(data: FindImmunizationsDto) {
    return this.healthlakeHttpService.get<IBundle<IImmunization>>(
      `/Immunization?${new URLSearchParams(data as any)}`,
    );
  }

  createImmunization(data: IImmunization) {
    return this.healthlakeHttpService.post<IImmunization>(
      `/Immunization`,
      {
        ...data,
        resourceType: 'Immunization',
      },
    );
  }

  deleteImmunization(id: string) {
    return this.healthlakeHttpService.delete<void>(`/Immunization/${id}`);
  }

  findCarePlans(data: FindCarePlansDto) {
    return this.healthlakeHttpService.get<IBundle<ICarePlan>>(
      `/CarePlan?${new URLSearchParams(data as any)}`,
    );
  }

  createCarePlan(data: ICarePlan) {
    return this.healthlakeHttpService.post<ICarePlan>(`/CarePlan`, {
      ...data,
      resourceType: 'CarePlan',
    });
  }

  updateCarePlan(data: ICarePlan) {
    return this.healthlakeHttpService.put<ICarePlan>(`/CarePlan/${data.id}`, {
      ...data,
      resourceType: 'CarePlan',
    });
  }

  deleteCarePlan(id: string) {
    return this.healthlakeHttpService.delete<void>(`/CarePlan/${id}`);
  }

  findObservations(data: FindObservationsDto) {
    return this.healthlakeHttpService.get<IBundle<IObservation>>(
      `/Observation?${new URLSearchParams(data as any)}`,
    );
  }

  createObservation(data: IObservation) {
    return this.healthlakeHttpService.post<IObservation>(`/Observation`, {
      ...data,
      resourceType: 'Observation',
    });
  }

  deleteObservation(id: string) {
    return this.healthlakeHttpService.delete<void>(`/Observation/${id}`);
  }
}
