import { Module } from '@nestjs/common';
import { FhirController } from './fhir.controller';
import { FhirService } from './fhir.service';
import { HealthLakeHttpModule } from '../aws';

@Module({
  imports: [HealthLakeHttpModule.register()],
  controllers: [FhirController],
  providers: [FhirService],
})
export class FhirModule {}
