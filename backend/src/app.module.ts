import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard, AuthModule } from './auth';
import { AWSModule } from './aws';
import { FhirModule } from './fhir';
import { HealthcareProjectsModule } from './healthcare-projects';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ['.env.development.local', '.env.development', '.env'],
    }),
    HttpModule,
    AuthModule,
    AWSModule,
    FhirModule,
    HealthcareProjectsModule,
  ],
  providers: [{ provide: APP_GUARD, useClass: AuthGuard }],
})
export class AppModule {}
