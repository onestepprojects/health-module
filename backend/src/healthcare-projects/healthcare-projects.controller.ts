import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  Param,
  Patch,
  Post,
  Put,
} from '@nestjs/common';
import { HealthcareProjectEntity } from './healthcare-project.entity';
import { HealthcareProjectsService } from './healthcare-projects.service';

@Controller('projects')
export class HealthcareProjectsController {
  constructor(private healthcareProjectsService: HealthcareProjectsService) {}

  @Get()
  findAll() {
    return this.healthcareProjectsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.healthcareProjectsService.findOne(id);
  }

  /**
   * Called by organization service after creating a new health project. Only the project ID will be passed here.
   */
  @Post()
  create(
    @Body() { id }: HealthcareProjectEntity,
    /** User's bearer token */
    @Headers('Authorization') authorization: string,
  ) {
    return this.healthcareProjectsService.syncProject(id, authorization);
  }

  /**
   * Called by organization service after creating a new health project. Only the project ID will be passed here.
   */
  @Put(':id')
  replace(
    @Param('id') id: string,
    /** User's bearer token */
    @Headers('Authorization') authorization: string,
  ) {
    return this.healthcareProjectsService.syncProject(id, authorization);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateHealthcareProjectDto: HealthcareProjectEntity,
  ) {
    return this.healthcareProjectsService.update(
      id,
      updateHealthcareProjectDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.healthcareProjectsService.remove(id);
  }
}
