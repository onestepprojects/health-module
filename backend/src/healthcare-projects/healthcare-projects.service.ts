import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { DynamoDataMapper, asyncIteratorToArray } from '../aws/dynamo-db';
import { EnvironmentVariables } from '../types';
import { HealthcareProjectEntity } from './healthcare-project.entity';
import type { Project } from './interface/project.interface';

@Injectable()
export class HealthcareProjectsService {
  constructor(
    private configService: ConfigService<EnvironmentVariables>,
    private mapper: DynamoDataMapper,
    private httpService: HttpService,
  ) {}

  private logger = new Logger(HealthcareProjectsService.name);

  findAll() {
    return asyncIteratorToArray(this.mapper.scan(HealthcareProjectEntity));
  }

  findOne(id: string) {
    return this.mapper.get(HealthcareProjectEntity.new({ id }));
  }

  update(id: string, updateHealthcareProjectDto: HealthcareProjectEntity) {
    this.logger.log('update', id, updateHealthcareProjectDto);
    return this.mapper.update(
      HealthcareProjectEntity.new({ ...updateHealthcareProjectDto, id }),
    );
  }

  remove(id: string) {
    return this.mapper.delete(HealthcareProjectEntity.new({ id }));
  }

  async syncProject(projectId: string, authorization: string) {
    this.logger.log(`sync project ${projectId}`);

    const organizationProjectsUrl =
      this.configService.get(
        'SERVICE_BASE_URL',
        'https://staging.onestepprojects.org/onestep',
        { infer: true },
      ) +
      this.configService.get(
        'ORGANIZATION_PROJECTS_PATH',
        '/organization/projects',
        { infer: true },
      );
    const { data: project } = await this.httpService.axiosRef.get<Project>(
      `${organizationProjectsUrl}/${projectId}`,
      {
        headers: { Authorization: authorization },
      },
    );

    let healthcareProject = await this.mapper
      .get(HealthcareProjectEntity.new({ id: projectId }))
      .catch<HealthcareProjectEntity>(() => null);
    healthcareProject ??= HealthcareProjectEntity.new({ id: projectId });

    healthcareProject.name = project.name;
    healthcareProject.description = project.description;
    healthcareProject.startDate = new Date(Number(project.startDate));
    healthcareProject.endDate = new Date(Number(project.closeDate));
    healthcareProject.fhirResources ??= [];

    this.logger.debug('sync healthcare project', healthcareProject);

    return this.mapper.update(healthcareProject);
  }
}
