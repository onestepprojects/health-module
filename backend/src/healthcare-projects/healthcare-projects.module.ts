import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DynamoDataMapperModule } from '../aws/dynamo-db';
import { HealthcareProjectsController } from './healthcare-projects.controller';
import { HealthcareProjectsService } from './healthcare-projects.service';
import { HealthcareProjectEntity } from './healthcare-project.entity';

@Module({
  imports: [
    ConfigModule,
    HttpModule,
    DynamoDataMapperModule.forFeature([HealthcareProjectEntity]),
  ],
  controllers: [HealthcareProjectsController],
  providers: [HealthcareProjectsService],
})
export class HealthcareProjectsModule {}
