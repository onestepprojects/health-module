export interface Project {
  id: string;
  name: string;
  description: string;
  startDate: string;
  closeDate: string;
}
