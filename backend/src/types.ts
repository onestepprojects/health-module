export interface EnvironmentVariables {
  NODE_ENV: 'development' | 'test' | 'production';
  SERVER_ENV: 'development' | 'test' | 'staging' | 'core' | 'production';
  PORT: number;

  AWS_REGION: string;

  HEALTH_LAKE_ENDPOINT: string;

  SERVICE_CLIENT_ID: string;
  SERVICE_CLIENT_SECRET: string;

  SERVICE_BASE_URL: string;
  AUTHORIZATION_SERVICE_LOGIN_PATH: string;
  AUTHORIZATION_USER_PATH: string;
  ORGANIZATION_PROJECTS_PATH: string;
}
