import { HttpModuleOptions } from '@nestjs/axios';
import { Credentials, InterceptorOptions } from 'aws4-axios/dist/interceptor';

export interface HttpV4ModuleOptions
  extends HttpModuleOptions,
    Pick<InterceptorOptions, 'region' | 'service'> {
  credentials?: Credentials;
}
