import { HttpModule } from '@nestjs/axios';
import {
  DynamicModule,
  FactoryProvider,
  Module,
  ModuleMetadata,
} from '@nestjs/common';
import Axios from 'axios';
import {
  AXIOS_INSTANCE_TOKEN,
  HTTP_V4_MODULE_OPTIONS,
} from './http-v4.constants';
import { HttpV4ModuleOptions } from './http-v4.interface';
import { HttpV4Service } from './http-v4.service';

/**
 * A NestJS http v4 module with automatic AWS v4 request signing. This is useful for, among other things, IAM authorization in AWS API Gateway.
 *
 * @see https://docs.aws.amazon.com/general/latest/gr/signature-version-4.html
 */
@Module({})
export class HttpV4Module {
  static register(options = {} as HttpV4ModuleOptions): DynamicModule {
    return this.registerAsync({ useFactory: async () => options });
  }

  static registerAsync(
    config = {} as Omit<
      FactoryProvider<Promise<HttpV4ModuleOptions>>,
      'provide'
    > &
      Pick<ModuleMetadata, 'imports'>,
  ): DynamicModule {
    const { imports = [], inject, useFactory } = config;
    return {
      module: HttpV4Module,
      imports: [HttpModule, ...imports],
      providers: [
        HttpV4Service,
        {
          provide: AXIOS_INSTANCE_TOKEN,
          useFactory: (options: HttpV4ModuleOptions) => Axios.create(options),
          inject: [HTTP_V4_MODULE_OPTIONS],
        },
        {
          provide: HTTP_V4_MODULE_OPTIONS,
          useFactory,
          inject,
        },
      ],
      exports: [HttpV4Service],
    };
  }
}
