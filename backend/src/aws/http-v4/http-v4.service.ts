import { fromNodeProviderChain } from '@aws-sdk/credential-providers';
import { HttpService } from '@nestjs/axios';
import { HttpException, Inject, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { aws4Interceptor } from 'aws4-axios';
import Axios, { AxiosInstance } from 'axios';
import { EnvironmentVariables } from '../../types';
import {
  AXIOS_INSTANCE_TOKEN,
  HTTP_V4_MODULE_OPTIONS,
} from './http-v4.constants';
import { HttpV4ModuleOptions } from './http-v4.interface';

@Injectable()
export class HttpV4Service extends HttpService {
  constructor(
    @Inject(AXIOS_INSTANCE_TOKEN)
    protected instance: AxiosInstance,
    @Inject(HTTP_V4_MODULE_OPTIONS)
    protected options: HttpV4ModuleOptions,
    protected configService: ConfigService<EnvironmentVariables>,
  ) {
    super(instance);

    const {
      region = configService.get('AWS_REGION', 'us-east-2', {
        infer: true,
      }),
      service,
    } = options;

    const credentialProvider = fromNodeProviderChain({
      clientConfig: { region },
    });

    instance.interceptors.request.use(async (requestConfig) => {
      const credentials = await credentialProvider();
      const interceptor = aws4Interceptor({ region, service }, credentials);
      const config = await interceptor(requestConfig);

      this.logger.debug(
        `${config.method.toUpperCase()} ${config.baseURL}${config.url}${
          config.data ? '\n' + JSON.stringify(config.data) : ''
        }`,
      );

      return config;
    });

    instance.interceptors.response.use(
      ({ data }) => {
        /** NOTE: Don't leak sensitive data. e.g. config, headers. */
        return data;
      },
      (error) => {
        if (!Axios.isAxiosError(error)) {
          throw error;
        }
        throw new HttpException(
          {
            statusCode: error.response?.status || 500,
            message: error.message,
            data: error.response?.data,
          },
          error.response?.status || 500,
        );
      },
    );
  }

  private logger = new Logger(HttpV4Service.name);
}
