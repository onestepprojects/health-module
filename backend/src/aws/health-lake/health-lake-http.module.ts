import {
  DynamicModule,
  FactoryProvider,
  Module,
  ModuleMetadata,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Axios from 'axios';
import { EnvironmentVariables } from '../../types';
import {
  AXIOS_INSTANCE_TOKEN,
  HttpV4Module,
  HttpV4ModuleOptions,
  HTTP_V4_MODULE_OPTIONS,
} from '../http-v4';
import { HealthLakeHttpService } from './health-lake-http.service';

@Module({})
export class HealthLakeHttpModule {
  static register(options = {} as HttpV4ModuleOptions): DynamicModule {
    return this.registerAsync({ useFactory: async () => options });
  }

  static registerAsync(
    config = {} as Omit<
      FactoryProvider<Promise<HttpV4ModuleOptions>>,
      'provide'
    > &
      Pick<ModuleMetadata, 'imports'>,
  ): DynamicModule {
    const { imports = [], inject, useFactory } = config;
    return {
      module: HealthLakeHttpModule,
      imports: [HttpV4Module, ...imports],
      providers: [
        HealthLakeHttpService,
        {
          provide: AXIOS_INSTANCE_TOKEN,
          useFactory: (
            options: HttpV4ModuleOptions,
            configService: ConfigService<EnvironmentVariables>,
          ) => {
            options.baseURL ??= configService.get(
              'HEALTH_LAKE_ENDPOINT',
              'https://healthlake.us-east-2.amazonaws.com/datastore/94284b2cbb53f081daceb69beae76ef9/r4',
              { infer: true },
            );
            return Axios.create(options);
          },
          inject: [HTTP_V4_MODULE_OPTIONS, ConfigService],
        },
        {
          provide: HTTP_V4_MODULE_OPTIONS,
          useFactory,
          inject,
        },
      ],
      exports: [HealthLakeHttpService],
    };
  }
}
