import { Inject, Injectable } from '@nestjs/common';
import { AXIOS_INSTANCE_TOKEN } from '@nestjs/axios/dist/http.constants';
import { ConfigService } from '@nestjs/config';
import { AxiosInstance } from 'axios';
import { EnvironmentVariables } from '../../types';
import {
  HttpV4ModuleOptions,
  HttpV4Service,
  HTTP_V4_MODULE_OPTIONS,
} from '../http-v4';

@Injectable()
export class HealthLakeHttpService extends HttpV4Service {
  constructor(
    @Inject(AXIOS_INSTANCE_TOKEN)
    protected instance: AxiosInstance,
    @Inject(HTTP_V4_MODULE_OPTIONS)
    protected options: HttpV4ModuleOptions,
    protected configService: ConfigService<EnvironmentVariables>,
  ) {
    super(instance, options, configService);
  }
}
