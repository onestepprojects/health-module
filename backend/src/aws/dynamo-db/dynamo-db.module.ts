import http from 'http';
import { DynamicModule, FactoryProvider, Logger, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { DynamoDB } from 'aws-sdk';
import { EnvironmentVariables } from '../../types';

export { DynamoDB } from 'aws-sdk';

@Module({})
export class DynamoDBModule {
  private static logger = new Logger(DynamoDBModule.name);

  static forRoot() {
    return this.forRootAsync();
  }

  static forRootAsync(): DynamicModule {
    const asyncProvider: FactoryProvider<Promise<DynamoDB>> = {
      provide: DynamoDB,
      inject: [ConfigService],

      useFactory: async (
        configService: ConfigService<EnvironmentVariables>,
      ) => {
        const nodeEnv = configService.get('NODE_ENV', 'development', {
          infer: true,
        });

        if (nodeEnv === 'development') {
          // const path = './node_modules/.cache/dynalite';
          // await fs.promises.mkdir(path, { recursive: true });
          // const { endpoint } = await this.runDynaliteServer(4567, path);
          // return new DynamoDB({ endpoint, logger: this.logger });
          return new DynamoDB({
            logger: { log: this.logger.debug.bind(this.logger) },
          });
        }

        if (nodeEnv === 'test') {
          return new DynamoDB({ endpoint: 'http://localhost:7654' });
        }

        return new DynamoDB();
      },
    };

    return {
      global: true,
      module: DynamoDBModule,
      providers: [asyncProvider],
      exports: [asyncProvider],
    };
  }

  private static dynalitePromise: Promise<{
    server: http.Server;
    port: number;
    endpoint: string;
  }>;
  /**
   * @deprecated Use online dynamodb on us-east-2 instead.
   */
  private static runDynaliteServer(port: number, path?: string) {
    this.dynalitePromise ??= new Promise((resolve, reject) => {
      try {
        const dynalite = require('dynalite');
        const server: http.Server = dynalite({
          path,
          createTableMs: 0,
          updateTableMs: 0,
          deleteTableMs: 0,
        });
        server.listen(port, () => {
          const endpoint = `http://localhost:${port}`;
          this.logger.log(`Dynalite listening on ${endpoint}`);
          resolve({ server, port, endpoint });
        });
      } catch (err) {
        reject(err);
      }
    });

    return this.dynalitePromise;
  }
}
