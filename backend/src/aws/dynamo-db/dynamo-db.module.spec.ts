import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import { DynamoDBModule, DynamoDB } from './dynamo-db.module';

describe('DynamoDBModule', () => {
  let dynamodb: DynamoDB;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          envFilePath: ['.env.test.local', '.env.test', '.env'],
        }),
        DynamoDBModule.forRoot(),
      ],
    }).compile();

    dynamodb = app.get<DynamoDB>(DynamoDB);
  });

  it('gets dynamodb instance', () => {
    expect(dynamodb).toBeDefined();
    expect(dynamodb.endpoint.href).toBe('http://localhost:7654/');
  });
});
