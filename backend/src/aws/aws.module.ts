import { Module } from '@nestjs/common';
import { DynamoDataMapperModule } from './dynamo-db/dynamo-data-mapper.module';
import { DynamoDBModule } from './dynamo-db/dynamo-db.module';
import { HealthLakeHttpModule } from './health-lake/health-lake-http.module';

@Module({
  imports: [
    DynamoDataMapperModule.forRoot(),
    DynamoDBModule.forRoot(),
    HealthLakeHttpModule.register(),
  ],
})
export class AWSModule {}
