import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AuthGuard } from './auth.guard';
import { RolesGuard } from './roles.guard';

@Module({
  imports: [ConfigModule, HttpModule],
  providers: [AuthGuard, RolesGuard],
})
export class AuthModule {}
