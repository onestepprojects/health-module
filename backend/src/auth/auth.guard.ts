import { HttpService } from '@nestjs/axios';
import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
  Logger,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { IncomingMessage } from 'http';
import { EnvironmentVariables } from '../types';
import { User } from './interface/user.interface';

/**
 * Delegation authorization service to verifying the user's token in authorization header. If successful, assign user to request.
 */
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private configService: ConfigService<EnvironmentVariables>,
    private httpService: HttpService,
  ) {}

  private logger = new Logger(AuthGuard.name);

  private serviceBaseUrl = this.configService.get(
    'SERVICE_BASE_URL',
    'https://staging.onestepprojects.org/onestep',
    { infer: true },
  );

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest<IncomingMessage>();
    const authToken = request.headers.authorization;

    if (authToken?.startsWith('Bearer ')) {
      try {
        const user = await this.validateUser(authToken);
        this.logger.debug(`User ${user.userId}. Roles [${user.roles}].`);
        Object.assign(request, { user });
        return true;
      } catch (err) {
        this.logger.error('Authorization service error', err.response?.data);
      }
    }

    throw new UnauthorizedException();
  }

  async validateUser(authToken: string): Promise<User> {
    const authorizationUserUrl =
      this.serviceBaseUrl +
      this.configService.get('AUTHORIZATION_USER_PATH', '/authorization/user', {
        infer: true,
      });
    const response = await this.httpService.axiosRef.get(authorizationUserUrl, {
      headers: { Authorization: authToken },
    });

    return response.data.data;
  }

  async getServiceToken() {
    const authorizationServiceLoginUrl =
      this.serviceBaseUrl +
      this.configService.get(
        'AUTHORIZATION_SERVICE_LOGIN_PATH',
        '/authorization/servicelogin',
        { infer: true },
      );
    const clientId = this.configService.get('SERVICE_CLIENT_ID', {
      infer: true,
    });
    const clientSecret = this.configService.get('SERVICE_CLIENT_SECRET', {
      infer: true,
    });
    const credentials = Buffer.from(
      `${clientId}:${clientSecret}`,
      'utf8',
    ).toString('base64');
    const authToken = `Basic ${credentials}`;

    const response = await this.httpService.axiosRef.post<{
      access_token: string;
      expires_in: number;
      token_type: string;
    }>(authorizationServiceLoginUrl, undefined, {
      headers: {
        Authorization: authToken,
      },
    });

    return response.data;
  }
}
