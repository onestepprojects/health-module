import { Role } from './role.interface';

export interface User {
  userId: string;
  username: string;
  roles: Role[];
}
