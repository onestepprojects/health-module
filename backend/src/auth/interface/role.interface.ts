export type Role =
  | 'admin'
  | 'auditor' // Someone who needs read, v_read and search access on patients.
  | 'development'
  | 'non-practitioner' // This is a member of the hospital staff who needs access to non-medical record.
  | 'practitioner' // This is a member of the hospital staff, who directly helps patients.
  | 'user'
  | (string & { _: never });
