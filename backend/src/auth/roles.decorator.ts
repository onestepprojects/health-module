import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { Role } from './interface';
import { RolesGuard } from './roles.guard';

/**
 * Permits access only to users with a specific role
 *
 * @example
 *  //@Roles('admin', 'practitioner') Allow admin or practitioner.
 *  class AdminController {}
 */
export function Roles(...roles: Role[]) {
  return applyDecorators(SetMetadata('roles', roles), UseGuards(RolesGuard));
}
