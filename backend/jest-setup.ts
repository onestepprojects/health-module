import dynalite from 'dynalite';

process.env.TZ = 'UTC';

export default async () => {
  if (global.__DYNALITE__) return;

  /** @type {import('http').Server} */
  const server = (global.__DYNALITE__ = dynalite({
    createTableMs: 0,
    updateTableMs: 0,
    deleteTableMs: 0,
  }));

  return new Promise<void>((resolve) => {
    const port = 7654;
    server.listen(port, () => {
      console.log(`\nDynalite listening on http://localhost:${port}`);
      resolve();
    });
  });
};
