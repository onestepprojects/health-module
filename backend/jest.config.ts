import { InitialOptionsTsJest } from 'ts-jest';
import { jsWithTsESM } from 'ts-jest/presets';

const config: InitialOptionsTsJest = {
  globals: {
    'ts-jest': {
      diagnostics: false,
      isolatedModules: true,
    },
  },
  collectCoverageFrom: ['**/*.(t|j)s'],
  coverageDirectory: '<rootDir>/coverage',
  globalSetup: '<rootDir>/jest-setup.ts',
  globalTeardown: '<rootDir>/jest-teardown.ts',
  logHeapUsage: true,
  passWithNoTests: true,
  transform: {
    ...jsWithTsESM.transform,
  },
};

export default config;
