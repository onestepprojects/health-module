# Health Service

Base on [Nest](https://github.com/nestjs/nest) framework.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Environment Variables

```bash
PORT=3102 # development port
SERVER_ENV=development # fallback to NODE_ENV

AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
AWS_REGION=us-east-2

HEALTH_LAKE_ENDPOINT=https://healthlake.us-east-1.amazonaws.com/datastore/xxx/r4

SERVICE_CLIENT_ID # Cognito app client (Organization Module Service Account)
SERVICE_CLIENT_SECRET

SERVICE_BASE_URL=https://staging.onestepprojects.org/onestep
AUTHORIZATION_SERVICE_LOGIN_PATH=/authorization/servicelogin
AUTHORIZATION_USER_PATH=/authorization/user
ORGANIZATION_PROJECTS_PATH=/organization/projects

```
