# health-module

> OneStep Health Module

[![pipeline status](https://gitlab.com/onestepprojects/health-module/badges/main/pipeline.svg)](https://gitlab.com/onestepprojects/health-module/-/commits/main)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Development

IMPORTANT DISCLAIMER: This UI should not be used to collect and health information until the PHI/PII laws of the country
for which this application will be released have been thoroughly studied and the code modified to comply with.
As of this commit made by Haley Huang on April 27th, 2022, the Health Module is not suitable for use in collecting PHI
in any country and should not be used in its current state to collect any information on any user.

Before attempting to run or build the Health module, the .env file must be updated with valid URLs connecting to
backend services that maintain health projects and FHIR data. There are multiple TODOs commented in the code around
authentication against the backend. This should be updated to use the project auth instead of basic auth once a project
backend has been stood up.

### Developing with the app webpack dev server

Install dependencies and start module in watch mode.

```bash
# health-module/frontend
npm install
npm start
```

Link module and start app dev server.

```bash
# one-step-ui
npm install
npm link ../health-module/frontend
npm start
```

Open https://localhost:3000/ in your browser.

## Usage

```bash
npm install --save @onestepprojects/health-module
```

```jsx
import HeathModule from '@onestepprojects/health-module'
import '@onestepprojects/health-module/dist/index.css'

const Example = () => {
  return <HeathModule />
}
```

## Environment Variables

- REACT_APP_HEALTH_MODULE_API_URL

## License

MIT © [mdeforest](https://github.com/mdeforest)
