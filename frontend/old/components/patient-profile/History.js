import React, { useEffect, useState, useContext } from 'react'
import ReactPaginate from 'react-paginate'
import { Modal, Button, Spinner } from 'react-bootstrap'

import HistoryItem from './HistoryItem'
import { observationsByPatient } from '../../api/ObservationApi'
import { conditionsByPatient, createCondition } from '../../api/ConditionsApi'
import GenericSearch from '../common/GenericSearch'
import { addResourceToProject } from '../../api/ProjectApi'

import styles from './patient-profile.module.css'
import FhirContext from '../../context/FhirContext'

const History = ({ patient, auth, projectId }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [history, setHistory] = useState([])
  const [data, setData] = useState(null)
  const [pageCount, setPageCount] = useState(0)
  const [show, setShow] = useState(false)
  const [condition, setConditionData] = useState('')
  const [note, setNote] = useState('')
  const [loading, setLoading] = useState(false)

  const perPage = 8

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  const onConditionSelected = ({ suggestionIndex, allSuggestions }) => {
    setConditionData(allSuggestions[3][suggestionIndex][0])
  }

  const onConditionType = (value) => {
    setConditionData(value)
  }

  const createNewCondition = (e) => {
    e.preventDefault()

    async function create() {
      const result = await createCondition(
        auth,
        client,
        signal,
        { 'Content-Type': 'application/json' },
        { condition, note },
        patient.id
      )
      addResourceToProject({ resourceId: result.id, resourceType: 'Condition' }, projectId)
      let newHistory = history
      newHistory.push(result)
      setHistory(newHistory)
      setPageCount(Math.ceil(newHistory.length / perPage))
      setData(newHistory.slice(0, perPage))
    }
    create()
    handleClose()
  }

  useEffect(() => {
    async function gatherHistory(id) {
      if (!ready) return

      setLoading(true)
      let results = []
      const observations = await observationsByPatient(auth, client, signal, id, requestHeaders)
      results = results.concat(observations)
      const conditions = await conditionsByPatient(auth, client, signal, id, requestHeaders)
      results = results.concat(conditions)

      setHistory(results)
      setLoading(false)
    }

    gatherHistory(patient.id)

    return () => {
      abortController.abort()
    }
  }, [ready, patient.id])

  useEffect(() => {
    setPageCount(Math.ceil(history.length / perPage))
    setData(history.slice(0, perPage))
  }, [history])

  const onPageChange = ({ selected }) => {
    let offset = Math.ceil(selected * perPage)
    setData(history.slice(offset, offset + perPage))
  }

  const displayHistory = () => {
    if (loading)
      return (
        <div className='text-center'>
          <Spinner animation='border' variant='primary' />
        </div>
      )
    else if (data && data.length > 0) {
      return (
        <React.Fragment>
          <div className='d-flex flex-wrap'>
            {data.map((datum) => {
              return <HistoryItem key={datum.id} item={datum} />
            })}
          </div>

          <div className='d-flex justify-content-center mt-5'>
            <ReactPaginate
              pageCount={pageCount}
              pageRangeDisplayed={5}
              marginPagesDisplayed={5}
              previousLabel='Previous'
              nextLabel='Next'
              breakLabel='...'
              breakClassName='page-item'
              onPageChange={onPageChange}
              containerClassName='pagination'
              pageClassName='page-item'
              pageLinkClassName='page-link'
              activeClassName={styles.pageActive}
              previousClassName='page-item'
              nextClassName='page-item'
              previousLinkClassName='page-link'
              nextLinkClassName='page-link'
            />
          </div>
        </React.Fragment>
      )
    } else {
      return 'No Available History'
    }
  }

  return (
    <React.Fragment>
      {displayHistory()}
      <div className='d-flex flex-column align-items-end'>
        <div onClick={handleShow}>
          <div type='button' className={`btn-circle btn-primary ${styles.plus}`}>
            +
          </div>
        </div>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New Condition</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className='form-row'>
              <div className='form-group col'>
                <GenericSearch
                  placeholder='Search Conditions'
                  requestUrl='https://clinicaltables.nlm.nih.gov/api/conditions/v3/search?maxList=500'
                  onDataSelected={onConditionSelected}
                  onType={onConditionType}
                />
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col'>
                <input
                  type='text'
                  id='note'
                  placeholder='Additional notes'
                  className='form-control'
                  onChange={(e) => setNote(e.target.value)}
                />
              </div>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={handleClose}>
            Close
          </Button>
          <Button variant='primary' onClick={createNewCondition}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  )
}

export default History
