import React from 'react'
import _get from 'lodash/get'

import { dateInWords } from '../../utils'

import styles from './patient-profile.module.css'

const Allergy = ({ allergy }) => {
  return (
    <div className={`card mt-3 ${styles.allergy}`}>
      <div className='card-body d-flex flex-column align-items-start'>
        <h4 className='card-title'>{_get(allergy, 'code.text', 'Allergy')}</h4>
        <h6 className='card-subtitle mt-2'>{dateInWords(_get(allergy, 'recordedDate'), 'N/A')}</h6>
        <span className='mt-2'>{_get(allergy, 'reaction[0].manifestation[0].text')}</span>
      </div>
    </div>
  )
}

export default Allergy
