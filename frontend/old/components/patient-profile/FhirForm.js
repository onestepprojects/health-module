import React, { useEffect, useState } from 'react'
import { Modal } from 'react-bootstrap'

import * as Survey from 'survey-react'

import 'survey-react/survey.css'

const FhirForm = ({ form, show, setShow, auth, client, signal, id }) => {
  const [model, setModel] = useState(null)

  useEffect(() => {
    var data = form.createForm()
    var newModel = new Survey.Model(data)
    newModel.data = form.getFormData()[0]

    newModel.completedHtml = '<span></span>'

    setModel(newModel)
  }, [])

  const onComplete = () => {
    for (let item of model.getPlainData()) {
      form.resources[0].updateFieldValue(item.name, item.value)
    }

    form.submitResources(
      auth,
      client,
      signal,
      { 'Content-Type': 'application/json' },
      id
    )
  }

  return (
    <Modal size='lg' show={show} onHide={() => setShow(false)}>
      <Modal.Header>
        <Modal.Title>Form</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {model && (
          <Survey.Survey model={model} onComplete={onComplete}></Survey.Survey>
        )}
      </Modal.Body>
      <Modal.Footer>
        <button
          type='button'
          className='btn btn-secondary'
          onClick={() => setShow(false)}
        >
          Close
        </button>
      </Modal.Footer>
    </Modal>
  )
}

export default FhirForm
