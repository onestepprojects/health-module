import React, { useState, useEffect, useContext } from 'react'
import FhirContext from '../../context/FhirContext'
import { createPatient } from '../../api/PatientApi'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Form } from 'react-bootstrap'
import { Redirect } from 'react-router-dom'
import { addResourceToProject } from '../../api/ProjectApi'
import ProjectContext from '../../context/ProjectContext'
//import styles from './project-dashboard.module.css'

const Case = ({ basePath, projectId, match, history, auth }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)
  const projectContext = useContext(ProjectContext)
  const projectClient = projectContext.client
  const projectRequestHeaders = projectContext.requestHeaders
  const projectReady = projectContext.ready

  const [project, setProject] = useState('')
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [birthDate, setBirthDate] = useState('')
  const [gender, setGender] = useState('')
  const [language, setLanguage] = useState('')
  const [addressLine1, setAddress1] = useState('')
  const [addressLine2, setAddress2] = useState('')
  const [city, setCity] = useState('')
  const [state, setState] = useState('')
  const [country, setCountry] = useState('')
  const [zipCode, setZip] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [redirect, setRedirect] = useState(false)

  const submit = async () => {
    const patientData = {
      firstName: firstName,
      lastName: lastName,
      birthDate: birthDate,
      gender: gender,
      language: language,
      addressLine1: addressLine1,
      addressLine2: addressLine2,
      city: city,
      state: state,
      country: country,
      zipCode: zipCode,
      email: email,
      phone: phone,
    }

    const newPatient = await createPatient(auth, client, signal, requestHeaders, patientData)
    addResourceToProject({ resourceId: newPatient.id, resourceType: 'Patient' }, projectId)
    setRedirect(true)
  }

  if (redirect) {
    return <Redirect to={`${basePath}/project/${projectId}`} />
  }

  return (
    <div className='main-content-container container-fluid px-4 vh-100'>
      <div className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'>
        <div className='col-8 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Create Patient</span>
          <h3 className='page-title'>Patient Management</h3>
          <a href={`${basePath}/project/${projectId}`}>{'< '}Back to Project</a>
        </div>
      </div>
      <form>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>First Name</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <input
              type='text'
              id='firstname'
              name='firstname'
              placeholder='John'
              className='form-control'
              onChange={(e) => setFirstName(e.target.value)}
            />
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Last Name</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <input
              type='text'
              id='lastname'
              name='lastname'
              placeholder='Doe'
              className='form-control'
              onChange={(e) => setLastName(e.target.value)}
            />
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Birth Date</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <input
              type='date'
              name='birthdate'
              id='birthdate'
              className='form-control'
              onChange={(e) => setBirthDate(e.target.value)}
            />
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Sex</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <Form.Control
              as='select'
              placeholder='Select...'
              name='gender'
              id='gender'
              onChange={(e) => setGender(e.target.value)}
            >
              <option>Select...</option>
              <option value='female'>Female</option>
              <option value='male'>Male</option>
              <option value='other'>Other</option>
              <option value='unknown'>Unknown</option>
            </Form.Control>
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Language</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <Form.Control
              as='select'
              name='language'
              id='language'
              onChange={(e) => setLanguage(e.target.value)}
            >
              <option>Select...</option>
              <option value='english'>English</option>
              <option value='spanish'>Spanish</option>
              <option value='chinese'>Chinese</option>
            </Form.Control>
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Address Line 1</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <input
              type='text'
              id='addressline1'
              name='addressline1'
              placeholder='123 Apple Street'
              className='form-control'
              onChange={(e) => setAddress1(e.target.value)}
            />
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Address Line 2</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <input
              type='text'
              id='addressline2'
              name='addressline2'
              placeholder='Unit 1'
              className='form-control'
              onChange={(e) => setAddress2(e.target.value)}
            />
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>City</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <input
              type='text'
              id='city'
              name='city'
              placeholder='New York'
              className='form-control'
              onChange={(e) => setCity(e.target.value)}
            />
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>State/District/Province</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <input
              type='text'
              id='state'
              name='state'
              placeholder='New York'
              className='form-control'
              onChange={(e) => setState(e.target.value)}
            />
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Country</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <Form.Control
              as='select'
              name='country'
              id='country'
              onChange={(e) => setCountry(e.target.value)}
            >
              <option>Select...</option>
              <option value='USA'>USA</option>
              <option value='Brazil'>Brazil</option>
              <option value='Canada'>Canada</option>
            </Form.Control>
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Zip Code</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <input
              type='text'
              id='zip'
              name='zip'
              placeholder='123456'
              className='form-control'
              onChange={(e) => setZip(e.target.value)}
            />
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Phone Number</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <input
              type='text'
              id='phone'
              name='phone'
              placeholder='+1 555-555-5555'
              className='form-control'
              onChange={(e) => setPhone(e.target.value)}
            />
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Email</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <input
              type='text'
              id='email'
              name='email'
              placeholder='email@email.com'
              className='form-control'
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
        </div>
        <div className='row'>
          <button type='button' className='btn btn-lg btn-primary' onClick={submit}>
            Create Patient
          </button>
        </div>
      </form>
    </div>
  )
}

export default Case
