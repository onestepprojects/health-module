import React, { useState, useEffect, useContext } from 'react'
import _get from 'lodash/get'
import { dateInWords } from '../../utils'
import styles from './patient-profile.module.css'
import FhirContext from '../../context/FhirContext'
import { getPractitionerByID } from '../../api/PractitionerApi'

const CarePlanPractitioner = ({ id, auth }) => {
  const abortController = new AbortController()
  const signal = abortController.signal
  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [practitioner, setPractitioner] = useState({})

  useEffect(() => {
    async function getPractitionerDetails() {
      const practitioner = await getPractitionerByID(auth, client, signal, id, requestHeaders)
      setPractitioner(practitioner)
    }

    getPractitionerDetails()

    return () => {
      abortController.abort()
    }
  }, [ready])

  return (
    <React.Fragment>
      <li>{`${practitioner.name ? practitioner.name[0].given[0] : 'N/A'} ${
        practitioner.name ? practitioner.name[0].family : 'N/A'
      } - ID: ${practitioner.id}`}</li>
    </React.Fragment>
  )
}

export default CarePlanPractitioner
