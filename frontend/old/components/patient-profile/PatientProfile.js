import React, { useState, useEffect, useContext } from 'react'
import { Route, Switch, NavLink, useParams } from 'react-router-dom'

import styles from './patient-profile.module.css'
import PersonalInfo from './PersonalInfo'
import Visits from './Visits'
import { getPatientByID } from '../../api/PatientApi'
import Medications from './Medications'
import Allergies from './Allergies'
import Immunizations from './Immunizations'
import CarePlans from './CarePlans'
import History from './History'
import Cases from './Cases'
import FhirContext from '../../context/FhirContext'

const PatientProfile = ({ match, basePath, auth }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [patient, setPatient] = useState(null)
  const { id, pid } = useParams()

  useEffect(() => {
    if (!ready) return
    getPatientByID(auth, client, signal, pid, requestHeaders).then((result) => {
      setPatient(result)
    })

    return () => {
      abortController.abort()
    }
  }, [ready, pid])

  const calculateAge = (date) => {
    const birthDate = new Date(date)
    const ageDifMs = Date.now() - birthDate.getTime()
    const ageDate = new Date(ageDifMs)

    return Math.abs(ageDate.getUTCFullYear() - 1970)
  }

  const buildDetailsString = () => {
    var detailString = ''

    if (patient.gender) {
      detailString += patient.gender.charAt(0).toUpperCase() + patient.gender.slice(1)
    }

    if (patient.birthDate) {
      if (patient.deceasedDateTime) {
        detailString += ' | Deceased '
      } else {
        detailString += ' | Age ' + calculateAge(patient.birthDate)
      }
    }

    if (!patient.address) {
      return detailString
    }

    if (patient.address[0].city) {
      detailString += ' | ' + patient.address[0].city
    }

    if (patient.address[0].state) {
      detailString += ', ' + patient.address[0].state
    }

    if (patient.address[0].country) {
      detailString += ', ' + patient.address[0].country
    }

    return detailString
  }

  return (
    patient && (
      <div className='main-content-container container-fluid px-4'>
        <div className='page-header row no-gutters py-4 mb-3 border-bottom'>
          <div className='col text-center text-sm-left mb-0'>
            <span className='text-uppercase page-subtitle'>Patient Record</span>
            <h3 className='page-title'>
              {patient !== '' && patient.name
                ? `${patient.name[0].given[0]} ${patient.name[0].family}`
                : ''}
            </h3>
            <h6 className={styles.detailString}>{patient !== '' ? buildDetailsString() : ''}</h6>
            <a href={`${basePath}/project/${id}`}>{'< '}Back to Project</a>
          </div>
          <div className='col col-auto mb-0 align-self-end'>
            <h6>Patient ID {patient.id}</h6>
          </div>
        </div>
        <div className='d-flex justify-content-center'>
          <div className={`card ${styles.navProfile}`}>
            <div className='card-header'>
              <ul className='nav nav-tabs card-header-tabs'>
                <li className='nav-item'>
                  <NavLink exact className='nav-link' to={`${match.url}`} activeClassName='active'>
                    Personal Information
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    exact
                    className='nav-link'
                    to={`${match.url}/visits`}
                    activeClassName='active'
                  >
                    Visits
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    exact
                    className='nav-link'
                    to={`${match.url}/medications`}
                    activeClassName='active'
                  >
                    Medications
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    exact
                    className='nav-link'
                    to={`${match.url}/allergies`}
                    activeClassName='active'
                  >
                    Allergies
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    exact
                    className='nav-link'
                    to={`${match.url}/immunizations`}
                    activeClassName='active'
                  >
                    Immunizations
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    exact
                    className='nav-link'
                    to={`${match.url}/careplans`}
                    activeClassName='active'
                  >
                    Care Plans
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    exact
                    className='nav-link'
                    to={`${match.url}/history`}
                    activeClassName='active'
                  >
                    Medical History
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    exact
                    className='nav-link'
                    to={`${match.url}/cases`}
                    activeClassName='active'
                  >
                    Cases
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className='card-body'>
              <Switch>
                <Route
                  path={`${match.url}/visits`}
                  render={(props) => (
                    <Visits
                      patient={patient}
                      {...props}
                      auth={auth}
                      client={client}
                      signal={signal}
                      projectId={id}
                    />
                  )}
                />
                <Route
                  path={`${match.url}/medications`}
                  render={(props) => (
                    <Medications
                      patient={patient}
                      {...props}
                      auth={auth}
                      client={client}
                      signal={signal}
                      projectId={id}
                    />
                  )}
                />
                <Route
                  path={`${match.url}/allergies`}
                  render={(props) => (
                    <Allergies
                      patient={patient}
                      {...props}
                      auth={auth}
                      client={client}
                      signal={signal}
                      projectId={id}
                    />
                  )}
                />
                <Route
                  path={`${match.url}/immunizations`}
                  render={(props) => (
                    <Immunizations
                      patient={patient}
                      {...props}
                      auth={auth}
                      client={client}
                      signal={signal}
                      projectId={id}
                    />
                  )}
                />
                <Route
                  path={`${match.url}/careplans`}
                  render={(props) => (
                    <CarePlans
                      patient={patient}
                      {...props}
                      auth={auth}
                      client={client}
                      signal={signal}
                    />
                  )}
                />
                <Route
                  path={`${match.url}/history`}
                  render={(props) => (
                    <History
                      patient={patient}
                      {...props}
                      auth={auth}
                      client={client}
                      signal={signal}
                      projectId={id}
                    />
                  )}
                />
                <Route
                  path={`${match.url}/cases`}
                  render={(props) => (
                    <Cases
                      patient={patient}
                      {...props}
                      basePath={basePath}
                      projectId={id}
                      auth={auth}
                      client={client}
                      signal={signal}
                    />
                  )}
                />
                <Route
                  exact
                  path={`${match.url}`}
                  render={(props) => (
                    <PersonalInfo
                      patient={patient}
                      {...props}
                      auth={auth}
                      client={client}
                      signal={signal}
                    />
                  )}
                />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    )
  )
}

export default PatientProfile
