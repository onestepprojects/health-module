import React from 'react'
import _get from 'lodash/get'
import styles from './patient-profile.module.css'
import { dateInWords } from '../../utils'

const Medication = ({ medication }) => {
  return (
    <div key={medication.id} className={`card mt-3 ${styles.medication}`}>
      <div className='card-body d-flex flex-column align-items-start'>
        <h4 className='card-title'>
          {_get(medication, 'medicationCodeableConcept.text', 'Unknown Medication')}
        </h4>
        <h6 className='card-subtitle mt-2'>{dateInWords(_get(medication, 'effectiveDateTime'))}</h6>
        <span className='mt-2'>Status: {_get(medication, 'status')}</span>
        <span className='mt-2'>Dosage: {_get(medication, 'dosage.text')}</span>
        <span className='mt-2'>Notes: {_get(medication, 'note[0].text', '')}</span>
      </div>
    </div>
  )
}

export default Medication
