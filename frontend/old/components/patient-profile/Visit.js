import React from 'react'
import _get from 'lodash/get'

import { dateInWords } from '../../utils'

import styles from './patient-profile.module.css'

const Visit = ({ visit }) => {
  return (
    <div key={visit.id} className={`card mt-3 ${styles.visit}`}>
      <div className='card-body d-flex flex-column align-items-start'>
        <h4 className='card-title'>{dateInWords(_get(visit, 'period.start'), 'Unknown')}</h4>
        <h6 className='card-subtitle mt-2'>
          Reason for visit: {_get(visit, 'reasonCode[0].text')}
        </h6>
      </div>
    </div>
  )
}

export default Visit
