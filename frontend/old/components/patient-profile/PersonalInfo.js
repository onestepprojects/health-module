import React, { useState, useEffect } from 'react'
import moment from 'moment'
import _get from 'lodash/get'

import { findExtension } from '../../api/PatientApi'
import { patientToObject } from '../../api/schema/Patient'

import HealthForm from '../../api/schema/HealthForm'

import styles from './patient-profile.module.css'
import FhirForm from './FhirForm'

const PersonalInfo = ({ patient, auth, client, signal }) => {
  const [show, setShow] = useState(false)
  const form = new HealthForm(patientToObject(patient))

  const dateInWords = (date) => {
    if (date === '') {
      return ''
    }

    const dateParsed = new Date(date)

    return moment(dateParsed).format('MMMM D, YYYY')
  }

  return (
    <React.Fragment>
      <div className='row'>
        <div className='col'>
          <ul className='list-group list-group-flush'>
            <li className='list-group-item pt-0 pl-0 pb-1'>
              <strong className='text-muted d-block mb-1'>Full Name</strong>
              <span>
                {patient !== '' && patient.name
                  ? `${_get(patient, 'name[0].given[0]', '')} ${_get(
                      patient,
                      'name[0].family',
                      ''
                    )}`
                  : ''}
              </span>
            </li>
            <li className='list-group-item pl-0 pb-1'>
              <strong className='text-muted d-block mb-1'>Birthdate</strong>
              <span>
                <span>{patient !== '' ? dateInWords(_get(patient, 'birthDate', '')) : ''}</span>
              </span>
            </li>
            <li className='list-group-item pl-0 pb-1'>
              <strong className='text-muted d-block mb-1'>Gender</strong>
              <span>
                {patient !== ''
                  ? _get(patient, 'gender', '').charAt(0).toUpperCase() +
                    _get(patient, 'gender', '').slice(1)
                  : ''}
              </span>
            </li>
            <li className='list-group-item pl-0 pb-1'>
              <strong className='text-muted d-block mb-1'>Preferred Language</strong>
              <span>
                {patient !== '' ? _get(patient, 'communication[0].language.text', '') : ''}
              </span>
            </li>
          </ul>
        </div>
        <div className='col'>
          <ul className='list-group list-group-flush'>
            <li className='list-group-item pt-0 pl-0 pb-1'>
              <strong className='text-muted d-block mb-1'>Address</strong>
              <span>{patient !== '' ? _get(patient, 'address[0].line[0]', '') : ''}</span>
              <br />
              <span>{patient !== '' ? _get(patient, 'address[0].line[1]', '') : ''}</span>
              <br />
              <span>
                {`${patient !== '' ? _get(patient, 'address[0].city', '') : ''}, ${
                  patient !== '' ? _get(patient, 'address[0].state') : ''
                } ${patient !== '' ? ` ${_get(patient, 'address[0].postalCode', '')}` : ''}`}
              </span>
              <br />
              <span>{patient !== '' ? _get(patient, 'address[0].country', '') : ''}</span>
            </li>
            <li className='list-group-item pl-0 pb-1'>
              <strong className='text-muted d-block mb-1'>Phone Number</strong>
              <span>{patient !== '' ? _get(patient, 'telecom[0].value', '') : ''}</span>
            </li>
            <li className='list-group-item pl-0 pb-1'>
              <strong className='text-muted d-block mb-1'>Email</strong>
              <span>{patient !== '' ? _get(patient, 'telecom[1].value', '') : ''}</span>
            </li>
          </ul>
        </div>
      </div>
      <div className='row align-items-end mt-5 pl-0 pb-0'>
        <div className='col-auto mr-auto'></div>
        <div className='col-auto'>
          {/*<button type='button' className='btn btn-lg btn-primary' onClick={() => setShow(true)}>
            Edit
            </button>*/}
        </div>
      </div>
      <FhirForm
        form={form}
        show={show}
        setShow={setShow}
        auth={auth}
        client={client}
        signal={signal}
        id={patient.id}
      />
    </React.Fragment>
  )
}

export default PersonalInfo
