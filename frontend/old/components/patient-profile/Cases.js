import React, { useEffect, useState, useContext } from 'react'
import ReactPaginate from 'react-paginate'
import { Modal, Button, Spinner } from 'react-bootstrap'
import { Link } from 'react-router-dom'

import CaseItem from './CaseItem'
import { compositionsByPatient } from '../../api/CompositionApi'

import styles from './patient-profile.module.css'
import FhirContext from '../../context/FhirContext'

const Cases = ({ patient, projectId, basePath, auth }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [cases, setCases] = useState([])
  const [data, setData] = useState(null)
  const [pageCount, setPageCount] = useState(0)
  const [show, setShow] = useState(false)
  const [loading, setLoading] = useState(false)

  const perPage = 8

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  useEffect(() => {
    async function gatherCases(id) {
      if (!ready) return

      setLoading(true)
      const result = await compositionsByPatient(auth, client, signal, id, requestHeaders)
      setCases(result)
      setLoading(false)
    }

    gatherCases(patient.id)

    return () => {
      abortController.abort()
    }
  }, [ready, patient.id])

  useEffect(() => {
    setPageCount(Math.ceil(cases.length / perPage))
    setData(cases.slice(0, perPage))
  }, [cases])

  const onPageChange = ({ selected }) => {
    let offset = Math.ceil(selected * perPage)
    setData(cases.slice(offset, offset + perPage))
  }

  const displayCases = () => {
    if (loading)
      return (
        <div className='text-center'>
          <Spinner animation='border' variant='primary' />
        </div>
      )
    else if (data && data.length > 0) {
      return (
        <React.Fragment>
          <div className='d-flex flex-wrap'>
            {data.map((datum) => {
              return (
                <div key={datum.id} className={`card mt-3 ${styles.item}`}>
                  <Link to={`${basePath}/project/${projectId}/case/${datum.id}`}>
                    <CaseItem item={datum} />
                  </Link>
                </div>
              )
            })}
          </div>

          <div className='d-flex justify-content-center mt-5'>
            <ReactPaginate
              pageCount={pageCount}
              pageRangeDisplayed={5}
              marginPagesDisplayed={5}
              previousLabel='Previous'
              nextLabel='Next'
              breakLabel='...'
              breakClassName='page-item'
              onPageChange={onPageChange}
              containerClassName='pagination'
              pageClassName='page-item'
              pageLinkClassName='page-link'
              activeClassName={styles.pageActive}
              previousClassName='page-item'
              nextClassName='page-item'
              previousLinkClassName='page-link'
              nextLinkClassName='page-link'
            />
          </div>
        </React.Fragment>
      )
    } else {
      return 'No Available Cases'
    }
  }

  return (
    <React.Fragment>
      {displayCases()}
      <div className='d-flex flex-column align-items-end'>
        <Link to={`${basePath}/project/${projectId}/case`}>
          <div type='button' className={`btn-circle btn-primary ${styles.plus}`}>
            +
          </div>
        </Link>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New Composition</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input type='text' id='demo9' />
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={handleClose}>
            Close
          </Button>
          <Button variant='primary' onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  )
}

export default Cases
