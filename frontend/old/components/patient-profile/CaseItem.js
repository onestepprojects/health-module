import React from 'react'
import _get from 'lodash/get'

import { dateInWords } from '../../utils'

import styles from './patient-profile.module.css'

const HistoryItem = ({ item }) => {
  return (
    <div className='card-body d-flex flex-column'>
      <h4 className='card-title'>{_get(item, 'title', 'Other')}</h4>
      <h6 className='card-subtitle mt-2'>{dateInWords(_get(item, 'date'), 'Unknown')}</h6>
      <span className='mt-2'>{_get(item, 'id', '')}</span>
    </div>
  )
}

export default HistoryItem
