import React from 'react'
import _get from 'lodash/get'

import styles from './patient-profile.module.css'
import { dateInWords } from '../../utils'

const Immunization = ({ immunization }) => {
  return (
    <div className={`card mt-3 ${styles.immunization}`}>
      <div className='card-body d-flex flex-column align-items-start'>
        <h4 className='card-title'>{_get(immunization, 'vaccineCode.text', 'Immunization')}</h4>
        <h6 className='card-subtitle mt-2'>
          {dateInWords(_get(immunization, 'occurrenceDateTime'), 'N/A')}
        </h6>
        <span className='mt-2'>Status: {_get(immunization, 'status')}</span>
      </div>
    </div>
  )
}

export default Immunization
