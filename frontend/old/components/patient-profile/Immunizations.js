import React, { useState, useEffect, useContext } from 'react'
import { Modal, Button, Spinner } from 'react-bootstrap'
import ReactPaginate from 'react-paginate'

import { immunizationsByPatient, createImmunization } from '../../api/ImmunizationApi'
import FhirContext from '../../context/FhirContext'
import Immunization from './Immunization'
import { addResourceToProject } from '../../api/ProjectApi'
import ListSearch from '../common/ListSearch'

import styles from './patient-profile.module.css'

const Immunizations = ({ patient, auth, projectId }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [immunizations, setImmunizations] = useState([])
  const [text, setText] = useState('')
  const [date, setDate] = useState('')
  const [data, setData] = useState(null)
  const [pageCount, setPageCount] = useState(0)
  const [show, setShow] = useState(false)
  const [loading, setLoading] = useState(false)

  const perPage = 8

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  const immunizationList = [
    'BCG (Tuberculosis)',
    'Chickenpox (Varicella)',
    'SARS-CoV-2 (Covid-19)',
    'DTaP (Diptheria, tetanus, and whooping cough)',
    'Hib (Haemophilus influenzae type b)',
    'HepA (Hepatitis A)',
    'HepB (Hepatitis B)',
    'HPV',
    'MMR (Measles, mumps, and rubella)',
    'Meningo C (Menigitis)',
    'Pneumococcal (PCV/Pneumo10/Pneumo23)',
    'Rotavirus',
    'IPV (Polio)',
    'OPV (Polio)',
    'Yellow Fever',
  ]

  const createNewImmunization = (e) => {
    e.preventDefault()

    async function create() {
      const immunization = await createImmunization(
        auth,
        client,
        signal,
        { 'Content-Type': 'application/json' },
        { text, date },
        patient.id
      )
      addResourceToProject({ resourceId: immunization.id, resourceType: 'Immunization' }, projectId)

      let newImmunizations = immunizations
      newImmunizations.push(immunization)
      setImmunizations(newImmunizations)
      setPageCount(Math.ceil(newImmunizations.length / perPage))
      setData(newImmunizations.slice(0, perPage))
    }
    create()
    handleClose()
  }

  useEffect(() => {
    async function gatherImmunizations(id) {
      if (!ready) return

      setLoading(true)
      const result = await immunizationsByPatient(auth, client, signal, id, requestHeaders)
      setImmunizations(result)
      setLoading(false)
    }

    gatherImmunizations(patient.id)

    return () => {
      abortController.abort()
    }
  }, [ready, patient.id])

  useEffect(() => {
    setPageCount(Math.ceil(immunizations.length / perPage))
    setData(immunizations.slice(0, perPage))
  }, [immunizations])

  const onPageChange = ({ selected }) => {
    let offset = Math.ceil(selected * perPage)
    setData(immunizations.slice(offset, offset + perPage))
  }

  const displayImmunizations = () => {
    if (loading)
      return (
        <div className='text-center'>
          <Spinner animation='border' variant='primary' />
        </div>
      )
    else if (data && data.length > 0) {
      return (
        <React.Fragment>
          <div className='d-flex flex-wrap'>
            {data.map((datum) => {
              return <Immunization key={datum.id} immunization={datum} />
            })}
          </div>

          <div className='d-flex justify-content-center mt-5'>
            <ReactPaginate
              pageCount={pageCount}
              pageRangeDisplayed={5}
              marginPagesDisplayed={5}
              previousLabel='Previous'
              nextLabel='Next'
              breakLabel='...'
              breakClassName='page-item'
              onPageChange={onPageChange}
              containerClassName='pagination'
              pageClassName='page-item'
              pageLinkClassName='page-link'
              activeClassName={styles.pageActive}
              previousClassName='page-item'
              nextClassName='page-item'
              previousLinkClassName='page-link'
              nextLinkClassName='page-link'
            />
          </div>
        </React.Fragment>
      )
    } else {
      return 'No Available Immunizations'
    }
  }

  return (
    <React.Fragment>
      {displayImmunizations()}
      <div className='d-flex flex-column align-items-end'>
        <div onClick={handleShow}>
          <div type='button' className={`btn-circle btn-primary ${styles.plus}`}>
            +
          </div>
        </div>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New Immunization</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className='form-row'>
              <div className='form-group col'>
                <ListSearch
                  placeholder='Search Immunizations'
                  choiceList={immunizationList}
                  onType={setText}
                />
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col'>
                <label>Immunization Date</label>
                <input
                  type='date'
                  id='immunization-date'
                  className='form-control'
                  onChange={(e) => setDate(e.target.value)}
                />
              </div>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={handleClose}>
            Close
          </Button>
          <Button variant='primary' onClick={createNewImmunization}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  )
}

export default Immunizations
