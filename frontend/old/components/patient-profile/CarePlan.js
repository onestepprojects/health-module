import React, { useState, useEffect, useContext } from 'react'
import _get from 'lodash/get'
import { dateInWords } from '../../utils'
import styles from './patient-profile.module.css'
import { getCareTeamByID } from '../../api/CareTeamApi'
import FhirContext from '../../context/FhirContext'
import { getEncounterByID } from '../../api/EncounterApi'
import CarePlanPractitioner from './CarePlanPractitioner'

const CarePlan = ({ carePlan, auth }) => {
  const abortController = new AbortController()
  const signal = abortController.signal
  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [careTeams, setCareTeams] = useState([])
  const [encounter, setEncounter] = useState([])

  useEffect(() => {
    async function gatherCareTeams() {
      const careTeamPromises = carePlan.careTeam.map((elem) => {
        return new Promise((res, rej) => {
          getCareTeamByID(
            auth,
            client,
            signal,
            elem.reference.replace('CareTeam/', ''),
            requestHeaders
          )
            .then((r) => {
              res(r)
            })
            .catch((e) => {
              console.error(e)
              res()
            })
        })
      })
      Promise.all(careTeamPromises).then((results) => {
        setCareTeams(results)
      })
    }

    async function getEncounterDetails() {
      const encounter = await getEncounterByID(
        auth,
        client,
        signal,
        carePlan.encounter.reference.replace('Encounter/', ''),
        requestHeaders
      )
      setEncounter(encounter)
    }

    gatherCareTeams()
    getEncounterDetails()

    return () => {
      abortController.abort()
    }
  }, [ready])

  return (
    <React.Fragment>
      {carePlan !== undefined ? (
        <div className={`card mt-3 ${styles.carePlan}`}>
          <div className='card-body d-flex flex-column align-items-start'>
            <h4 className='card-title'>CarePlan</h4>
            <h6 className='card-subtitle mt-2'>{`Ticket ID: ${_get(
              carePlan,
              'description',
              'No Ticket ID'
            )}`}</h6>
            <span className='mt-2'>
              Case ID: {_get(carePlan, 'note[0].text').replace('Composition/', '')}
            </span>
            <span className='mt-2'>
              Start Period: {dateInWords(_get(carePlan, 'period.start'), 'Unknown')}
            </span>
            <span className='mt-2'>
              Visit ID: {_get(carePlan, 'encounter.reference').replace('Encounter/', '')}
            </span>
            <span className='mt-2'>Visit Date: {_get(encounter, 'period.start')}</span>
            <span className='mt-2'>
              <strong className='d-block mb-1'>Care Team</strong>
              {careTeams.map((careTeam) => {
                return (
                  <>
                    <span>Care Team ID: {careTeam.id}</span>
                    <ul>
                      {careTeam.participant.map((value, i) => {
                        return (
                          <CarePlanPractitioner
                            auth={auth}
                            id={value.member.reference.replace('Practitioner/', '')}
                          />
                        )
                      })}
                    </ul>
                  </>
                )
              })}
            </span>
          </div>
        </div>
      ) : (
        'No Care Plan'
      )}
    </React.Fragment>
  )
}

export default CarePlan
