import React, { useState, useEffect, useContext } from 'react'
import { Modal, Button, Spinner } from 'react-bootstrap'
import ReactPaginate from 'react-paginate'

import { allergiesByPatient } from '../../api/AllergyApi'
import { createAllergy } from '../../api/AllergyApi'
import FhirContext from '../../context/FhirContext'
import Allergy from './Allergy'
import { addResourceToProject } from '../../api/ProjectApi'
import ListSearch from '../common/ListSearch'

import styles from './patient-profile.module.css'

const Allergies = ({ patient, auth, projectId }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [allergies, setAllergies] = useState([])
  const [data, setData] = useState(null)
  const [pageCount, setPageCount] = useState(0)
  const [show, setShow] = useState(false)
  const [newAllergy, setNewAllergy] = useState([])
  const [symptom, setSymptoms] = useState([])
  const [loading, setLoading] = useState(false)

  const perPage = 8

  const allergyList = [
    'Penicillin',
    'Benzathine penicillin',
    'Amoxicillin',
    'Ampicillin',
    'Oxacillin',
    'Sulphonamides',
    'Bactrim',
    'Trimethoprim',
    'Sulfamethoxazole',
    'Metamizole (dipyrone)',
    'Metoclopramide (Plasil)',
    'Anticonvulsants',
    'Aspirin',
  ]

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  useEffect(() => {
    async function gatherAllergies(id) {
      if (!ready) return
      setLoading(true)

      const result = await allergiesByPatient(auth, client, signal, id, requestHeaders)
      setAllergies(result)
      setLoading(false)
    }

    gatherAllergies(patient.id)

    return () => {
      abortController.abort()
    }
  }, [ready, patient.id])

  useEffect(() => {
    setPageCount(Math.ceil(allergies.length / perPage))
    setData(allergies.slice(0, perPage))
  }, [allergies])

  const onPageChange = ({ selected }) => {
    let offset = Math.ceil(selected * perPage)
    setData(allergies.slice(offset, offset + perPage))
  }

  const createNewAllergy = async (e) => {
    e.preventDefault()

    const allergy = await createAllergy(
      auth,
      client,
      signal,
      { 'Content-Type': 'application/json' },
      { newAllergy, symptom },
      patient.id
    )
    addResourceToProject({ resourceId: allergy.id, resourceType: 'AllergyIntolerance' }, projectId)
    let newAllergies = allergies
    newAllergies.push(allergy)
    setAllergies(newAllergies)
    setPageCount(Math.ceil(newAllergies.length / perPage))
    setData(newAllergies.slice(0, perPage))
    handleClose()
  }

  const displayAllergies = () => {
    if (loading)
      return (
        <div className='text-center'>
          <Spinner animation='border' variant='primary' />
        </div>
      )
    else if (data && data.length > 0) {
      return (
        <React.Fragment>
          <div className='d-flex flex-wrap'>
            {data.map((datum) => {
              return <Allergy key={datum.id} allergy={datum} />
            })}
          </div>

          <div className='d-flex justify-content-center mt-5'>
            <ReactPaginate
              pageCount={pageCount}
              pageRangeDisplayed={5}
              marginPagesDisplayed={5}
              previousLabel='Previous'
              nextLabel='Next'
              breakLabel='...'
              breakClassName='page-item'
              onPageChange={onPageChange}
              containerClassName='pagination'
              pageClassName='page-item'
              pageLinkClassName='page-link'
              activeClassName={styles.pageActive}
              previousClassName='page-item'
              nextClassName='page-item'
              previousLinkClassName='page-link'
              nextLinkClassName='page-link'
            />
          </div>
        </React.Fragment>
      )
    } else {
      return 'No Available Allergies'
    }
  }

  return (
    <React.Fragment>
      {displayAllergies()}
      <div className='d-flex flex-column align-items-end'>
        <div onClick={handleShow}>
          <div type='button' className={`btn-circle btn-primary ${styles.plus}`}>
            +
          </div>
        </div>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New Allergy</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className='form-row'>
              <div className='form-group col'>
                <ListSearch
                  placeholder='Search Allergies'
                  choiceList={allergyList}
                  onType={setNewAllergy}
                />
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col'>
                <textarea
                  placeholder='Comma-Separated List of Symptoms'
                  className='form-control'
                  onChange={(e) => setSymptoms(e.target.value)}
                ></textarea>
              </div>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={handleClose}>
            Close
          </Button>
          <Button variant='primary' onClick={createNewAllergy}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  )
}

export default Allergies
