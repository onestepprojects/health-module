import React from 'react'
import _get from 'lodash/get'

import { dateInWords } from '../../utils'

import styles from './patient-profile.module.css'

const HistoryItem = ({ item }) => {
  return (
    <div className={`card mt-3 ${styles.item}`}>
      <div className='card-body d-flex flex-column'>
        <h4 className='card-title'>{_get(item, 'code.text', 'Other')}</h4>
        <h6 className='card-subtitle mt-2'>{dateInWords(_get(item, 'recordedDate'), 'Unknown')}</h6>
        <span className='mt-2'>Note: {_get(item, 'note[0].text', '')}</span>
      </div>
    </div>
  )
}

export default HistoryItem
