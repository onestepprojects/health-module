import React, { useEffect, useState, useContext } from 'react'
import ReactPaginate from 'react-paginate'
import { Spinner } from 'react-bootstrap'
import Medication from './Medication'

import {
  medicationsByPatient,
  createMedicationRequest,
  medicationRequestsByPatient,
  createMedicationStatement,
  createMedicationAdministration,
} from '../../api/MedicationsApi'
import { addResourceToProject } from '../../api/ProjectApi'
import { Modal, Button } from 'react-bootstrap'

import styles from './patient-profile.module.css'
import FhirContext from '../../context/FhirContext'
import GenericSearch from '../common/GenericSearch'
import {} from '../../api/MedicationsApi'

const Medications = ({ patient, auth, projectId }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [medications, setMedications] = useState([])
  const [medData, setMedData] = useState([])
  const [data, setData] = useState(null)
  const [pageCount, setPageCount] = useState(0)
  const [show, setShow] = useState(false)
  const perPage = 4
  const [newMed, setNewMed] = useState('')
  const [strength, setStrength] = useState('')
  const [note, setNote] = useState('')
  const [loading, setLoading] = useState(false)

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  useEffect(() => {
    async function gatherMedications(id) {
      if (!ready) return

      setLoading(true)
      const result = await medicationsByPatient(auth, client, signal, id, requestHeaders)
      setMedications(result)
      setLoading(false)
    }

    gatherMedications(patient.id)

    return () => {
      abortController.abort()
    }
  }, [ready, patient.id])

  useEffect(() => {
    setPageCount(Math.ceil(medications.length / perPage))
    setData(medications.slice(0, perPage))
  }, [medications])

  const onPageChange = ({ selected }) => {
    let offset = Math.ceil(selected * perPage)
    setData(medications.slice(offset, offset + perPage))
  }

  const onMedSelected = ({ suggestionIndex, allSuggestions }) => {
    setMedData(allSuggestions[2].STRENGTHS_AND_FORMS[suggestionIndex])
    setNewMed(allSuggestions[1][suggestionIndex])
  }

  const onMedType = (value) => {
    setNewMed(value)
  }

  const onStrengthType = (value) => {
    setStrength(value)
  }

  const onStrengthSelected = ({ suggestionIndex }) => {
    setStrength(medData[suggestionIndex])
  }

  const displayMedications = () => {
    if (loading)
      return (
        <div className='text-center'>
          <Spinner animation='border' variant='primary' />
        </div>
      )
    else if (data && data.length > 0) {
      return (
        <React.Fragment>
          <div className='d-flex flex-wrap'>
            {data.map((datum) => {
              return <Medication key={datum.id} medication={datum} />
            })}
          </div>

          <div className='d-flex justify-content-center mt-5'>
            <ReactPaginate
              pageCount={pageCount}
              pageRangeDisplayed={5}
              marginPagesDisplayed={5}
              previousLabel='Previous'
              nextLabel='Next'
              breakLabel='...'
              breakClassName='page-item'
              onPageChange={onPageChange}
              containerClassName='pagination'
              pageClassName='page-item'
              pageLinkClassName='page-link'
              activeClassName={styles.pageActive}
              previousClassName='page-item'
              nextClassName='page-item'
              previousLinkClassName='page-link'
              nextLinkClassName='page-link'
            />
          </div>
        </React.Fragment>
      )
    } else {
      return 'No Available Medications'
    }
  }

  const createNewMedication = (e) => {
    e.preventDefault()

    async function createMedication() {
      /*const result = await createMedicationRequest(
        auth,
        client,
        signal,
        { 'Content-Type': 'application/json' },
        { newMed, strength, instructions },
        patient.id
      )
      const resourceId = await medicationRequestsByPatient(
        auth,
        client,
        signal,
        patient.id,
        { 'Content-Type': 'application/json' },
        { newMed, strength, instructions },
        result
      )
      const medResource = await createMedicationStatement(
        auth,
        client,
        signal,
        { 'Content-Type': 'application/json' },
        resourceId,
        patient.id
      )*/

      const medication = await createMedicationAdministration(
        auth,
        client,
        signal,
        { 'Content-Type': 'application/json' },
        { newMed, strength, note },
        patient.id
      )

      addResourceToProject(
        { resourceId: medication.id, resourceType: 'MedicationAdministration' },
        projectId
      )

      let newMedications = medications
      newMedications.push(medication)
      setMedications(newMedications)
      setPageCount(Math.ceil(newMedications.length / perPage))
      setData(newMedications.slice(0, perPage))
    }
    createMedication()
    handleClose()
  }

  return (
    <React.Fragment>
      {displayMedications()}
      <div className='d-flex flex-column align-items-end'>
        <div onClick={handleShow}>
          <div type='button' className={`btn-circle btn-primary ${styles.plus}`}>
            +
          </div>
        </div>
      </div>
      <Modal size='lg' show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New Medication</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className='form-row'>
              <div className='form-group col'>
                <GenericSearch
                  placeholder='Search Medications'
                  requestUrl='https://clinicaltables.nlm.nih.gov/api/rxterms/v3/search?&ef=STRENGTHS_AND_FORMS&maxList=500'
                  onDataSelected={onMedSelected}
                  onType={onMedType}
                />
              </div>
              <div className='form-group col'>
                <GenericSearch
                  placeholder='Search Strengths'
                  inputData={medData}
                  onDataSelected={onStrengthSelected}
                  onType={onStrengthType}
                />
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col'>
                <input
                  type='text'
                  id='note'
                  placeholder='Additional notes'
                  className='form-control'
                  onChange={(e) => setNote(e.target.value)}
                />
              </div>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={handleClose}>
            Close
          </Button>
          <Button variant='primary' onClick={createNewMedication}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  )
}

export default Medications
