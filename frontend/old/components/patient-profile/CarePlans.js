import React, { useState, useEffect, useContext } from 'react'
import ReactPaginate from 'react-paginate'
import { Spinner } from 'react-bootstrap'

import FhirContext from '../../context/FhirContext'

import { carePlansByPatient } from '../../api/CarePlanApi'

import CarePlan from './CarePlan'

import styles from './patient-profile.module.css'

const CarePlans = ({ patient, auth }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [carePlans, setCarePlans] = useState([])
  const [data, setData] = useState(null)
  const [pageCount, setPageCount] = useState(0)
  const [loading, setLoading] = useState(false)

  const perPage = 1

  useEffect(() => {
    async function gatherCarePlans(id) {
      if (!ready) return
      setLoading(true)
      const result = await carePlansByPatient(auth, client, signal, id, requestHeaders)
      setCarePlans(result)
      setLoading(false)
    }

    gatherCarePlans(patient.id)

    return () => {
      abortController.abort()
    }
  }, [ready, patient.id])

  useEffect(() => {
    setPageCount(Math.ceil(carePlans.length / perPage))
    setData(carePlans.slice(0, perPage))
  }, [carePlans])

  const onPageChange = ({ selected }) => {
    let offset = Math.ceil(selected * perPage)
    setData(carePlans.slice(offset, offset + perPage))
  }

  const displayCarePlans = () => {
    if (loading)
      return (
        <div className='text-center'>
          <Spinner animation='border' variant='primary' />
        </div>
      )
    else if (data && data.length > 0) {
      return (
        <React.Fragment>
          <div className='d-flex flex-wrap'>
            {data.map((datum) => {
              return <CarePlan key={datum.id} carePlan={datum} auth={auth} />
            })}
          </div>

          <div className='d-flex justify-content-center mt-5'>
            <ReactPaginate
              pageCount={pageCount}
              pageRangeDisplayed={5}
              marginPagesDisplayed={5}
              previousLabel='Previous'
              nextLabel='Next'
              breakLabel='...'
              breakClassName='page-item'
              onPageChange={onPageChange}
              containerClassName='pagination'
              pageClassName='page-item'
              pageLinkClassName='page-link'
              activeClassName={styles.pageActive}
              previousClassName='page-item'
              nextClassName='page-item'
              previousLinkClassName='page-link'
              nextLinkClassName='page-link'
            />
          </div>
        </React.Fragment>
      )
    } else {
      return 'No Available Care Plans'
    }
  }

  return <React.Fragment>{displayCarePlans()}</React.Fragment>
}

export default CarePlans
