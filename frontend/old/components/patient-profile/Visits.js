import React, { useEffect, useState, useContext } from 'react'
import ReactPaginate from 'react-paginate'
import { Spinner, Modal, Button } from 'react-bootstrap'

import Visit from './Visit'
import { getEncountersByPatientID, createEncounter } from '../../api/EncounterApi'
import { addResourceToProject } from '../../api/ProjectApi'

import styles from './patient-profile.module.css'
import FhirContext from '../../context/FhirContext'

const Visits = ({ patient, match, auth, projectId }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [encounters, setEncounters] = useState([])
  const [reason, setReason] = useState(null)
  const [startDate, setDate] = useState(null)
  const [data, setData] = useState(null)
  const [pageCount, setPageCount] = useState(0)
  const [show, setShow] = useState(false)
  const [loading, setLoading] = useState(false)

  const perPage = 8

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  const createNewVisit = (e) => {
    e.preventDefault()

    async function create() {
      const encounter = await createEncounter(
        auth,
        client,
        signal,
        { 'Content-Type': 'application/json' },
        { startDate, reason },
        patient.id
      )
      addResourceToProject({ resourceId: encounter.id, resourceType: 'Encounter' }, projectId)
      let newEncounters = encounters
      newEncounters.push(encounter)
      setEncounters(newEncounters)
      setPageCount(Math.ceil(newEncounters.length / perPage))
      setData(newEncounters.slice(0, perPage))
    }
    create()
    handleClose()
  }

  useEffect(() => {
    async function gatherEncounters(id) {
      if (!ready) return
      setLoading(true)
      const result = await getEncountersByPatientID(auth, client, signal, id, requestHeaders)
      setEncounters(result)
      setLoading(false)
    }

    gatherEncounters(patient.id)

    return () => {
      abortController.abort()
    }
  }, [ready, patient.id])

  useEffect(() => {
    setPageCount(Math.ceil(encounters.length / perPage))
    setData(encounters.slice(0, perPage))
  }, [encounters])

  const onPageChange = ({ selected }) => {
    let offset = Math.ceil(selected * perPage)
    setData(encounters.slice(offset, offset + perPage))
  }

  const displayVisits = () => {
    if (loading)
      return (
        <div className='text-center'>
          <Spinner animation='border' variant='primary' />
        </div>
      )
    else if (data && data.length > 0) {
      return (
        <React.Fragment>
          <div className='d-flex flex-wrap'>
            {data.map((datum) => {
              return <Visit key={datum.id} visit={datum} />
            })}
          </div>

          <div className='d-flex justify-content-center mt-5'>
            <ReactPaginate
              pageCount={pageCount}
              pageRangeDisplayed={5}
              marginPagesDisplayed={5}
              previousLabel='Previous'
              nextLabel='Next'
              breakLabel='...'
              breakClassName='page-item'
              onPageChange={onPageChange}
              containerClassName='pagination'
              pageClassName='page-item'
              pageLinkClassName='page-link'
              activeClassName={styles.pageActive}
              previousClassName='page-item'
              nextClassName='page-item'
              previousLinkClassName='page-link'
              nextLinkClassName='page-link'
            />
          </div>
        </React.Fragment>
      )
    } else {
      return 'No Available Visits'
    }
  }

  return (
    <React.Fragment>
      {displayVisits()}
      <div className='d-flex flex-column align-items-end'>
        <div onClick={handleShow}>
          <div type='button' className={`btn-circle btn-primary ${styles.plus}`}>
            +
          </div>
        </div>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New Visit</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className='form-row'>
              <div className='form-group col'>
                <label>Visit Date</label>
                <input
                  type='date'
                  id='visit-date'
                  className='form-control'
                  onChange={(e) => setDate(e.target.value)}
                />
                <br />
                <label>Reason for Visit</label>
                <input
                  type='text'
                  id='visit-reason'
                  placeholder='Reason for visit'
                  className='form-control'
                  onChange={(e) => setReason(e.target.value)}
                />
              </div>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={handleClose}>
            Close
          </Button>
          <Button variant='primary' onClick={createNewVisit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  )
}

export default Visits
