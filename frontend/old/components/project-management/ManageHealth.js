import React, { useState } from 'react'

import { Spinner, Button } from 'react-bootstrap'

import HealthProject from './HealthProject'
import styles from './manage-health.module.css'

const ManageHealth = ({ projects, match, history, auth }) => {
  const displayProjects = () => {
    if (projects && projects.length > 0) {
      return (
        <React.Fragment>
          <div className={`d-flex flex-column ${styles.healthProjects}`}>
            {projects.map((project) => {
              return (
                <HealthProject key={project.name} project={project} match={match} auth={auth} />
              )
            })}
          </div>
        </React.Fragment>
      )
    } else if (projects) {
      return (
        <div className='d-flex flex-column justify-content-center' style={{ zIndex: zIndex }}>
          <h4 className={styles.noProjects}>No Available Health Projects</h4>
        </div>
      )
    } else {
      return (
        <div className='d-flex flex-column justify-content-center'>
          <h5 className={styles.noProjects}>Loading Health Projects</h5>

          <div className={`${styles.noProjectsButton}`}>
            <Spinner animation='border' variant='primary' />
          </div>
        </div>
      )
    }
  }

  return (
    <div className='main-content-container container-fluid px-4'>
      <div className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'>
        <div className='col-10 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Health</span>
          <h3 className='page-title'>Manage Health Projects</h3>
        </div>
        <div className='col-8 col-sm-4 text-center text-sm-left mb-0'>
          <div className='text-right'>
            <Button href={`${process.env.REACT_APP_MENTOR_MODULE}`}>Sign Up for Mentorship</Button>
          </div>
        </div>
      </div>
      <div className={`d-flex mt-5 ${styles.search}`}></div>
      {displayProjects()}
    </div>
  )
}

export default ManageHealth
