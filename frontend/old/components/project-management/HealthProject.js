import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import styles from './health-project.module.css'

const HealthProject = ({ project, match, auth }) => {
  useEffect(() => {
    //TODO: Change to use auth when backend uses project auth
    /*auth.Get(
      `${process.env.REACT_APP_ORG_MODULE_API_URL}/projects/${project.id}`
    )*/
  }, [])

  return (
    <Link to={`${match.url}/project/${project.id}`} style={{ textDecoration: 'none' }}>
      <div style={{ maxWidth: '1200px' }} className={`card mt-3 w-100 ${styles.project}`}>
        <div className='card-body d-flex flex-column'>
          <h4 className='card-title mt-2'>{project.name}</h4>
          <h6 className='card-subtitle mt-2'>{project.description}</h6>
        </div>
      </div>
    </Link>
  )
}

export default HealthProject
