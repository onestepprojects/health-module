import React from 'react'

import * as Survey from 'survey-react'

import HealthForm from '../api/schema/HealthForm'
import Patient from '../api/schema/Patient'

import 'survey-react/survey.css'

const TestEditor = () => {
  const data = new HealthForm(new Patient()).createForm()

  return <Survey.Survey json={data} />
}

export default TestEditor
