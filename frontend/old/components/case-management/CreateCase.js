import React, { useState, useEffect, useContext } from 'react'
import FhirContext from '../../context/FhirContext'
import { Modal, Button, Form } from 'react-bootstrap'
import { Link, Redirect } from 'react-router-dom'
import { createComposition, updateComposition } from '../../api/CompositionApi'
import { createEncounter, getEncountersByPatientID } from '../../api/EncounterApi'
import { createObservation } from '../../api/ObservationApi'
import { createCarePlan } from '../../api/CarePlanApi'
import { createCareTeam } from '../../api/CareTeamApi'
import PatientSearch from '../../components/common/PatientSearch'
import 'bootstrap/dist/css/bootstrap.min.css'
import styles from './case.module.css'
import { addResourceToProject } from '../../api/ProjectApi'
import { createTicket } from '../../api/TicketApi'
import { createDocumentReference } from '../../api/DocumentReferenceApi'

const Case = ({ basePath, projectId, practitioner, patients, match, history, auth }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [patientCase, setPatientCase] = useState([])
  const [visits, setVisits] = useState([])
  const [patient, setPatient] = useState('')
  const [visit, setVisit] = useState('new-visit')
  const [visitReason, setVisitReason] = useState('')
  const [details, setDetails] = useState('')
  const [specialty, setSpecialty] = useState('')
  const [show, setShow] = useState(false)
  const [caseId, setCaseId] = useState(null)
  const [encounterId, setEncounterId] = useState(null)
  const [date, setDate] = useState(null)
  const [redirect, setRedirect] = useState(false)
  const [title, setCaseTitle] = useState('')
  const [priority, setPriority] = useState('')

  const gatherVisits = async function (patientId) {
    if (!ready) return
    const result = await getEncountersByPatientID(auth, client, signal, patientId, requestHeaders)
    setVisits(result)
  }

  const selectPatient = function (patientId) {
    setPatient(patientId)
    gatherVisits(patientId)
  }

  const specialties = [
    'Cardiology',
    'Infectious Disease',
    'Dermatology',
    'Hematology',
    'Neurology',
    'Gastroenterology',
    'OBGYN',
    'Ophthalmology',
    'Pediatrics',
    'Family',
    'Psychiatry',
    'Pain',
    'Internal',
    'Allergy',
    'Radiology',
    'Endocrinology',
    'Nephrology',
    'Rheumatology',
  ]

  const priorities = ['low', 'normal', 'high']

  const createVisit = async () => {
    const visitData = {
      reason: visitReason,
      startDate: date,
    }
    return createEncounter(auth, client, signal, requestHeaders, visitData, patient)
  }

  const createDetails = async () => {
    const detailData = {
      patientId: patient,
      value: details,
    }
    return createObservation(auth, client, signal, requestHeaders, detailData)
  }

  const createDocRef = async () => {
    const detailData = {
      patientId: patient,
      encounterId: visit,
      data: details,
    }
    return createDocumentReference(auth, client, signal, requestHeaders, detailData)
  }

  const createCase = async (encounterId, observationId, docRefId) => {
    const compositionData = {
      patientId: patient,
      provider: practitioner,
      encounterId: encounterId,
      observationId: observationId,
      documentReferenceId: docRefId,
      title: title,
    }
    return createComposition(auth, client, signal, requestHeaders, compositionData)
  }

  const submit = async () => {
    let encounterId = visit
    if (encounterId === 'new-visit') {
      //create Encounter
      const encounter = await createVisit()
      encounterId = encounter.id
      addResourceToProject({ resourceId: encounterId, resourceType: 'Encounter' }, projectId)
    }

    setEncounterId(encounterId)

    //create Observation
    const observation = await createDetails()
    const observationId = observation.id
    addResourceToProject({ resourceId: observationId, resourceType: 'Observation' }, projectId)

    //create DocumentReference
    const documentReference = await createDocRef()
    const docRefId = documentReference.id
    addResourceToProject({ resourceId: docRefId, resourceType: 'DocumentReference' }, projectId)

    const patientCase = await createCase(encounterId, observationId, docRefId)
    setPatientCase(patientCase)

    setCaseId(patientCase.id)
    addResourceToProject({ resourceId: patientCase.id, resourceType: 'Composition' }, projectId)

    setShow(true)
  }

  const handleClose = () => {
    setShow(false)
    setRedirect(true)
  }

  const newTicket = async () => {
    const ticket = await createTicket(
      auth,
      priority,
      specialty,
      title,
      visitReason,
      projectId,
      caseId
    )
    const ticketId = ticket.id

    let newCase = patientCase
    newCase.section.push({ title: 'ticket', entry: [{ reference: ticketId }] })

    await updateComposition(auth, client, signal, requestHeaders, caseId, newCase)

    const patientId = patient
    //Create Care Team
    const careTeam = await createCareTeam(
      auth,
      client,
      signal,
      { 'Content-Type': 'application/json' },
      { practitioner, patientId }
    )
    //Create Care Plan
    const careTeamId = careTeam.id
    addResourceToProject({ resourceId: careTeamId, resourceType: 'CareTeam' }, projectId)

    const carePlan = await createCarePlan(
      auth,
      client,
      signal,
      { 'Content-Type': 'application/json' },
      { careTeamId, patientId, ticketId, encounterId, caseId }
    )
    addResourceToProject({ resourceId: carePlan.id, resourceType: 'CarePlan' }, projectId)

    setShow(false)
    setRedirect(true)
  }

  const onOpen = () => {
    //do nothing
  }

  const onClose = () => {
    //do nothing
  }

  if (redirect) {
    return <Redirect to={`${basePath}/project/${projectId}`} />
  }

  return (
    <div className='main-content-container container-fluid px-4 vh-100'>
      <div className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'>
        <div className='col-8 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Create Case</span>
          <h3 className='page-title'>Case Management</h3>
          <a href={`${basePath}/project/${projectId}`}>{'< '}Back to Project</a>
        </div>
      </div>
      <form>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Select Patient</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <PatientSearch
              onOpen={onOpen}
              onClose={onClose}
              onSelect={selectPatient}
              auth={auth}
              projectId={projectId}
            />
            <br />
          </div>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <Link to={`${basePath}/project/${projectId}/patient`}>New Patient</Link>
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Case Title</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <textarea
              rows='1'
              name='case-title'
              id='case-title'
              className='form-control'
              onChange={(e) => setCaseTitle(e.target.value)}
            />
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Select Specialty</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <Form.Control
              as='select'
              name='specialty-select'
              id='speciality-select'
              onChange={(e) => setSpecialty(e.target.value)}
            >
              <option value={''}>{'Select...'}</option>
              {specialties.map((specialty) => {
                return (
                  <option value={specialty} key={specialty}>
                    {specialty}
                  </option>
                )
              })}
            </Form.Control>
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Select Priority</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <Form.Control
              as='select'
              name='priority-select'
              id='priority-select'
              onChange={(e) => setPriority(e.target.value)}
            >
              <option value={''}>{'Select...'}</option>
              {priorities.map((prio) => {
                return (
                  <option value={prio} key={prio}>
                    {prio}
                  </option>
                )
              })}
            </Form.Control>
          </div>
        </div>
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Select Visit</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <Form.Control
              as='select'
              name='visit-select'
              id='visit-select'
              onChange={(e) => setVisit(e.target.value)}
            >
              <option value='new-visit'>New Visit</option>
              {visits.map((visit) => {
                return (
                  <option value={visit.id} key={visit.id}>
                    {visit.period.start}
                  </option>
                )
              })}
            </Form.Control>
          </div>
        </div>
        {visit === 'new-visit' && (
          <>
            <div className='row'>
              <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
                <label>Select Visit Date</label>
              </div>
              <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
                <input
                  type='date'
                  name='visit-date'
                  id='visit-date'
                  className='form-control'
                  onChange={(e) => setDate(e.target.value)}
                />
              </div>
            </div>
            <div className='row'>
              <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
                <label>Reason for Visit</label>
              </div>
              <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
                <textarea
                  rows='3'
                  name='visit-reason'
                  id='visit-reason'
                  className='form-control'
                  onChange={(e) => setVisitReason(e.target.value)}
                />
              </div>
            </div>
          </>
        )}
        <div className='row'>
          <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
            <label>Pertinent Medical History/Case Details</label>
          </div>
          <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
            <textarea
              rows='5'
              name='case-details'
              id='case-details'
              className='form-control'
              onChange={(e) => setDetails(e.target.value)}
            />
          </div>
        </div>
        <button type='button' className='btn btn-lg btn-primary' onClick={submit}>
          Create Case
        </button>
      </form>
      <Modal className={`${styles.onTop}`} size='lg' show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Case ID {caseId} has been created. Do you also want to create a ticket for this case?
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={handleClose}>
            No
          </Button>
          <Button variant='primary' onClick={newTicket}>
            Yes
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}

export default Case
