import React, { useState, useEffect, useContext, useRef } from 'react'
import FhirContext from '../../context/FhirContext'
import { getCompositionByID, updateComposition } from '../../api/CompositionApi'
import { getEncounterByID } from '../../api/EncounterApi'
import { Form, Modal, Button, Spinner } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { createCarePlan } from '../../api/CarePlanApi'
import 'bootstrap/dist/css/bootstrap.min.css'
import { getObservationByID } from '../../api/ObservationApi'
import { addResourceToProject } from '../../api/ProjectApi'
import { createCareTeam } from '../../api/CareTeamApi'
import { createTicket } from '../../api/TicketApi'
import { translate, emotionalize, understand } from '../../api/AnalysisApi'
//import styles from './project-dashboard.module.css'

const Case = ({ basePath, projectId, caseId, practitioner, match, history, auth }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [patientCase, setCase] = useState(null)
  const [visit, setVisit] = useState(null)
  const [details, setDetails] = useState(null)
  const [isLoading, setIsLoading] = useState(false)
  const [showTicketModal, setShowTicketModal] = useState(false)
  const [showDictionModal, setShowDictionModal] = useState(false)
  const [specialty, setSpecialty] = useState('')
  const [priority, setPriority] = useState('')
  const [destinationLanguage, setDestinationLanguage] = useState('ENGLISH')
  const [sourceLanguage, setSourceLanguage] = useState('ENGLISH')
  const [file, setFile] = useState('')
  const [analysis, setAnalysis] = useState('')

  const languages = [
    'ARABIC',
    'CHINESE',
    'ENGLISH',
    'FRENCH',
    'GERMAN',
    'PORTUGUESE',
    'RUSSIAN',
    'SPANISH',
  ]

  const specialties = [
    'Cardiology',
    'Infectious Disease',
    'Dermatology',
    'Hematology',
    'Neurology',
    'Gastroenterology',
    'OBGYN',
    'Ophthalmology',
    'Pediatrics',
    'Family',
    'Psychiatry',
    'Pain',
    'Internal',
    'Allergy',
    'Radiology',
    'Endocrinology',
    'Nephrology',
    'Rheumatology',
  ]

  const priorities = ['low', 'normal', 'high']

  const newTicket = async () => {
    const caseId = patientCase.id
    const title = patientCase.title
    const ticket = await createTicket(
      auth,
      priority,
      specialty,
      title,
      visit.reasonCode[0].text,
      projectId,
      caseId
    )
    const ticketId = ticket.id

    let newCase = patientCase
    newCase.section.push({ title: 'ticket', entry: [{ reference: ticketId }] })

    await updateComposition(auth, client, signal, requestHeaders, patientCase.id, newCase)

    const patientId = patientCase.subject.reference.replace('Patient/', '')
    //Create Care Team
    const careTeam = await createCareTeam(
      auth,
      client,
      signal,
      { 'Content-Type': 'application/json' },
      { practitioner, patientId }
    )
    //Create Care Plan
    const careTeamId = careTeam.id
    addResourceToProject({ resourceId: careTeamId, resourceType: 'CareTeam' }, projectId)

    const encounterId = visit.id
    const carePlan = await createCarePlan(
      auth,
      client,
      signal,
      { 'Content-Type': 'application/json' },
      { careTeamId, patientId, ticketId, encounterId, caseId }
    )
    addResourceToProject({ resourceId: carePlan.id, resourceType: 'CarePlan' }, projectId)

    setShowTicketModal(false)
  }

  const onFileChange = (event) => {
    // Update the state
    setFile(event.target.files[0])
  }

  /*const createDiction = async (e) => {
    // Details of the uploaded file

    // Create an object of formData
    const formData = new FormData();

    // Update the formData object
    formData.append(
      "testFile",
      file,
      file.name
    );

    // Request made to the backend api
    // Send formData object
    await fetch(
      `https://4m0t4oye4j.execute-api.us-east-1.amazonaws.com/prod/upload`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${btoa(
            process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
            ':' +
            process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
          )}`,
        },
        body: formData,
      }
    )
    setShowDictionModal(false)
  }*/

  const translateDetails = async () => {
    const result = await translate(sourceLanguage, destinationLanguage, details.valueString)
    setAnalysis(result)
  }

  const emoteDetails = async () => {
    const result = await emotionalize(sourceLanguage, details.valueString)
    setAnalysis(result)
  }

  const understandDetails = async () => {
    const result = await understand(sourceLanguage, details.valueString)
    setAnalysis(result)
  }

  function groupBy(objectArray, property) {
    return objectArray.reduce((acc, obj) => {
      const key = obj[property]
      if (!acc[key]) {
        acc[key] = []
      }
      // Add object to list for given key's value
      acc[key].push(obj)
      return acc
    }, {})
  }

  const analyzeDetails = async () => {
    let result = await understand(sourceLanguage, details.valueString)
    result = JSON.parse(result)
    result = groupBy(result, 'category')
    let text = ''
    Object.keys(result).forEach((key) => {
      text = text + key + ': '
      let values = result[key]
      values.forEach((val, index) => {
        if (index < values.length - 1) text = text + val.text + ', '
        else text = text + val.text
      })
      text = text + '\n'
    })
    setAnalysis(text)
  }

  useEffect(() => {
    async function getCaseDetails() {
      if (!ready) return
      setIsLoading(true)
      const caseResult = await getCompositionByID(auth, client, signal, caseId, requestHeaders)
      setCase(caseResult)
      const encounterId = caseResult.encounter.reference.replace('Encounter/', '')
      const visitResult = await getEncounterByID(auth, client, signal, encounterId, requestHeaders)
      setVisit(visitResult)
      const observationId = caseResult.section[0].entry[0].reference.replace('Observation/', '')
      const observationResult = await getObservationByID(
        auth,
        client,
        signal,
        observationId,
        requestHeaders
      )
      setDetails(observationResult)
      setIsLoading(false)
    }

    getCaseDetails()

    return () => {
      abortController.abort()
    }
  }, [ready])

  let tickets = ''
  if (patientCase?.section.length > 2) {
    patientCase.section.forEach((sect, index) => {
      if (index > 1 && index < patientCase.section.length - 1) {
        tickets = tickets + sect.entry[0].reference + ', '
      } else if (index === patientCase.section.length - 1) {
        tickets = tickets + sect.entry[0].reference
      } else {
        // do nothing
      }
    })
  } else {
    tickets = 'N/A'
  }

  return (
    <div className='main-content-container container-fluid px-4 vh-100'>
      <div className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'>
        <div className='col-8 col-sm-4 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>View Case</span>
          <h3 className='page-title'>Case Details</h3>
          <a href={`${basePath}/project/${projectId}`}>{'< '}Back to Project</a>
        </div>
        <div className='col-8 col-sm-4 text-center text-sm-left mb-0'>
          <div className='text-right'>
            <Button onClick={() => setShowTicketModal(true)}>Create Ticket</Button>
            {/*<Button onClick={() => setShowDictionModal(true)}>Add Diction</Button>*/}
          </div>
        </div>
      </div>
      {!isLoading && (
        <>
          <div className='row'>
            <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
              <label>Case Title</label>
            </div>
            <div className='col-8 col-sm-10 text-center text-sm-left mb-0'>
              {patientCase?.title}
            </div>
          </div>
          <div className='row'>
            <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
              <label>Case ID</label>
            </div>
            <div className='col-8 col-sm-10 text-center text-sm-left mb-0'>{patientCase?.id}</div>
          </div>
          <div className='row'>
            <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
              <label>Tickets</label>
            </div>
            <div className='col-8 col-sm-10 text-center text-sm-left mb-0'>{tickets}</div>
          </div>
          <div className='row'>
            <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
              <label>Patient</label>
            </div>
            <div className='col-8 col-sm-10 text-center text-sm-left mb-0'>
              <Link to={`${basePath}/project/${projectId}/${patientCase?.subject?.reference}`}>
                {patientCase?.subject?.reference}
              </Link>
            </div>
          </div>
          <div className='row'>
            <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
              <label>Visit Date</label>
            </div>
            <div className='col-8 col-sm-10 text-center text-sm-left mb-0'>
              {visit ? visit.period.start : ''}
            </div>
          </div>
          <div className='row'>
            <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
              <label>Reason for Visit</label>
            </div>
            <div className='col-8 col-sm-10 text-center text-sm-left mb-0'>
              {visit ? visit.reasonCode[0].text : ''}
            </div>
          </div>
          <div className='row'>
            <div className='col-8 col-sm-2 text-center text-sm-left mb-0'>
              <label>Pertinent Medical History/Case Details</label>
            </div>
            <div className='col-8 col-sm-10 text-center text-sm-left mb-0'>
              {details ? details.valueString : ''}
            </div>
          </div>
          <hr />
          <div className='row'>
            <div className='col-8 col-sm-12 text-left text-sm-left mb-0'>
              <div className='text-left'>
                <Button onClick={() => translateDetails()}>Translate Details</Button>
                <Button onClick={() => emoteDetails()}>Emotionalize Details</Button>
                <Button onClick={() => understandDetails()}>Understand Details</Button>
                <Button onClick={() => analyzeDetails()}>Analyze Details</Button>
              </div>
            </div>
          </div>
          <br />
          <div className='row'>
            <div className='col-8 col-sm-3 text-center text-sm-left mb-0'>
              <label>Source Language</label>
            </div>
            <div className='col-8 col-sm-3 text-left text-sm-left mb-0'>
              <Form.Control
                as='select'
                name='source-language-select'
                id='source-language-select'
                onChange={(e) => setSourceLanguage(e.target.value)}
              >
                <option value={''}>{'Select...'}</option>
                {languages.map((language) => {
                  return (
                    <option value={language} key={language}>
                      {language}
                    </option>
                  )
                })}
              </Form.Control>
            </div>
            <div className='col-8 col-sm-3 text-center text-sm-left mb-0'>
              <label>Destination Language</label>
            </div>
            <div className='col-8 col-sm-3 text-left text-sm-left mb-0'>
              <Form.Control
                as='select'
                name='dest-language-select'
                id='dest-language-select'
                onChange={(e) => setDestinationLanguage(e.target.value)}
              >
                <option value={''}>{'Select...'}</option>
                {languages.map((language) => {
                  return (
                    <option value={language} key={language}>
                      {language}
                    </option>
                  )
                })}
              </Form.Control>
            </div>
          </div>
          <br />
          <div className='row'>
            <div className='col-8 col-sm-12 text-left text-sm-left mb-0'>
              <Form>
                <Form.Group>
                  <Form.Control
                    as='textarea'
                    rows='10'
                    name='analysis'
                    readOnly={true}
                    value={analysis}
                  />
                </Form.Group>
              </Form>
            </div>
          </div>
          <Modal show={showTicketModal} onHide={() => setShowTicketModal(false)}>
            <Modal.Header closeButton>
              <Modal.Title>Create Ticket</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>
                <label>Select Specialty</label>
              </div>
              <Form.Control
                as='select'
                name='specialty-select'
                id='speciality-select'
                onChange={(e) => setSpecialty(e.target.value)}
              >
                <option value={''}>{'Select...'}</option>
                {specialties.map((spec) => {
                  return (
                    <option value={spec} key={spec}>
                      {spec}
                    </option>
                  )
                })}
              </Form.Control>
              <br />
              <div>
                <label>Select Priority</label>
              </div>
              <Form.Control
                as='select'
                name='priority-select'
                id='priority-select'
                onChange={(e) => setPriority(e.target.value)}
              >
                <option value={''}>{'Select...'}</option>
                {priorities.map((prio) => {
                  return (
                    <option value={prio} key={prio}>
                      {prio}
                    </option>
                  )
                })}
              </Form.Control>
            </Modal.Body>
            <Modal.Footer>
              <Button variant='secondary' onClick={() => setShowTicketModal(false)}>
                Close
              </Button>
              <Button variant='primary' onClick={newTicket}>
                Create Ticket
              </Button>
            </Modal.Footer>
          </Modal>
          <Modal show={showDictionModal} onHide={() => setShowDictionModal(false)}>
            <Modal.Header closeButton>
              <Modal.Title>Add Diction</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>
                <label>Upload File</label>
              </div>
              <input className='form-control' type='file' onChange={onFileChange} />
            </Modal.Body>
            <Modal.Footer>
              <Button variant='secondary' onClick={() => setShowDictionModal(false)}>
                Close
              </Button>
              {/*<Button variant='primary' onClick={createDiction}>
                Create Diction
              </Button>*/}
            </Modal.Footer>
          </Modal>
        </>
      )}
      {isLoading && (
        <div className='text-center'>
          <Spinner animation='border' variant='primary' />
        </div>
      )}
    </div>
  )
}

export default Case
