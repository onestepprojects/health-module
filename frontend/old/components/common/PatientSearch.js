import React, { useState, useEffect, useContext } from 'react'
import Autosuggest from 'react-autosuggest'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faQrcode } from '@fortawesome/free-solid-svg-icons'
import ProjectContext from '../../context/ProjectContext'
import { getProject } from '../../api/ProjectApi'

import { getPatientByID } from '../../api/PatientApi'

import theme from './AutosuggestThemeGeneric.module.css'
import styles from './search.generic.module.css'
import FhirContext from '../../context/FhirContext'

const PatientSearch = ({ onOpen, onClose, onSelect, auth, projectId }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)
  const projectContext = useContext(ProjectContext)
  const projectClient = projectContext.client
  const projectRequestHeaders = projectContext.requestHeaders
  const projectReady = projectContext.ready

  const [loading, setLoading] = useState(true)
  const [value, setValue] = useState('')
  const [patients, setPatients] = useState([])
  const [allPatients, setAllPatients] = useState([])
  const [project, setProject] = useState({})

  useEffect(() => {
    async function gatherPatients() {
      if (!projectReady) return
      const result = await getProject(auth, projectClient, signal, projectId, projectRequestHeaders)
      let allPats = result.project.fhirResources?.filter((elem) => elem.resourceType === 'Patient')
      allPats = allPats.map((patient) => {
        return new Promise((res, rej) => {
          getPatientByID(auth, client, signal, patient.resourceId, requestHeaders)
            .then((r) => {
              res(r)
            })
            .catch((e) => {
              console.error(e)
              res()
            })
        })
      })
      Promise.all(allPats).then((results) => {
        setAllPatients(results)
        setLoading(false)
      })
    }

    gatherPatients()

    return () => {
      abortController.abort()
    }
  }, [projectReady])

  // Teach Autosuggest how to calculate suggestions for any given input value.
  const getSuggestions = (value) => {
    const inputValue = value.trim().toLowerCase()
    const inputLength = inputValue.length

    return inputLength === 0
      ? []
      : allPatients.filter((patient) => {
          if (patient.name)
            return patient.name[0].given[0].toLowerCase().slice(0, inputLength) === inputValue
          else return false
        })
  }

  // When suggestion is clicked, Autosuggest needs to populate the input
  // based on the clicked suggestion. Teach Autosuggest how to calculate the
  // input value for every given suggestion.
  const getSuggestionValue = (suggestion) =>
    `${suggestion.name ? suggestion.name[0].given[0] : 'N/A'} ${
      suggestion.name ? suggestion.name[0].family : 'N/A'
    } | ${suggestion.gender} | ${suggestion.birthDate}`

  const renderSuggestion = (suggestion) => (
    <span>{`${suggestion.name ? suggestion.name[0].given[0] : 'N/A'} ${
      suggestion.name ? suggestion.name[0].family : 'N/A'
    }  | ${suggestion.gender} | ${suggestion.birthDate}`}</span>
  )

  const onChange = (event, { newValue }) => {
    setValue(newValue)
  }

  const onSuggestionsFetchRequested = ({ value }) => {
    setPatients(getSuggestions(value))
    onOpen()
  }

  const onSuggestionsClearRequested = () => {
    setPatients([])
    onClose()
  }

  const onSuggestionSelected = (event, { suggestion }) => {
    onSelect(suggestion.id)
  }

  const renderInputComponent = (inputProps) => (
    <React.Fragment>
      <FontAwesomeIcon icon={faSearch} className={styles.searchIcon} />
      <input {...inputProps} />
    </React.Fragment>
  )

  const inputProps = {
    placeholder: 'Patient Search',
    value,
    onChange: onChange,
    disabled: loading,
  }

  return (
    <React.Fragment>
      <Autosuggest
        suggestions={patients}
        onSuggestionsFetchRequested={onSuggestionsFetchRequested}
        onSuggestionsClearRequested={onSuggestionsClearRequested}
        onSuggestionSelected={onSuggestionSelected}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        renderInputComponent={renderInputComponent}
        inputProps={inputProps}
        theme={theme}
      />
    </React.Fragment>
  )
}

export default PatientSearch
