import React, { useState, useEffect } from 'react'
import Autosuggest from 'react-autosuggest'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios'

import theme from './AutosuggestThemeGeneric.module.css'
import styles from './search.generic.module.css'

const ListSearch = ({ placeholder, choiceList, inputData, onDataSelected, onType }) => {
  const [value, setValue] = useState('')
  const [data, setData] = useState([])

  // Teach Autosuggest how to calculate suggestions for any given input value.
  const storeSuggestions = (value) => {
    setData(choiceList.filter((choice) => choice.toLowerCase().indexOf(value.toLowerCase()) !== -1))
  }

  // Teach Autosuggest how to calculate suggestions for any given input value.
  const getSuggestions = (value) => {
    const inputValue = value.trim().toLowerCase()
    const inputLength = inputValue.length

    return inputLength === 0
      ? inputData
      : inputData.filter((datum) => datum.toLowerCase().slice(0, inputLength) === inputValue)
  }

  // When suggestion is clicked, Autosuggest needs to populate the input
  // based on the clicked suggestion. Teach Autosuggest how to calculate the
  // input value for every given suggestion.
  const getSuggestionValue = (suggestion) => suggestion

  const renderSuggestion = (suggestion) => <span>{suggestion}</span>

  const onChange = (event, { newValue }) => {
    setValue(newValue)
    onType(newValue)
  }

  const onSuggestionsFetchRequested = ({ value }) => {
    if (inputData.length != 0) {
      setData(getSuggestions(value))
    } else {
      storeSuggestions(value)
    }
  }

  const onSuggestionsClearRequested = () => {
    setData([])
  }

  const onSuggestionSelected = (event, { suggestionIndex }) => {
    onDataSelected({ suggestionIndex: suggestionIndex, allSuggestions: choiceList })
  }

  const shouldRenderSuggestions = (value, reason) => {
    if (choiceList !== '') {
      return true
    }

    if (reason == 'input-focused') {
      return true
    }

    return false
  }

  const renderInputComponent = (inputProps) => (
    <React.Fragment>
      <FontAwesomeIcon icon={faSearch} className={styles.searchIcon} />
      <input {...inputProps} />
    </React.Fragment>
  )

  const inputProps = {
    placeholder: placeholder,
    value,
    onChange: onChange,
    disabled: () => {
      data.length == 0 && inputData.length == 0 ? 'true' : 'false'
    },
  }

  return (
    <React.Fragment>
      <Autosuggest
        suggestions={data}
        onSuggestionsFetchRequested={onSuggestionsFetchRequested}
        onSuggestionsClearRequested={onSuggestionsClearRequested}
        onSuggestionSelected={onSuggestionSelected}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        renderInputComponent={renderInputComponent}
        shouldRenderSuggestions={shouldRenderSuggestions}
        inputProps={inputProps}
        theme={theme}
      />
    </React.Fragment>
  )
}

ListSearch.defaultProps = {
  inputData: [],
  onDataSelected: () => {},
}

export default ListSearch
