import React from 'react'

const EditableFormField = ({
  inputType,
  StaticType,
  text,
  editable,
  props
}) => {
  const { className = '', ...otherProps } = props || {}
  const getRenderedField = () => {
    if (editable) {
      return (
        <input
          type={inputType}
          placeholder={text}
          className={`form-control ${className}`}
          {...props}
        />
      )
    } else {
      return (
        <StaticType className={className} {...props}>
          {text}
        </StaticType>
      )
    }
  }

  return getRenderedField()
}

export default EditableFormField
