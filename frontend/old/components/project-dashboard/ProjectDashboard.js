import React, { useState, useEffect, useContext } from 'react'
import ProjectInfo from './ProjectInfo'
import PatientWidget from './PatientWidget'
import CaseWidget from './CaseWidget'
import ProjectContext from '../../context/ProjectContext'
import { getProject } from '../../api/ProjectApi'
import { Tabs, Tab } from 'react-bootstrap'

import 'bootstrap/dist/css/bootstrap.min.css'

const ProjectDashboard = ({ basePath, projectId, match, history, auth }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(ProjectContext)

  const [thisProject, setThisProject] = useState([])
  const [key, setKey] = useState('patients')

  const pid = projectId

  useEffect(() => {
    async function gatherProjectInfo() {
      if (!ready) return

      const result = await getProject(auth, client, signal, pid, requestHeaders)
      setThisProject(result.project)
    }

    gatherProjectInfo()

    return () => {
      abortController.abort()
    }
  }, [ready])

  const projectName = thisProject.name
  const projectDescription = thisProject.description
  const projectManager = thisProject.manager
  const projectStartDate = thisProject.startDate
  const projectEndDate = thisProject.endDate
  const projectPatients = thisProject.fhirResources
    ? thisProject.fhirResources.filter((resource) => resource.resourceType === 'Patient')
    : []
  const projectCases = thisProject.fhirResources
    ? thisProject.fhirResources.filter((resource) => resource.resourceType === 'Composition')
    : []

  return (
    <div className='main-content-container container-fluid px-4 vh-100'>
      <div className='page-header row no-gutters py-4 mb-3 border-bottom justify-content-between'>
        <div className='col-8 col-sm-6 text-center text-sm-left mb-0'>
          <span className='text-uppercase page-subtitle'>Health Project Dashboard</span>
          <h3 className='page-title'>{projectName}</h3>
          <h6>Project ID {pid}</h6>
          <a href={`${basePath}`}>{'< '}Back to Projects</a>
        </div>
      </div>
      <div className='btn-group' role='group'></div>
      <div className='container-fluid'>
        <div className='row'>
          <ProjectInfo
            description={projectDescription}
            manager={projectManager}
            start={projectStartDate}
            end={projectEndDate}
          />
        </div>

        <Tabs
          id='controlled-tab-example'
          activeKey={key}
          onSelect={(k) => setKey(k)}
          className='mb-3'
        >
          <Tab eventKey='patients' title='Patients'>
            <PatientWidget
              match={match}
              auth={auth}
              projectId={pid}
              patients={projectPatients}
              basePath={basePath}
            />
          </Tab>
          <Tab eventKey='cases' title='Cases'>
            <CaseWidget match={match} auth={auth} projectCases={projectCases} projectId={pid} />
          </Tab>
        </Tabs>
      </div>
    </div>
  )
}

export default ProjectDashboard
