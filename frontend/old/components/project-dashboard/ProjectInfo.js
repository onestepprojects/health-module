import React, { useState } from 'react'
import moment from 'moment'

import styles from './project-dashboard.module.css'

const ProjectInfo = ({ description, manager, start, end }) => {
  var startDate = new Date(parseInt(start))
  var endDate = new Date(parseInt(end))

  const dateInWords = (date) => {
    if (date === '') {
      return ''
    }

    const dateParsed = new Date(date)

    return moment(dateParsed).format('MMMM D, YYYY')
  }

  return (
    <React.Fragment>
      <div className='col-6 pb-4'>
        <div className='card card-small'>
          <div className={`card-header ${styles.hsize}`}>
            <h6 className='card-title'>Description</h6>
          </div>
          <div className='card-body d-flex flex-column'>
            <p className='nom tj'>{description}</p>
          </div>
        </div>
      </div>
      <div className='col-2 pb-4'>
        <div className='card card-small'>
          <div className={`card-header ${styles.hsize}`}>
            <h6 className='card-title'>Manager</h6>
          </div>
          <div className='card-body d-flex flex-column'>
            <p className='nom tj'>{manager}</p>
          </div>
        </div>
      </div>
      <div className='col-2 pb-4'>
        <div className='card card-small'>
          <div className={`card-header ${styles.hsize}`}>
            <h6 className='card-title'>Start Date</h6>
          </div>
          <div className='card-body d-flex flex-column'>
            <p className='nom tj'>{dateInWords(startDate.toString())}</p>
          </div>
        </div>
      </div>
      <div className='col-2 pb-4'>
        <div className='card card-small'>
          <div className={`card-header ${styles.hsize}`}>
            <h6 className='card-title'>End Date</h6>
          </div>
          <div className='card-body d-flex flex-column'>
            <p className='nom tj'>{dateInWords(endDate.toString())}</p>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}
export default ProjectInfo
