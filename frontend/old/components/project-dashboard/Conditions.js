import React, { useState, useEffect, useContext } from 'react'
import { Bar } from 'react-chartjs-2'
import FhirContext from '../../context/FhirContext'
import { getConditions } from '../../api/ConditionsApi'

import styles from './project-dashboard.module.css'

const Conditions = ({ auth, resources }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [conditions, setConditions] = useState('')
  const [topConditions, setTopConditions] = useState([])

  const state = {
    labels: [...topConditions?.map((element) => element.name).slice(0, 6)],
    datasets: [
      {
        data: [...topConditions?.map((element) => element.count).slice(0, 6)],
        backgroundColor: 'rgba(83, 51, 237, 1)'
      }
    ]
  }

  useEffect(() => {
    if (!ready) return
    getConditions(auth, client, signal, resources, requestHeaders).then(
      (result) => {
        setConditions(result)
        setTopConditions(getTopConditions(result))
      }
    )

    return () => {
      abortController.abort()
    }
  }, [ready, resources])

  const getTopConditions = (allConditions) => {
    let occurence = []
    let count = 0
    let uniqueConditionChecker = []
    for (let i = 0; i < allConditions?.length; i++) {
      if (!uniqueConditionChecker.includes(allConditions[i])) {
        for (let j = 0; j < allConditions?.length; j++) {
          if (allConditions[i] === allConditions[j]) {
            count++
          }
        }
        occurence.push({ id: i, name: allConditions[i], count })
      }
      uniqueConditionChecker.push(allConditions[i])
      count = 0
    }

    let topConditionsData = occurence
      .filter((element) => {
        return element.name !== undefined
      })
      .sort((a, b) => {
        if (a.count < b.count) return -1
        if (a.count > b.count) return 1
        return 0
      })
      .reverse()
    return topConditionsData
  }

  return (
    <div>
      <div className='card card-small'>
        <div className={`card-header ${styles.hsize}`}>
          <h6 className='card-title'>Conditions</h6>
        </div>
        <div className='card-body d-flex flex-column'>
          <div className='schart'>
            <Bar
              data={state}
              options={{
                legend: {
                  display: false
                },
                scales: {
                  yAxes: [
                    {
                      ticks: {
                        beginAtZero: true
                      }
                    }
                  ]
                }
              }}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Conditions
