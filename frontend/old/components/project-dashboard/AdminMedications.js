import React, { useState, useEffect, useContext } from 'react'
import { Bar } from 'react-chartjs-2'
import FhirContext from '../../context/FhirContext'
import { getAdminMedications } from '../../api/AdminMedicationsApi'

import styles from './project-dashboard.module.css'

const AdminMedications = ({ auth, resources }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [medicines, setMedicines] = useState('')
  const [topMedicines, setTopMedicines] = useState([])

  const state = {
    labels: [...topMedicines?.map((element) => element.name).slice(0, 6)],
    datasets: [
      {
        data: [...topMedicines?.map((element) => element.count).slice(0, 6)],
        backgroundColor: 'rgba(83, 51, 237, 1)'
      }
    ]
  }

  useEffect(() => {
    if (!ready) return
    getAdminMedications(auth, client, signal, resources, requestHeaders).then(
      (result) => {
        setMedicines(result)
        setTopMedicines(getTopMedicines(result))
      }
    )

    return () => {
      abortController.abort()
    }
  }, [ready, resources])

  const getTopMedicines = (allMedicines) => {
    let occurence = []
    let count = 0
    let uniqueMedicineChecker = []
    for (let i = 0; i < allMedicines?.length; i++) {
      if (!uniqueMedicineChecker.includes(allMedicines[i])) {
        for (let j = 0; j < allMedicines?.length; j++) {
          if (allMedicines[i] === allMedicines[j]) {
            count++
          }
        }
        occurence.push({ id: i, name: allMedicines[i], count })
      }
      uniqueMedicineChecker.push(allMedicines[i])
      count = 0
    }

    let topMedicinesData = occurence
      .filter((element) => {
        return element.name !== undefined
      })
      .sort((a, b) => {
        if (a.count < b.count) return -1
        if (a.count > b.count) return 1
        return 0
      })
      .reverse()
    return topMedicinesData
  }

  return (
    <div>
      <div className='card card-small'>
        <div className={`card-header ${styles.hsize}`}>
          <h6 className='card-title'>Administered Medications</h6>
        </div>
        <div className='card-body d-flex flex-column'>
          <div className='schart'>
            <Bar
              data={state}
              options={{
                legend: {
                  display: false
                },
                scales: {
                  yAxes: [
                    {
                      ticks: {
                        beginAtZero: true
                      }
                    }
                  ]
                }
              }}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default AdminMedications
