import React, { useState, useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { ListGroup, Button, Spinner } from 'react-bootstrap'
import FhirContext from '../../context/FhirContext'
import { getPatientByID } from '../../api/PatientApi'

import styles from './project-dashboard.module.css'
import { filter } from 'lodash'

const PatientWidget = ({ match, auth, projectId, patients, basePath }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [loading, setLoading] = useState(false)
  const [allPatients, setAllPatients] = useState([])
  const [searchTerm, setSearch] = useState([])
  const [displayPatients, setDisplayPatients] = useState([])

  useEffect(() => {
    async function gatherPatients() {
      if (!ready) return
      setLoading(true)
      let allPats = patients.map((patient) => {
        return new Promise((res, rej) => {
          getPatientByID(auth, client, signal, patient.resourceId, requestHeaders)
            .then((r) => {
              res(r)
            })
            .catch((e) => {
              console.error(e)
              res()
            })
        })
      })
      Promise.all(allPats).then((results) => {
        setAllPatients(results)
        setDisplayPatients(results)
        setLoading(false)
      })
    }

    gatherPatients()

    return () => {
      abortController.abort()
    }
  }, [patients])

  const calculateAge = (date) => {
    const birthDate = new Date(date)
    const ageDifMs = Date.now() - birthDate.getTime()
    const ageDate = new Date(ageDifMs)

    return Math.abs(ageDate.getUTCFullYear() - 1970)
  }

  const search = () => {
    setDisplayPatients(
      allPatients.filter((elem) => {
        console.log(elem)
        if (
          elem.name
            ? elem.name[0].given[0].toLowerCase().includes(searchTerm.toLowerCase())
            : false || elem.name
            ? elem.name[0].family.toLowerCase().includes(searchTerm.toLowerCase())
            : false || elem.id.toLowerCase().includes(searchTerm.toLowerCase())
        )
          return elem
      })
    )
  }

  const clear = () => {
    setDisplayPatients(allPatients)
  }

  return (
    <React.Fragment>
      <div className='card'>
        <div className={`card-header ${styles.hsize}`}>
          <div className='row'>
            <div className='col-5'>
              <input
                className='form-control'
                type='text'
                placeholder='Search'
                aria-label='Search'
                onChange={(e) => setSearch(e.target.value)}
              />
            </div>
            <div className='col-1'>
              <Button onClick={() => search()}>Search</Button>
            </div>
            <div className='col-1'>
              <Button onClick={() => clear()}>Clear</Button>
            </div>
            <div className='col-5'>
              <h6 className='card-title text-right'>
                <Link to={`${match.url}/patient`}>(+)</Link>
              </h6>
            </div>
          </div>
        </div>
        <div className='card-body d-flex flex-column'>
          {!loading && displayPatients.length === 0 && (
            <div className='text-center'>No Patients have been added to this Project.</div>
          )}
          {!loading && (
            <div className={`${styles.scrollableDiv}`}>
              <ListGroup>
                {displayPatients.map((elem, index) => {
                  return (
                    <Link key={index} to={`${basePath}/project/${projectId}/patient/${elem.id}`}>
                      <ListGroup.Item>{`${elem.name ? elem.name[0].given : 'N/A'} ${
                        elem.name ? elem.name[0].family : 'N/A'
                      } | ${elem.gender} | Age ${calculateAge(elem.birthDate)}`}</ListGroup.Item>
                    </Link>
                  )
                })}
              </ListGroup>
            </div>
          )}
          {loading && (
            <div className='text-center'>
              <Spinner animation='border' variant='primary' />
            </div>
          )}
        </div>
      </div>
    </React.Fragment>
  )
}
export default PatientWidget
