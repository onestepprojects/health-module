import React, { useState, useEffect, useContext } from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { ListGroup, Button, Spinner } from 'react-bootstrap'
import FhirContext from '../../context/FhirContext'
import { getCompositionByID } from '../../api/CompositionApi'

import styles from './project-dashboard.module.css'

const CaseWidget = ({ match, auth, projectCases, projectId }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [cases, setCases] = useState([])
  const [searchTerm, setSearch] = useState([])
  const [displayCases, setDisplayCases] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    async function getCases() {
      if (!ready) return
      setLoading(true)
      let allCases = projectCases.map((composition) => {
        return new Promise((res, rej) => {
          getCompositionByID(auth, client, signal, composition.resourceId, requestHeaders)
            .then((r) => {
              res(r)
            })
            .catch((e) => {
              console.error(e)
              res()
            })
        })
      })
      Promise.all(allCases).then((results) => {
        setCases(results)
        setDisplayCases(results)
        setLoading(false)
      })
    }

    getCases()

    return () => {
      abortController.abort()
    }
  }, [projectCases])

  const search = () => {
    setDisplayCases(
      cases.filter((elem) => {
        if (
          elem.id.toLowerCase().includes(searchTerm.toLowerCase()) ||
          elem.title.toLowerCase().includes(searchTerm.toLowerCase())
        )
          return elem
      })
    )
  }

  const clear = () => {
    setDisplayCases(cases)
  }

  return (
    <React.Fragment>
      <div className='card'>
        <div className={`card-header ${styles.hsize}`}>
          <div className='row'>
            <div className='col-5'>
              <input
                className='form-control'
                type='text'
                placeholder='Search'
                aria-label='Search'
                onChange={(e) => setSearch(e.target.value)}
              />
            </div>
            <div className='col-1'>
              <Button onClick={() => search()}> Search</Button>
            </div>
            <div className='col-1'>
              <Button onClick={() => clear()}>Clear</Button>
            </div>
            <div className='col-5'>
              <h6 className='card-title text-right'>
                <Link to={`${match.url}/case`}>(+)</Link>
              </h6>
            </div>
          </div>
        </div>
        <div className='card-body d-flex flex-column'>
          {!loading && displayCases.length === 0 && (
            <div className='text-center'>No Cases have been added to this Project.</div>
          )}
          {!loading && (
            <div className={`${styles.scrollableDiv}`}>
              <ListGroup>
                {displayCases
                  .sort((a, b) => (a.date < b.date ? 1 : -1))
                  .map((elem, index) => {
                    return (
                      <Link key={index} to={`${match.url}/case/${elem.id}`}>
                        <ListGroup.Item>
                          {new Date(elem.date).toString()} | {elem.title}
                        </ListGroup.Item>
                      </Link>
                    )
                  })}
              </ListGroup>
            </div>
          )}
          {loading && (
            <div className='text-center'>
              <Spinner animation='border' variant='primary' />
            </div>
          )}
        </div>
      </div>
    </React.Fragment>
  )
}
export default CaseWidget
