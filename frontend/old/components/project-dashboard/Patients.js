import React, { useState } from 'react'
import PatientSearch from '../common/PatientSearch'

import styles from './project-dashboard.module.css'

const Patients = ({ match, history, auth }) => {
  const [zIndex, setZIndex] = useState(3)

  const onOpen = () => {
    setZIndex(1)
  }

  const onClose = () => {
    setZIndex(3)
  }

  return (
    <div>
      <div className={`d-flex mt-5 ${styles.search}`}>
        <PatientSearch
          onOpen={onOpen}
          onClose={onClose}
          basePath={`/health/`}
          history={history}
          auth={auth}
        />
      </div>
    </div>
  )
}

export default Patients
