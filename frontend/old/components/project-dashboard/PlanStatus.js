import React, { useState, useEffect, useContext } from 'react'
import { Pie } from 'react-chartjs-2'
import FhirContext from '../../context/FhirContext'
import { getPlanStatus } from '../../api/PlanStatusApi'

import styles from './project-dashboard.module.css'

const PlanStatus = ({ auth, resources }) => {
  const abortController = new AbortController()
  const signal = abortController.signal

  const { ready, client, requestHeaders } = useContext(FhirContext)

  const [planStatus, setPlanStatus] = useState([])
  const [chartData, setChartData] = useState({})

  useEffect(() => {
    if (!ready) return
    getPlanStatus(auth, client, signal, resources, requestHeaders).then(
      (result) => {
        setPlanStatus(result)
      }
    )

    return () => {
      abortController.abort()
    }
  }, [ready, resources])

  const chart = (ps) => {
    let act = 0
    let com = 0

    for (const item in ps) {
      if (String(ps[item]) === 'active') {
        act += 1
      } else if (String(ps[item]) === 'completed') {
        com += 1
      }
    }

    setChartData({
      labels: ['Active', 'Completed'],
      datasets: [
        {
          data: [act, com],
          backgroundColor: ['rgba(83, 51, 237, 1)', 'rgba(129, 207, 224, 1)']
        }
      ]
    })
  }

  useEffect(() => {
    chart(planStatus)
  }, [planStatus])

  return (
    <div>
      <div className='card card-small'>
        <div className={`card-header ${styles.hsize}`}>
          <h6 className='card-title'>Health Plan Status</h6>
        </div>
        <div className='card-body d-flex flex-column'>
          <div className='fchart'>
            <Pie
              data={chartData}
              options={{
                legend: {
                  align: 'start',
                  position: 'bottom'
                }
              }}
            ></Pie>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PlanStatus
