import React from 'react'

const FhirContext = React.createContext({
  ready: false,
  client: undefined,
  requestHeaders: {
    'content-type': 'application/json'
  }
})

export default FhirContext
