import React from 'react'

const ProjectContext = React.createContext({
  ready: false,
  client: undefined,
  // authToken: '',
  requestHeaders: {
    // 'x-api-key': process.env.REACT_APP_HEALTH_MODULE_PROJECTS_API_KEY,
    // Authorization: ''
    'content-type': 'application/json'
  }
})

export default ProjectContext
