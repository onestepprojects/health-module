import React, { useState, useEffect } from 'react'
import ProjectContext from './ProjectContext'
import FHIR from 'fhirclient'

const ProjectProvider = ({ children }) => {
  const [client, setClient] = useState(null)
  const [ready, setReady] = useState(false)
  const [requestHeaders, setRequestHeaders] = useState(null)

  useEffect(() => {
    setClient(FHIR.client(process.env.REACT_APP_HEALTH_MODULE_PROJECTS_API))
    //TODO: Change to use authToken when backend uses project auth
    setRequestHeaders({
      // 'x-api-key': process.env.REACT_APP_HEALTH_MODULE_PROJECTS_API_KEY,
      // Authorization: token
      'Content-Type': 'application/json',
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    })
    setReady(true)
  }, [])

  return (
    <ProjectContext.Provider
      value={{
        ready: ready,
        client: client,
        requestHeaders: requestHeaders,
        // authToken: token
      }}
    >
      {children}
    </ProjectContext.Provider>
  )
}

export default ProjectProvider
