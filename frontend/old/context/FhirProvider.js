import React, { useState, useEffect } from 'react'
import FhirContext from './FhirContext'
import FHIR from 'fhirclient'

const FhirProvider = ({ children }) => {
  const [client, setClient] = useState(null)
  const [ready, setReady] = useState(false)
  const [requestHeaders, setRequestHeaders] = useState(null)

  useEffect(() => {
    setClient(FHIR.client(process.env.REACT_APP_HEALTH_MODULE_FHIR_API))
    setRequestHeaders({
      'content-type': 'application/json',
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    })

    setReady(true)
  }, [])

  return (
    <FhirContext.Provider
      value={{
        ready: ready,
        client: client,
        requestHeaders: requestHeaders,
      }}
    >
      {children}
    </FhirContext.Provider>
  )
}

export default FhirProvider
