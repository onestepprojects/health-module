import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'

import React from 'react'

import { BrowserRouter as Router } from 'react-router-dom'

// Configure enzyme for react 16
Enzyme.configure({ adapter: new Adapter() })

describe('Health', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<Router />)
    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
