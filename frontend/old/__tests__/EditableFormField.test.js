import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'

import React from 'react'

import EditableFormField from '../components/common/EditableFormField'

// Configure enzyme for react 16
Enzyme.configure({ adapter: new Adapter() })

describe('EditableFormField', () => {
  const wrapperEditable = shallow(
    <EditableFormField
      inputType='text'
      text='Placeholder'
      editable
      StaticType='span'
      props={{ className: 'mt-auto' }}
    />
  )

  const wrapperNotEditable = shallow(
    <EditableFormField
      inputType='text'
      text='Placeholder'
      editable={false}
      StaticType='span'
    />
  )

  it('should render correctly', () => {
    expect(toJson(wrapperEditable)).toMatchSnapshot()
  })

  it('should render an input field if editable is true', () => {
    expect(wrapperEditable.find('input')).toHaveLength(1)
  })

  it('should render a span if editable is false', () => {
    expect(wrapperNotEditable.find('span')).toHaveLength(1)
  })

  it('should include a class name if provided', () => {
    expect(wrapperEditable.find('input').hasClass('mt-auto')).toBe(true)
  })
})
