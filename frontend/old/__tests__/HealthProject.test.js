import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'

import React from 'react'

import HealthProject from '../components/project-management/HealthProject'

// Configure enzyme for react 16
Enzyme.configure({ adapter: new Adapter() })

describe('HealthProject', () => {
  const name = 'Health Project'
  const description = 'Health Project Description'

  const wrapper = shallow(
    <HealthProject
      project={{ name: name, description: description }}
      zIndex={1}
    />
  )

  it('should render correctly', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should have the project name in the card', () => {
    expect(wrapper.text().includes(name)).toBe(true)
  })

  it('should have the project description in the card', () => {
    expect(wrapper.text().includes(description)).toBe(true)
  })

  it('should have three buttons', () => {
    expect(wrapper.find('button')).toHaveLength(3)
  })
})
