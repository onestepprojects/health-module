import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'

import React from 'react'

import ManageHealth from '../components/project-management/ManageHealth'
import HealthProject from '../components/project-management/HealthProject'
import PatientSearch from '../components/common/PatientSearch'

// Configure enzyme for react 16
Enzyme.configure({ adapter: new Adapter() })

describe('ManageHealth', () => {
  const projects = [
    { name: 'Health Project 1', description: 'Description of Project 1' },
    { name: 'Health Project 2', description: 'Description of Project 2' }
  ]

  const patients = [
    {
      lastName: 'Erdman',
      firstName: 'Brain',
      gender: 'male',
      city: 'Sherborn',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Kshlerin',
      firstName: 'Danae',
      gender: 'female',
      city: 'Salem',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Grant',
      firstName: 'Aundrea',
      gender: 'female',
      city: 'Pittsfield',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Cummings',
      firstName: 'Barrett',
      gender: 'male',
      city: 'Boston',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Montañez',
      firstName: 'Clara',
      gender: 'female',
      city: 'Southborough',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Bernhard',
      firstName: 'Bruno',
      gender: 'male',
      city: 'Boston',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Douglas',
      firstName: 'Bradly',
      gender: 'male',
      city: 'Springfield',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Breitenberg',
      firstName: 'Bryan',
      gender: 'male',
      city: 'Watertown Town',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Eichmann',
      firstName: 'Blake',
      gender: 'male',
      city: 'Tewksbury',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Fonseca',
      firstName: 'Agustín',
      gender: 'male',
      city: 'Fall River',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Emmerich',
      firstName: 'Casandra',
      gender: 'female',
      city: 'Boston',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Franecki',
      firstName: 'Austin',
      gender: 'male',
      city: 'Chicopee',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Cantú',
      firstName: 'Catalina',
      gender: 'female',
      city: 'Needham',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Jacobson',
      firstName: 'Brant',
      gender: 'male',
      city: 'Lowell',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Schmeler',
      firstName: 'Birgit',
      gender: 'female',
      city: 'Foxborough',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Pfannerstill',
      firstName: 'Bradly',
      gender: 'male',
      city: 'Braintree Town',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Tremblay',
      firstName: 'Addie',
      gender: 'female',
      city: 'Springfield',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Reynolds',
      firstName: 'Britany',
      gender: 'female',
      city: 'Norwell',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Kshlerin',
      firstName: 'Dannie',
      gender: 'male',
      city: 'Brockton',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Von',
      firstName: 'Chadwick',
      gender: 'male',
      city: 'New Bedford',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Lucio',
      firstName: 'Benito',
      gender: 'male',
      city: 'Boston',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Christiansen',
      firstName: 'Cherrie',
      gender: 'female',
      city: 'Worcester',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Johns',
      firstName: 'Daisey',
      gender: 'female',
      city: 'Rowley',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Salazar',
      firstName: 'Beatriz',
      gender: 'female',
      city: 'Lynn',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Howe',
      firstName: 'Angelita',
      gender: 'female',
      city: 'Medford',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Witting',
      firstName: 'Catina',
      gender: 'female',
      city: 'Cambridge',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Champlin',
      firstName: 'Basil',
      gender: 'male',
      city: 'Framingham',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Runte',
      firstName: 'Isabel',
      gender: 'female',
      city: 'Boston',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Skiles',
      firstName: 'Darin',
      gender: 'male',
      city: 'Easton',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Hirthe',
      firstName: 'Pete',
      gender: 'male',
      city: 'Wakefield',
      state: 'Massachusetts',
      country: 'US'
    },
    {
      lastName: 'Quigley',
      firstName: 'Renea',
      gender: 'female',
      city: 'Southbridge Town',
      state: 'Massachusetts',
      country: 'US'
    }
  ]

  const wrapper = shallow(
    <ManageHealth projects={projects} allPatients={patients} />
  )

  it('should render correctly', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should render a search component', () => {
    expect(wrapper.containsMatchingElement(<PatientSearch />))
  })

  it('should have a list of health projects', () => {
    expect(wrapper.find(HealthProject)).toHaveLength(2)
  })
})
