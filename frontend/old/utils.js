export function dateInWords(dateString, defaultValue) {
  const date = new Date(dateString)

  if (isNaN(date)) {
    return defaultValue
  }

  return date.toDateString()
}

export function mergeResource(resource, override) {
  for (let [key, value] of Object.entries(override)) {
    resource[key] = value
  }

  return resource
}
