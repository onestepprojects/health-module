import React, { useState, useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import FhirProvider from './context/FhirProvider'
import ProjectProvider from './context/ProjectProvider'

import ManageHealth from './components/project-management/ManageHealth'
import PatientProfile from './components/patient-profile/PatientProfile'
import ProjectDashboard from './components/project-dashboard/ProjectDashboard'
import Case from './components/case-management/Case'
import CreateCase from './components/case-management/CreateCase'
import CreatePatient from './components/patient-profile/CreatePatient'
import { getUser } from './api/OppApi'

import 'chart.js'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'shards-ui/dist/css/shards.min.css'
import { createPractitioner, getPractitionerByUserID } from './api/PractitionerApi'

const Health = ({ match, person, authHelper }) => {
  const [healthProjects, setHealthProjects] = useState(null)
  const [authToken, setAuthToken] = useState(null)
  const [practitioner, setPractitioner] = useState(null)

  useEffect(() => {
    const getUserToken = async () => {
      const token = await authHelper.GetUserAccessToken()
      setAuthToken(token)
    }

    getUserToken()
  }, [])

  // load the projects
  useEffect(async () => {
    if (!authToken) return

    const user = await getUser(authHelper)
    const practitionerLookup = await getPractitionerByUserID(user.uuid)
    if (!practitionerLookup) {
      //TODO: user does not have a practitioner record and should be not accessing this page unless they have been invited via ticket
      console.log('no practitioner ID')
    } else {
      const practitionerId = practitionerLookup[0].resource.id
      console.log(`setting practitioner ${practitionerId}`)
      setPractitioner(practitionerId)
    }

    const getProjects = async () => {
      const response = await fetch(`${process.env.REACT_APP_HEALTH_MODULE_PROJECTS_API}`, {
        headers: {
          //TODO: Change to use authToken when backend uses project auth
          //Authorization: authToken
          'Content-type': 'application/json',
          Authorization: `Basic ${btoa(
            process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
              ':' +
              process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
          )}`,
        },
      })

      let dataJson = await response.json()

      if (!dataJson) {
        throw new Error()
      }
      setHealthProjects(
        dataJson.filter((value) => {
          return value.name
        })
      )
    }

    getProjects()
  }, [authToken])

  return (
    authToken && (
      <Router>
        <FhirProvider>
          <div className='content'>
            <Switch>
              <Route
                exact
                path={match.url}
                render={(props) => (
                  <ManageHealth {...props} projects={healthProjects} auth={authHelper} />
                )}
              />
              <Route
                exact
                path={`${match.path}/project/:id`}
                render={(props) => (
                  <ProjectProvider>
                    <ProjectDashboard
                      basePath={match.url}
                      auth={authHelper}
                      projectId={props.match.params.id}
                      {...props}
                    />
                  </ProjectProvider>
                )}
              />
              <Route
                path={`${match.path}/project/:id/patient/:pid`}
                render={(props) => (
                  <PatientProfile
                    basePath={match.url}
                    projectId={props.match.params.id}
                    {...props}
                    auth={authHelper}
                  />
                )}
              />
              <Route
                path={`${match.path}/project/:id/case/:cid`}
                render={(props) => (
                  <Case
                    basePath={match.url}
                    projectId={props.match.params.id}
                    caseId={props.match.params.cid}
                    practitioner={practitioner}
                    {...props}
                    auth={authHelper}
                  />
                )}
              />
              <Route
                path={`${match.path}/project/:id/case`}
                render={(props) => (
                  <ProjectProvider>
                    <CreateCase
                      basePath={match.url}
                      projectId={props.match.params.id}
                      practitioner={practitioner}
                      {...props}
                      auth={authHelper}
                    />
                  </ProjectProvider>
                )}
              />
              <Route
                path={`${match.path}/project/:id/patient`}
                render={(props) => (
                  <ProjectProvider>
                    <CreatePatient
                      basePath={match.url}
                      projectId={props.match.params.id}
                      {...props}
                      auth={authHelper}
                    />
                  </ProjectProvider>
                )}
              />
            </Switch>
          </div>
        </FhirProvider>
      </Router>
    )
  )
}

export default Health
