export async function allergiesByPatient(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'AllergyIntolerance?patient=Patient/' + id,
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })

  result.sort((a, b) => {
    if (a.recordedDate === undefined) {
      return 1
    }

    if (b.recordedDate === undefined) {
      return -1
    }

    const aDate = new Date(a.recordedDate)
    const bDate = new Date(b.recordedDate)

    if (aDate < bDate) {
      return 1
    }

    if (aDate > bDate) {
      return -1
    }

    return 0
  })

  return result
}

export async function createAllergy(auth, client, signal, requestHeaders, allergyData, patientId) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  const symptoms = allergyData.symptom.trim().split('\\s*,\\s*')
  let symptomList = []
  for (let i in symptoms) {
    symptomList.push({ text: symptoms[i] })
  }

  const allergy = {
    resourceType: 'AllergyIntolerance',
    clinicalStatus: {
      coding: [
        {
          system: 'http://terminology.hl7.org/CodeSystem/allergyintolerance-clinical',
          code: 'active',
        },
      ],
    },
    verificationStatus: {
      coding: [
        {
          system: 'http://terminology.hl7.org/CodeSystem/allergyintolerance-verification',
          code: 'confirmed',
        },
      ],
    },
    type: 'allergy',
    criticality: 'low',
    code: {
      text: `${allergyData.newAllergy}`,
    },
    patient: {
      reference: `Patient/${patientId}`,
    },
    recordedDate: `${dateString}`,
    reaction: [
      {
        manifestation: symptomList,
      },
    ],
  }

  const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: 'AllergyIntolerance',
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  return client.create(allergy, requestOptions, { signal })
}
