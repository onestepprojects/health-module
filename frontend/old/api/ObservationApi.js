export async function getObservationByID(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    //headers: { ...requestHeaders, Authorization: token }
    //TODO: Change to use authToken when backend uses project auth
    url: `Observation/${id}`,
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  let result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })

  return result
}

export async function observationsByPatient(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    //url: 'Observation?patient=' + id,
    //headers: { ...requestHeaders, Authorization: token }
    //TODO: Change to use authToken when backend uses project auth
    url: 'Observation?patient=' + id,
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })

  result.sort((a, b) => {
    if (a.effectiveDateTime === undefined) {
      return 1
    }

    if (b.effectiveDateTime === undefined) {
      return -1
    }

    const aDate = new Date(a.effectiveDateTime)
    const bDate = new Date(b.effectiveDateTime)

    if (aDate < bDate) {
      return 1
    }

    if (aDate > bDate) {
      return -1
    }

    return 0
  })

  return result
}

export async function createObservation(auth, client, signal, requestHeaders, observationData) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  const observation = {
    resourceType: 'Observation',
    status: 'final',
    subject: {
      reference: `Patient/${observationData.patientId}`,
    },
    effectiveDateTime: dateString,
    valueString: observationData.value,
    code: {
      text: 'case details',
    },
  }

  //TODO: Change to use authToken when backend uses project auth
  //const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: 'Observation',
    //headers: { ...requestHeaders, Authorization: token },
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.create(observation, requestOptions, { signal })
  return result
}
