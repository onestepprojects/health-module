import { getUser } from './OppApi'

export async function getUserTickets(auth) {
  const token = await auth.GetUserAccessToken()

  const user = await getUser(auth)
  const userId = user.uuid

  const response = await fetch(
    `${process.env.REACT_APP_TICKET_SERVICE_API}/admin/${userId}/tickets`,
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-type': 'application/json',
      },
    }
  )

  const result = await response.json()

  return result
}

export async function createTicket(
  auth,
  priority,
  specialty,
  title,
  visitReason,
  projectId,
  caseId
) {
  const token = await auth.GetUserAccessToken()

  const user = await getUser(auth)
  const userId = user.uuid

  const ticket = {
    title: title,
    detail: visitReason,
    priority: priority,
    userId: userId,
    physicians: [specialty],
    referralUrl: `health/project/${projectId}/case/${caseId}`,
  }

  const response = await fetch(`${process.env.REACT_APP_TICKET_SERVICE_API}/tickets`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-type': 'application/json',
    },
    body: JSON.stringify(ticket),
  })

  const result = await response.json()

  return result
}
