export async function createDocumentReference(auth, client, signal, requestHeaders, documentData) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  const document = {
    resourceType: 'DocumentReference',
    subject: { reference: `Patient/${documentData.patientId}` },
    status: 'current',
    date: dateString,
    context: {
      encounter: [
        {
          reference: `Encounter/${documentData.encounterId}`,
        },
      ],
    },
    content: [
      {
        attachment: {
          contentType: 'text/plain',
          data: btoa(documentData.data),
        },
      },
    ],
  }

  //const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: 'DocumentReference',
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  return client.create(document, requestOptions, { signal })
}
