export async function carePlansByPatient(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'CarePlan?patient=Patient/' + id,
    //headers: { ...requestHeaders, Authorization: token }
    //TODO: Change to use authToken when backend uses project auth
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    resolveReferences: ['goal'],
    signal: signal,
  })

  result.sort((a, b) => {
    if (a.period === undefined) {
      return 1
    }

    if (b.period === undefined) {
      return -1
    }

    const aDate = new Date(a.period.start)
    const bDate = new Date(b.period.start)

    if (aDate < bDate) {
      return 1
    }

    if (aDate > bDate) {
      return -1
    }

    return 0
  })

  return result
}

export async function createCarePlan(auth, client, signal, requestHeaders, carePlanData) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  const carePlan = {
    resourceType: 'CarePlan',
    status: 'active',
    intent: 'plan',
    subject: {
      reference: `Patient/${carePlanData.patientId}`,
      type: 'Patient',
    },
    period: {
      start: dateString,
    },
    encounter: {
      reference: `Encounter/${carePlanData.encounterId}`,
    },
    description: carePlanData.ticketId,
    careTeam: [
      {
        reference: `CareTeam/${carePlanData.careTeamId}`,
      },
    ],
    note: [
      {
        text: `Composition/${carePlanData.caseId}`,
      },
    ],
  }

  const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: 'CarePlan',
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  return client.create(carePlan, requestOptions, { signal })
}
