export async function getAdminMedications(
  auth,
  client,
  signal,
  resources,
  requestHeaders
) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'MedicationAdministration',
    headers: { ...requestHeaders, Authorization: token }
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal
  })

  const meds = []

  for (const v of result) {
    for (let i = 0; i < resources.length; i++) {
      if (resources[i].resourceId === v.id) {
        meds.push(v.medicationCodeableConcept.text)
      }
    }
  }

  return meds
}
