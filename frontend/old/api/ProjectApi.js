export async function getProject(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: id,
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, { signal })

  let project = result
  const medResources = []
  const planResources = []
  const conditionResources = []

  return {
    project,
    medResources,
    planResources,
    conditionResources,
  }
}

export async function addResourceToProject(resource, projectId) {
  await fetch(`${process.env.REACT_APP_HEALTH_MODULE_PROJECTS_API}/Resource/${projectId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
    body: JSON.stringify(resource),
  })
}

export async function updateProject(auth, client, signal, id, requestHeaders, project) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: id,
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.update(project, requestOptions, { signal })

  return result
}
