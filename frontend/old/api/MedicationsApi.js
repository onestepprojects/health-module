export async function medicationsByPatient(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'MedicationAdministration?patient=Patient/' + id,
    //headers: { ...requestHeaders, Authorization: token }
    //TODO: Change to use authToken when backend uses project auth
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })

  result.sort((a, b) => {
    if (a.authoredOn === undefined) {
      return 1
    }

    if (b.authoredOn === undefined) {
      return -1
    }

    const aDate = new Date(a.authoredOn)
    const bDate = new Date(b.authoredOn)

    if (aDate < bDate) {
      return 1
    }

    if (aDate > bDate) {
      return -1
    }

    return 0
  })

  return result
}

export async function medicationRequestsByPatient(
  auth,
  client,
  signal,
  id,
  requestHeaders,
  medData,
  inputDate
) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'MedicationRequest?patient=' + id,
    headers: { ...requestHeaders, Authorization: token },
  }

  let rid = ''

  while (rid === '') {
    const result = await client.request(requestOptions, {
      flat: true,
      signal: signal,
    })
    for (const v of result) {
      if (v.authoredOn === inputDate) {
        rid = v.id
      }
    }
  }
  return rid
}

export async function createMedicationAdministration(
  auth,
  client,
  signal,
  requestHeaders,
  medicationData,
  patientId
) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  const medication = {
    resourceType: 'MedicationAdministration',
    status: 'completed',
    subject: {
      reference: `Patient/${patientId}`,
    },
    medicationCodeableConcept: {
      text: medicationData.newMed,
    },
    effectiveDateTime: `${dateString}`,
    dosage: {
      text: medicationData.strength,
      dose: {
        value: 1,
      },
    },
    note: [
      {
        text: medicationData.note,
      },
    ],
  }

  const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: 'MedicationAdministration',
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  return client.create(medication, requestOptions, { signal })
}

export async function createMedicationRequest(
  auth,
  client,
  signal,
  requestHeaders,
  medicationData,
  patientId
) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  const medicationRequest = {
    resourceType: 'MedicationRequest',
    status: 'active',
    intent: 'order',
    medicationCodeableConcept: {
      coding: [
        {
          system: 'http://www.nlm.nih.gov/research/umls/rxnorm',
          code: '309362',
          display: `${medicationData.newMed} ${medicationData.strength}`,
        },
      ],
      text: `${medicationData.newMed} ${medicationData.strength}`,
    },
    subject: {
      reference: `Patient/${patientId}`,
    },
    authoredOn: `${dateString}`,
  }

  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'MedicationRequest',
    headers: { ...requestHeaders, Authorization: token },
  }

  client.create(medicationRequest, requestOptions, { signal })

  return dateString
}

export async function createMedicationStatement(
  auth,
  client,
  signal,
  requestHeaders,
  resourceId,
  patientId
) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'
  const dateOnly = dateString.slice(0, -15)

  const medicationStatement = {
    resourceType: 'MedicationStatement',
    basedOn: [
      {
        reference: `MedicationRequest/${resourceId}`,
      },
    ],
    status: 'not-taken',
    subject: {
      reference: `Patient/${patientId}`,
    },
    effectiveDateTime: `${dateOnly}`,
    dateAsserted: `${dateString}`,
  }

  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'MedicationStatement',
    headers: { ...requestHeaders, Authorization: token },
  }

  return client.create(medicationStatement, requestOptions, { signal })
}
