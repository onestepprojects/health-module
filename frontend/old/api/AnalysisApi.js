export async function translate(sourceLanguage, destinationLanguage, data) {
  //const token = await auth.GetUserAccessToken()

  let url = `${process.env.REACT_APP_HEALTH_MODULE_FHIR_API}/translate?sourceLanguage=${sourceLanguage}&destinationLanguage=${destinationLanguage}`

  const response = await fetch(url, {
    method: 'POST',
    headers: {
      //TODO: Change to use authToken when backend uses project auth
      //'Authorization': `Bearer ${token}`,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
      'Content-type': 'text/html',
    },
    body: data,
  })
  const result = await response.text()

  return result
}

export async function emotionalize(sourceLanguage, data) {
  //const token = await auth.GetUserAccessToken()

  let url = `${process.env.REACT_APP_HEALTH_MODULE_FHIR_API}/emotionalize?sourceLanguage=${sourceLanguage}`

  const response = await fetch(url, {
    method: 'POST',
    headers: {
      //TODO: Change to use authToken when backend uses project auth
      //'Authorization': `Bearer ${token}`,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
      'Content-type': 'text/html',
    },
    body: data,
  })

  const result = await response.text()

  return result
}

export async function understand(sourceLanguage, data) {
  //const token = await auth.GetUserAccessToken()

  let url = `${process.env.REACT_APP_HEALTH_MODULE_FHIR_API}/understand?sourceLanguage=${sourceLanguage}`

  const response = await fetch(url, {
    method: 'POST',
    headers: {
      //TODO: Change to use authToken when backend uses project auth
      //'Authorization': `Bearer ${token}`,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
      'Content-type': 'text/html',
    },
    body: data,
  })

  const result = await response.text()

  return result
}
