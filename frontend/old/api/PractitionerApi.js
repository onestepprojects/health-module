export async function getPractitionerByID(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'Practitioner/' + id,
    //headers: { ...requestHeaders, Authorization: token },
    //TODO: Change to use authToken when backend uses project auth
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }
  return client.request(requestOptions, { signal })
}

export async function getPractitionerByUserID(userId) {
  //const token = await auth.GetUserAccessToken()

  const response = await fetch(
    `${process.env.REACT_APP_HEALTH_MODULE_FHIR_API}/Practitioner?identifier=${userId}`,
    {
      method: 'GET',
      headers: {
        //TODO: Change to use authToken when backend uses project auth
        Authorization: `Basic ${btoa(
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
            ':' +
            process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
        )}`,
        'Content-type': 'application/json',
      },
    }
  )

  const result = await response.json()
  return result.entry
}

export async function createPractitioner(practitionerData) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  const practitioner = {
    resourceType: 'Practitioner',
    name: [
      {
        given: practitionerData.firstName,
        family: practitionerData.lastName,
      },
    ],
    identifier: [
      {
        value: practitionerData.userId,
      },
    ],
  }

  const response = await fetch(`${process.env.REACT_APP_HEALTH_MODULE_FHIR_API}/Practitioner`, {
    method: 'POST',
    headers: {
      //TODO: Change to use authToken when backend uses project auth
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
      'Content-type': 'application/json',
    },
    body: JSON.stringify(practitioner),
  })

  const result = await response.json()

  return result
}
