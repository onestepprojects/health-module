export async function getEncounterByID(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    //headers: { ...requestHeaders, Authorization: token }
    //TODO: Change to use authToken when backend uses project auth
    url: `Encounter/${id}`,
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })

  return result
}

export async function getEncountersByPatientID(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    //url: 'Encounter?patient=' + id,
    //headers: { ...requestHeaders, Authorization: token }
    //TODO: Change to use authToken when backend uses project auth
    url: `Encounter?patient=Patient/${id}`,
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })

  result.sort((a, b) => {
    if (a.period === undefined) {
      return 1
    }

    if (b.period === undefined) {
      return -1
    }

    const aDate = new Date(a.period.start)
    const bDate = new Date(b.period.start)

    if (aDate < bDate) {
      return 1
    }

    if (aDate > bDate) {
      return -1
    }

    return 0
  })

  return result
}

export async function createEncounter(
  auth,
  client,
  signal,
  requestHeaders,
  encounterData,
  patientId
) {
  const encounter = {
    resourceType: 'Encounter',
    status: 'finished',
    class: {
      system: 'http://terminology.hl7.org/CodeSystem/v3-ActCode',
      code: 'AMB',
    },
    subject: {
      reference: `Patient/${patientId}`,
    },
    period: {
      start: `${encounterData.startDate}`,
    },
    reasonCode: [
      {
        text: `${encounterData.reason}`,
      },
    ],
  }

  //const token = await auth.GetUserAccessToken()

  //TODO: Change to use authToken when backend uses project auth
  const requestOptions = {
    url: 'Encounter',
    //headers: { ...requestHeaders, Authorization: token },
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.create(encounter, requestOptions, { signal })
  return result
}
