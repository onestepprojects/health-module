export async function getPlanStatus(
  auth,
  client,
  signal,
  resources,
  requestHeaders
) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'CarePlan',
    headers: { ...requestHeaders, Authorization: token }
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal
  })

  const planStatuses = []

  for (const v of result) {
    for (let i = 0; i < resources.length; i++) {
      if (resources[i].resourceId === v.id) {
        planStatuses.push(v.status)
      }
    }
  }

  return planStatuses
}
