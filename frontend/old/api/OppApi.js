export async function getUser(auth) {
  const token = await auth.GetUserAccessToken()
  console.log(token)

  const response = await fetch(
    `${process.env.REACT_APP_ORG_MODULE_API_URL}/persons/requester/token`,
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-type': 'application/json',
      },
    }
  )

  const result = await response.json()
  return result
}
