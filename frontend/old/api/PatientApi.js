export async function getPatientSearch(auth, client, signal, requestHeaders) {
  const result = []
  //TODO: Change to use authToken when backend uses project auth
  //const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'Patient',
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const responseData = await client.request(requestOptions, {
    signal: signal,
  })

  const { entry, total } = responseData

  //result.push(...entry)
  //result.push(entry)

  /*for (let i = 1; i < Math.floor(total / 20); i++) {
    const options = {
      ...requestOptions,
    }
    options.url += `?_getpagesoffset=${i * 20 + (i - 1)}&_count=20`

    const response = await client.request(options, {
      flat: true,
      signal: signal,
    })

    result.push(...response)
  }*/

  //const results = result.map((value) => {
  const results = entry.map((value) => {
    const v = value.resource ? value.resource : value
    let lastName = ''
    let firstName = ''
    let gender = ''
    let city = ''
    let state = ''
    let country = ''
    let dob = ''

    try {
      lastName = v.name[0].family
    } catch {}

    try {
      firstName = v.name[0].given[0]
    } catch {}

    try {
      gender = v.gender
    } catch {}

    try {
      dob = v.birthDate
    } catch {}

    try {
      city = v.address[0].city
    } catch (error) {}

    try {
      state = v.address[0].state
    } catch {}

    try {
      country = v.address[0].country
    } catch {}

    return [
      v.id,
      firstName +
        ' ' +
        lastName +
        ' | ' +
        gender +
        ' | ' +
        dob +
        ' | ' +
        city +
        ', ' +
        state +
        ', ' +
        country,
    ]
  })

  return results
}

export async function getPatientByID(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'Patient/' + id,
    //headers: { ...requestHeaders, Authorization: token },
    //TODO: Change to use authToken when backend uses project auth
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }
  return client.request(requestOptions, { signal })
}

export async function updatePatient(auth, client, signal, requestHeaders, id, json) {
  const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: 'Patient/' + id,
    method: 'PUT',
    body: JSON.stringify(json),
    headers: { ...requestHeaders, Authorization: token },
  }

  return client.request(requestOptions, { signal })
}

export function findExtension(data, urls) {
  if (!Object.prototype.hasOwnProperty.call(data, 'extension')) {
    return ''
  }

  const extension = data.extension.find((element) => {
    return urls[0] === element.url
  })

  if (extension === undefined) {
    return ''
  }

  if (extension.extension === undefined) {
    for (const key in extension) {
      if (key.toString().startsWith('value')) {
        return extension[key]
      }
    }
  }

  if (urls.length === 0) {
    return ''
  }

  urls.shift()
  return findExtension(extension, urls)
}

export async function createPatient(auth, client, signal, requestHeaders, patientData) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  const patient = {
    resourceType: 'Patient',
    meta: {
      lastUpdated: dateString,
    },
    gender: patientData.gender,
    name: [
      {
        family: patientData.lastName,
        given: [patientData.firstName],
      },
    ],
    address: {
      line: [patientData.addressLine1, patientData.addressLine2],
      city: patientData.city,
      state: patientData.state,
      postalCode: patientData.zipCode,
      country: patientData.country,
    },
    telecom: [
      {
        system: 'phone',
        value: patientData.phone,
      },
      {
        system: 'email',
        value: patientData.email,
      },
    ],
    birthDate: patientData.birthDate,
    communication: [
      {
        language: {
          text: patientData.language,
        },
      },
    ],
    generalPractitioner: [
      {
        reference: 'Practitioner/1961057',
      },
    ],
    managingOrganization: {
      reference: 'Organization/1961043',
    },
  }

  //const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: 'Patient',
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  return client.create(patient, requestOptions, { signal })
}
