export async function careTeamsByPractitioner(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'CareTeam?participant=Practitioner/' + id,
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })

  return result
}

export async function getCareTeamByID(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'CareTeam/' + id,
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })

  return result
}

export async function createCareTeam(auth, client, signal, requestHeaders, careTeamData) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  const careTeam = {
    resourceType: 'CareTeam',
    status: 'active',
    subject: {
      reference: `Patient/${careTeamData.patientId}`,
      type: 'Patient',
    },
    period: {
      start: dateString,
    },
    participant: [
      {
        member: {
          reference: `Practitioner/${careTeamData.practitionerId}`,
        },
      },
    ],
  }

  const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: 'CareTeam',
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  return client.create(careTeam, requestOptions, { signal })
}

export async function updateCareTeam(auth, client, signal, requestHeaders, careTeamData) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  const careTeam = {
    resourceType: 'CareTeam',
    status: 'active',
    subject: {
      reference: `Patient/${careTeamData.patientId}`,
      type: 'Patient',
    },
    period: {
      start: dateString,
    },
    participant: [
      {
        member: {
          reference: `Practitioner/${careTeamData.practitionerId}`,
        },
      },
    ],
  }

  const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: 'CareTeam',
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  return client.create(careTeam, requestOptions, { signal })
}
