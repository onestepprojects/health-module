/**
 * Field is found in each FHIR resources
 *
 * @param {string} name         display name
 * @param {Resource} resource   new resource object
 * @param {string} description  "help" text; set to empty string to not include
 * @param {boolean} inForm      whether to include in form or not
 * @param {string} fhirKey      FHIR identifier specification
 */
export default function Field(name, resource, description, inForm, fhirKey) {
  this.name = name
  this.resource = resource
  this.description = description

  this.key = fhirKey ? fhirKey : name

  if (inForm === undefined) {
    this.inForm = true
  } else {
    this.inForm = inForm
  }

  this.getInForm = () => {
    return this.inForm
  }

  this.setInForm = (newValue) => {
    this.setInForm = newValue
  }
}
