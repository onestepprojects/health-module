import Resource from './Resource'
import Field from './Field'

import FhirAge from './datatypes/primitive/FhirAge'
import FhirArray from './datatypes/primitive/FhirArray'
import FhirDateTime from './datatypes/primitive/FhirDateTime'
import FhirString from './datatypes/primitive/FhirString'

import Annotation from './datatypes/Annotation'
import CodeableConcept from './datatypes/CodeableConcept'
import Evidence from './datatypes/Evidence'
import Identifier from './datatypes/Identifier'
import Period from './datatypes/Period'
import Range from './datatypes/Range'
import Stage from './datatypes/Stage'
import Reference from './datatypes/Reference'

export default function Condition() {
  this.name = 'Practitioner'

  Resource.call(this)

  this.field.push(
    new Field(
      'identifier',
      new FhirArray(new Identifier()),
      'identifier',
      false
    )
  )
  this.field.push(
    new Field('clinicalStatus', new CodeableConcept(), 'clinicalStatus', false)
  )
  this.field.push(
    new Field(
      'verificationStatus',
      new CodeableConcept(),
      'verificationStatus',
      false
    )
  )
  this.field.push(
    new Field(
      'category',
      new FhirArray(new CodeableConcept()),
      'category',
      false
    )
  )
  this.field.push(
    new Field('severity', new CodeableConcept(), 'severity', false)
  )
  this.field.push(new Field('code', new CodeableConcept(), 'code', false))
  this.field.push(
    new Field(
      'bodySite',
      new FhirArray(new CodeableConcept()),
      'bodySite',
      false
    )
  )
  this.field.push(new Field('subject', new Reference(), 'subject', false)) // TODO pass reference
  this.field.push(new Field('encounter', new Reference(), 'encounter', false)) // TODO pass reference
  this.field.push(
    new Field('onsetDateTime', new FhirDateTime(), 'onsetDateTime', false)
  )
  this.field.push(new Field('onsetAge', new FhirAge(), 'onsetAge', false))
  this.field.push(new Field('onsetPeriod', new Period(), 'onsetPeriod', false))
  this.field.push(new Field('onsetRange', new Range(), 'onsetRange', false))
  this.field.push(
    new Field('onsetString', new FhirString(), 'onsetString', false)
  )
  this.field.push(
    new Field(
      'abatementDateTime',
      new FhirDateTime(),
      'abatementDateTime',
      false
    )
  )
  this.field.push(
    new Field('abatementAge', new FhirAge(), 'abatementAge', false)
  )
  this.field.push(
    new Field('abatementPeriod', new Period(), 'abatementPeriod', false)
  )
  this.field.push(
    new Field('abatementRange', new Range(), 'abatementRange', true)
  )
  this.field.push(
    new Field('abatementString', new FhirString(), 'abatementString', true)
  )
  this.field.push(
    new Field('recordedDate', new FhirDateTime(), 'recordedDate', true)
  )
  this.field.push(new Field('recorder', new Reference(), 'recorder', true)) // TODO pass reference
  this.field.push(new Field('asserter', new Reference(), 'asserter', true)) // TODO pass reference
  this.field.push(new Field('stage', new Stage(), 'stage', true))
  this.field.push(new Field('evidence', new Evidence(), 'evidence', true))
  this.field.push(
    new Field('note', new FhirArray(new Annotation()), 'note', true)
  )
}
