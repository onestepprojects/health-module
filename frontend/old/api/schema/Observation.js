import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirBoolean from './datatypes/primitive/FhirBoolean'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirDateTime from './datatypes/primitive/FhirDateTime'
import FhirInteger from './datatypes/primitive/FhirInteger'
import FhirString from './datatypes/primitive/FhirString'
import FhirTime from './datatypes/primitive/FhirTime'

import Annotation from './datatypes/Annotation'
import CodeableConcept from './datatypes/CodeableConcept'
import Component from './datatypes/Component'
import Identifier from './datatypes/Identifier'
import Instant from './datatypes/Instant'
import Period from './datatypes/Period'
import Quantity from './datatypes/Quantity'
import Range from './datatypes/Range'
import Ratio from './datatypes/Ratio'
import Reference from './datatypes/Reference'
import ReferenceRange from './datatypes/ReferenceRange'
import SampledData from './datatypes/SampledData'
import Timing from './datatypes/Timing'

export default function Observation() {
  this.name = 'Observation'

  Resource.call(this)

  this.field.push(
    new Field('identifier', new FhirArray(new Identifier()), 'identifier', true)
  )
  this.field.push(
    new Field('basedOn', new FhirArray(new Reference()), 'basedOn', true)
  ) // TODO pass Reference
  this.field.push(
    new Field('partOf', new FhirArray(new Identifier()), 'partOf', true)
  ) // TODO pass Reference
  this.field.push(new Field('status', new FhirCode(), 'status', true))
  this.field.push(
    new Field(
      'category',
      new FhirArray(new CodeableConcept()),
      'category',
      true
    )
  )
  this.field.push(new Field('code', new CodeableConcept(), 'code', true))
  this.field.push(new Field('subject', new Reference(), 'subject', true)) // TODO pass Reference
  this.field.push(
    new Field('focus', new FhirArray(new Reference()), 'focus', true)
  ) // TODO pass Reference
  this.field.push(new Field('encounter', new Reference(), 'encounter', true)) // TODO pass Reference
  this.field.push(
    new Field(
      'effectiveDateTime',
      new FhirDateTime(),
      'effectiveDateTime',
      true
    )
  )
  this.field.push(
    new Field('effectivePeriod', new Period(), 'effectivePeriod', true)
  )
  this.field.push(
    new Field('effectiveTiming', new Timing(), 'effectiveTiming', true)
  )
  this.field.push(
    new Field('effectiveInstant', new Instant(), 'effectiveInstant', true)
  )
  this.field.push(new Field('issued', new Instant(), 'issued', true))
  this.field.push(
    new Field('performer', new FhirArray(new Reference()), 'performer', true)
  ) // TODO pass Reference
  this.field.push(
    new Field('valueQuantity', new Quantity(), 'valueQuantity', true)
  )
  this.field.push(
    new Field(
      'valueCodeableConcept',
      new CodeableConcept(),
      'valueCodeableConcept',
      true
    )
  )
  this.field.push(
    new Field('valueString', new FhirString(), 'valueString', true)
  )
  this.field.push(
    new Field('valueBoolean', new FhirBoolean(), 'valueBoolean', true)
  )
  this.field.push(
    new Field('valueInteger', new FhirInteger(), 'valueInteger', true)
  )
  this.field.push(new Field('valueRange', new Range(), 'valueRange', true))
  this.field.push(new Field('valueRatio', new Ratio(), 'valueRatio', true))
  this.field.push(
    new Field('valueSampledData', new SampledData(), 'valueSampledData', true)
  )
  this.field.push(new Field('valueTime', new FhirTime(), 'valueTime', true))
  this.field.push(
    new Field('valueDateTime', new FhirDateTime(), 'valueDateTime', true)
  )
  this.field.push(new Field('valuePeriod', new Period(), 'valuePeriod', true))
  this.field.push(
    new Field(
      'dataAbsentReason',
      new CodeableConcept(),
      'dataAbsentReason',
      true
    )
  )
  this.field.push(
    new Field(
      'interpretation',
      new FhirArray(new CodeableConcept()),
      'interpretation',
      true
    )
  )
  this.field.push(
    new Field('note', new FhirArray(new Annotation()), 'note', true)
  )
  this.field.push(
    new Field('bodySite', new CodeableConcept(), 'bodySite', true)
  )
  this.field.push(new Field('method', new CodeableConcept(), 'method', true))
  this.field.push(new Field('specimen', new Reference(), 'specimen', true)) // TODO pass Reference
  this.field.push(new Field('device', new Reference(), 'device', true)) // TODO pass Reference
  this.field.push(
    new Field(
      'referenceRange',
      new FhirArray(new ReferenceRange()),
      'referenceRange',
      true
    )
  )
  this.field.push(
    new Field('hasMember', new FhirArray(new Reference()), 'hasMember', true)
  ) // TODO pass Reference
  this.field.push(
    new Field(
      'derivedFrom',
      new FhirArray(new Reference()),
      'derivedFrom',
      true
    )
  ) // TODO pass Reference
  this.field.push(
    new Field('component', new FhirArray(new Component()), 'component', true)
  )
}
