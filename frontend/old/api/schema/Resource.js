import { mergeResource } from '../../utils'
import FhirString from './datatypes/primitive/FhirString'
import Field from './Field'

export default function Resource(topLevel) {
  this.fields = [
    new Field('resourceType', new FhirString(this.name), '', false)
  ]

  this.fullResource = {}

  if (topLevel === undefined) {
    this.topLevel = false
  } else {
    this.topLevel = topLevel
  }

  this.updateFieldValue = (field, newValue) => {
    for (const item of this.fields) {
      if (field === item.key && item.resource.value !== undefined) {
        item.resource.value = newValue
        return
      } else if (field === item.key) {
        item.resource = newValue
        return
      }
    }
  }

  this.getFieldValue = (field) => {
    for (const item of this.fields) {
      if (field === item.key && item.resource.value !== undefined) {
        return item.resource.value
      } else if (field === item.key) {
        return item.resource
      }
    }
  }

  this.getField = (name) => {
    for (const item of this.fields) {
      if (name === item.key) {
        return item
      }
    }
  }

  this.fhirSchema = () => {
    var jsonObject = {}

    for (const item of this.fields) {
      if (item.inForm) {
        jsonObject[item.key] = item.resource.fhirSchema()
      }
    }

    return mergeResource(this.fullResource, jsonObject)
  }

  this.formSchema = () => {
    var elements = []

    for (const item of this.fields) {
      if (item.inForm) {
        let schema = item.resource.formSchema()
        schema['name'] = item.key
        schema['title'] = item.name
        if (item.description !== '') schema['description'] = item.description
        elements.push(schema)
      }
    }

    if (this.topLevel) {
      return { name: this.name, title: this.name, elements: elements }
    } else {
      return {
        name: this.name,
        title: this.name,
        elements: elements,
        type: 'panel'
      }
    }
  }

  this.formData = () => {
    var data = {}

    for (const item of this.fields) {
      if (item.inForm) {
        data[item.key] = item.resource.formData()
      }
    }

    return data
  }

  this.updateResource = (auth, client, signal, requestHeaders, id) => {
    // default
  }
}
