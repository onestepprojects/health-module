import { updatePatient } from '../PatientApi'

export default function HealthForm(...resources) {
  this.resources = resources

  this.createForm = () => {
    var elements = []

    for (const item of this.resources) {
      item.topLevel = true
      elements.push(item.formSchema())
    }

    return {
      pages: elements
    }
  }

  this.getFormData = () => {
    var allResources = []

    for (const item of this.resources) {
      allResources.push(item.formData())
    }

    return allResources
  }

  this.submitResources = (auth, client, signal, requestHeaders, id) => {
    for (const item of this.resources) {
      item.updateResource(auth, client, signal, requestHeaders, id)
    }
  }
}
