import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirDateTime from './datatypes/primitive/FhirDateTime'

import CodeableConcept from './datatypes/CodeableConcept'
import Identifier from './datatypes/Identifier'
import SuspectEntity from './datatypes/SuspectEntity'
import Reference from './datatypes/Reference'

export default function AdverseEvent() {
  this.name = 'Practitioner'

  Resource.call(this)

  this.field.push(new Field('Identifier', new Identifier(), 'identifier', true))
  this.field.push(
    new Field('Actuality', new CodeableConcept(), 'actuality', true)
  )
  this.field.push(
    new Field(
      'Category',
      new FhirArray(new CodeableConcept()),
      'category',
      true
    )
  )
  this.field.push(new Field('Event', new CodeableConcept(), 'event', true))
  this.field.push(new Field('Subject', new Reference(), 'subject', true)) // TODO pass reference
  this.field.push(new Field('Encounter', new Reference(), 'encounter', true)) // TODO pass reference
  this.field.push(new Field('Date', new FhirDateTime(), 'date', true))
  this.field.push(new Field('Detected', new FhirDateTime(), 'detected', true))
  this.field.push(
    new Field('Recorded Date', new FhirDateTime(), 'recordedDate', true)
  )
  this.field.push(
    new Field(
      'Resulting Condition',
      new FhirArray(new Reference()),
      'resultingCondition',
      true
    ) // TODO pass reference
  )
  this.field.push(new Field('Location', new Reference(), 'location', true)) // TODO pass reference
  this.field.push(
    new Field('Seriousness', new CodeableConcept(), 'seriousness', true)
  )
  this.field.push(
    new Field('Severity', new CodeableConcept(), 'severity', true)
  )
  this.field.push(new Field('Outcome', new CodeableConcept(), 'outcome', true))
  this.field.push(new Field('Recorder', new Reference(), 'recorder', true)) // TODO pass reference
  this.field.push(
    new Field(
      'Contributor',
      new FhirArray(new Reference()),
      'contributor',
      true
    )
  ) // TODO pass reference
  this.field.push(
    new Field('Suspect Entity', new SuspectEntity(), 'suspectEntity', true)
  )
  this.field.push(
    new Field(
      'Subject Medical History',
      new FhirArray(new Reference()),
      'subjectMedicalHistory',
      true
    )
  ) // TODO pass reference
  this.field.push(
    new Field(
      'Reference Document',
      new FhirArray(new Reference()),
      'referenceDocument',
      true
    )
  ) // TODO pass reference
  this.field.push(
    new Field('Study', new FhirArray(new Reference()), 'study', true)
  ) // TODO pass reference
}
