import Resource from './Resource'
import Field from './Field'

import FhirAge from './datatypes/primitive/FhirAge'
import FhirArray from './datatypes/primitive/FhirArray'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirDateTime from './datatypes/primitive/FhirDateTime'
import FhirString from './datatypes/primitive/FhirString'
import FhirUri from './datatypes/primitive/FhirUri'

import Annotation from './datatypes/Annotation'
import Canonical from './datatypes/Canonical'
import CodeableConcept from './datatypes/CodeableConcept'
import FocalDevice from './datatypes/FocalDevice'
import Identifier from './datatypes/Identifier'
import Performer from './datatypes/Performer'
import Period from './datatypes/Period'
import Range from './datatypes/Range'
import Reference from './datatypes/Reference'

export default function Procedure() {
  this.name = 'Procedure'

  Resource.call(this)

  this.field.push(
    new Field('Identifier', new FhirArray(new Identifier()), 'identifier', true)
  )
  this.field.push(
    new Field(
      'Instantiates Canonical',
      new FhirArray(new Canonical()),
      'instantiatesCanonical',
      true
    )
  )
  this.field.push(
    new Field(
      'Instantiates Uri',
      new FhirArray(new FhirUri()),
      'instantiatesUri',
      true
    )
  )
  this.field.push(
    new Field('Based On', new FhirArray(new Reference()), 'basedOn', true)
  ) // TODO pass Reference
  this.field.push(
    new Field('Part Of', new FhirArray(new Reference()), 'partOf', true)
  ) // TODO pass Reference
  this.field.push(new Field('Status', new FhirCode(), 'status', true))
  this.field.push(
    new Field('Status Reason', new CodeableConcept(), 'statusReason', true)
  )
  this.field.push(
    new Field('Category', new CodeableConcept(), 'category', true)
  )
  this.field.push(new Field('Code', new CodeableConcept(), 'code', true))
  this.field.push(new Field('Subject', new Reference(), 'subject', true)) // TODO pass Reference
  this.field.push(new Field('Encounter', new Reference(), 'encounter', true)) // TODO pass Reference
  this.field.push(
    new Field(
      'Performed Date Time',
      new FhirDateTime(),
      'performedDateTime',
      true
    )
  )
  this.field.push(
    new Field('Performed Period', new Period(), 'performedPeriod', true)
  )
  this.field.push(
    new Field('Performed String', new FhirString(), 'performedString', true)
  )
  this.field.push(
    new Field('Performed Age', new FhirAge(), 'performedAge', true)
  )
  this.field.push(
    new Field('Performed Range', new Range(), 'performedRange', true)
  )
  this.field.push(new Field('Recorder', Reference(), 'recorder', true)) // TODO pass Reference
  this.field.push(new Field('Asserter', Reference(), 'asserter', true)) // TODO pass Reference
  this.field.push(
    new Field('Performer', new FhirArray(new Performer()), 'performer', true)
  )
  this.field.push(new Field('Location', new Reference(), 'location', true)) // TODO pass Reference
  this.field.push(
    new Field(
      'Reason Code',
      new FhirArray(new CodeableConcept()),
      'reasonCode',
      true
    )
  )
  this.field.push(
    new Field(
      'Reason Reference',
      new FhirArray(new Reference()),
      'reasonReference',
      true
    )
  ) // TODO pass Reference
  this.field.push(
    new Field(
      'Body Site',
      new FhirArray(new CodeableConcept()),
      'bodySite',
      true
    )
  )
  this.field.push(new Field('Outcome', new CodeableConcept(), 'outcome', true))
  this.field.push(
    new Field('Report', new FhirArray(new Reference()), 'report', true)
  ) // TODO pass Reference
  this.field.push(
    new Field(
      'Complication',
      new FhirArray(new CodeableConcept()),
      'complication',
      true
    )
  )
  this.field.push(
    new Field(
      'Complication Detail',
      new FhirArray(new Reference(), 'complicationDetail', true)
    )
  ) // TODO pass Reference
  this.field.push(
    new Field(
      'Follow Up',
      new FhirString(new CodeableConcept()),
      'followUp',
      true
    )
  )
  this.field.push(
    new Field('Note', new FhirArray(new Annotation()), 'note', true)
  )
  this.field.push(
    new Field(
      'Focal Device',
      new FhirArray(new FocalDevice()),
      'focalDevice',
      true
    )
  )
  this.field.push(
    new Field(
      'Used References',
      new FhirArray(new Reference()),
      'usedReferences',
      true
    )
  ) // TODO pass Reference
  this.field.push(
    new Field(
      'Used Code',
      new FhirArray(new CodeableConcept()),
      'usedCode',
      true
    )
  )
}
