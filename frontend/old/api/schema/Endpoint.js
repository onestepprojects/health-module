import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirString from './datatypes/primitive/FhirString'
import FhirUrl from './datatypes/primitive/FhirUrl'

import Coding from './datatypes/Coding'
import CodeableConcept from './datatypes/CodeableConcept'
import ContactPoint from './datatypes/ContactPoint'
import Identifier from './datatypes/Identifier'
import Period from './datatypes/Period'
import Reference from './datatypes/Reference'

export default function Endpoint() {
  this.name = 'Endpoint'

  Resource.call(this)

  this.field.push(new Field('Identifier', new Identifier(), 'identifier', true))
  this.field.push(new Field('Status', new FhirCode(), 'status', true))
  this.field.push(
    new Field('Connection Type', new Coding(), 'connectionType', true)
  )
  this.field.push(new Field('Name', new FhirString(), 'name', true))
  this.field.push(
    new Field(
      'Managing Organization',
      new Reference(),
      'managingOrganization',
      true
    )
  ) // TODO pass Reference
  this.field.push(
    new Field('Contact', new FhirArray(new ContactPoint()), 'contact', true)
  )
  this.field.push(new Field('Period', new Period(), 'period', true))
  this.field.push(
    new Field(
      'Payload Type',
      new FhirArray(new CodeableConcept()),
      'payloadType',
      true
    )
  )
  this.field.push(
    new Field('Payload Mime Type', new FhirArray(new FhirCode())),
    'payloadType',
    true
  )
  this.field.push(new Field('Address', new FhirUrl()), 'address', true)
  this.field.push(
    new Field('Header', new FhirArray(new FhirString()), 'header', true)
  )
}
