import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirString from './datatypes/primitive/FhirString'

import Address from './datatypes/Address'
import Coding from './datatypes/Coding'
import CodeableConcept from './datatypes/CodeableConcept'
import ContactPoint from './datatypes/ContactPoint'
import HoursOfOperation from './datatypes/HoursOfOperation'
import Identifier from './datatypes/Identifier'
import Position from './datatypes/Position'
import Reference from './datatypes/Reference'

export default function Location() {
  this.name = 'Location'

  Resource.call(this)

  this.field.push(new Field('identifier', new FhirArray(new Identifier())))
  this.field.push(new Field('status', new FhirCode()))
  this.field.push(new Field('operationalStatus', new Coding()))
  this.field.push(new Field('name', new FhirString()))
  this.field.push(new Field('alias', new FhirArray(new FhirString())))
  this.field.push(new Field('description', new FhirString()))
  this.field.push(new Field('mode', new FhirCode()))
  this.field.push(new Field('type', new FhirArray(new CodeableConcept())))
  this.field.push(new Field('telecom', new FhirArray(new ContactPoint())))
  this.field.push(new Field('address', new Address()))
  this.field.push(new Field('physicalType', new CodeableConcept()))
  this.field.push(new Field('position', new Position()))
  this.field.push(new Field('managingOrganization', new Reference())) // TODO pass Reference
  this.field.push(new Field('partOf', new Reference())) // TODO pass Reference
  this.field.push(
    new Field('hoursOfOperation', new FhirArray(new HoursOfOperation()))
  )
  this.field.push(
    new Field('availabilityExceptions', new FhirArray(new Reference()))
  )
  this.field.push(new Field('endpoint', new Reference())) // TODO pass Reference
}
