import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirBoolean from './datatypes/primitive/FhirBoolean'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirDate from './datatypes/primitive/FhirDate'
import FhirDateTime from './datatypes/primitive/FhirDateTime'
import FhirString from './datatypes/primitive/FhirString'

import Annotation from './datatypes/Annotation'
import CodeableConcept from './datatypes/CodeableConcept'
import Education from './datatypes/Education'
import Identifier from './datatypes/Identifier'
import Quantity from './datatypes/Quantity'
import ProtocolApplied from './datatypes/ProtocolApplied'
import Performer from './datatypes/Performer'
import Reaction from './datatypes/Reaction'
import Reference from './datatypes/Reference'

export default function Immunization() {
  this.name = 'Immunization'

  Resource.call(this)

  this.field.push(new Field('identifier', new FhirArray(new Identifier())))
  this.field.push(new Field('status', new FhirCode()))
  this.field.push(new Field('statusReason', new CodeableConcept()))
  this.field.push(new Field('vaccineCode', new CodeableConcept()))
  this.field.push(new Field('patient', new Reference())) // TODO pass reference
  this.field.push(new Field('encounter', new Reference())) // TODO pass reference))
  this.field.push(new Field('occurrenceDateTime', new FhirDateTime()))
  this.field.push(new Field('occurrenceString', new FhirString()))
  this.field.push(new Field('recorded', new FhirDateTime()))
  this.field.push(new Field('primarySource', new FhirBoolean()))
  this.field.push(new Field('reportOrigin', new CodeableConcept()))
  this.field.push(new Field('location', new Reference())) // TODO pass reference
  this.field.push(new Field('manufacturer', new Reference())) // TODO pass reference
  this.field.push(new Field('lotNumber', new FhirString()))
  this.field.push(new Field('expirationDate', new FhirDate()))
  this.field.push(new Field('site', new CodeableConcept()))
  this.field.push(new Field('route', new CodeableConcept()))
  this.field.push(new Field('doseQuantity', new Quantity())) // TODO pass SimplyQuantity
  this.field.push(new Field('performer', new FhirArray(new Performer())))
  this.field.push(new Field('note', new FhirArray(new Annotation())))
  this.field.push(new Field('reasonCode', new FhirArray(new CodeableConcept())))
  this.field.push(new Field('reasonReference', new FhirArray(new Reference()))) // TODO pass reference
  this.field.push(new Field('isSubpotent', new FhirBoolean()))
  this.field.push(
    new Field('subpotentReason', new FhirArray(new CodeableConcept()))
  )
  this.field.push(new Field('education', new FhirArray(new Education())))
  this.field.push(
    new Field('programEligibility', new FhirArray(new CodeableConcept()))
  )
  this.field.push(new Field('fundingSource', new CodeableConcept()))
  this.field.push(new Field('reaction', new FhirArray(new Reaction())))
  this.field.push(new Field('protocolApplied', new ProtocolApplied()))
}
