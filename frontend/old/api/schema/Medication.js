import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirCode from './datatypes/primitive/FhirCode'

import Batch from './datatypes/Batch'
import CodeableConcept from './datatypes/CodeableConcept'
import Identifier from './datatypes/Identifier'
import Ingredient from './datatypes/Ingredient'
import Ratio from './datatypes/Ratio'
import Reference from './datatypes/Reference'

export default function Medication() {
  this.name = 'Medication'

  Resource.call(this)

  this.field.push(new Field('identifier', new FhirArray(new Identifier())))
  this.field.push(new Field('code', new CodeableConcept()))
  this.field.push(new Field('status', new FhirCode()))
  this.field.push(new Field('manufacturer', new Reference()))
  this.field.push(new Field('form', new CodeableConcept()))
  this.field.push(new Field('amount', new Ratio()))
  this.field.push(new Field('ingredient', new Ingredient()))
  this.field.push(new Field('batch', new Batch()))
}
