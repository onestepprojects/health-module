import Resource from './Resource'
import Field from './Field'

import FhirCode from './datatypes/primitive/FhirCode'
import Identifier from './datatypes/Identifier'
import FhirArray from './datatypes/primitive/FhirArray'
import FhirBoolean from './datatypes/primitive/FhirBoolean'
import HumanName from './datatypes/HumanName'
import ContactPoint from './datatypes/ContactPoint'
import FhirDate from './datatypes/primitive/FhirDate'
import Address from './datatypes/Address'
import CodeableConcept from './datatypes/CodeableConcept'
import Reference from './datatypes/Reference'
import Attachment from './datatypes/Attachment'
import { updatePatient } from '../PatientApi'

import _get from 'lodash/get'

export default function Patient() {
  this.name = 'Patient'

  Resource.call(this)

  this.fields.push(
    new Field(
      'Identifier',
      new FhirArray(new Identifier()),
      'identifier',
      false,
      'identifier'
    )
  )
  this.fields.push(new Field('Active', new FhirBoolean(), '', true, 'active'))
  this.fields.push(
    new Field('Name', new FhirArray(), '', true, 'name') // takes HumanName()
  )
  this.fields.push(
    new Field(
      'Telecom',
      new FhirArray(new ContactPoint()),
      'phone number',
      false
    )
  )
  this.fields.push(
    new Field(
      'Gender',
      new FhirCode([
        { text: 'Male', value: 'male' },
        { text: 'Female', value: 'female' },
        { text: 'Other', value: 'other' },
        { text: 'Unknown', value: 'unknown' }
      ]),
      '',
      true,
      'gender'
    )
  )
  this.fields.push(
    new Field('Birth Date', new FhirDate(), '', true, 'birthDate')
  )
  // TODO: Deceased
  this.fields.push(new Field('Address', new Address(), '', false))
  this.fields.push(
    new Field('Marital Status', new CodeableConcept(), '', false)
  )
  // TODO: Multiple Birth
  this.fields.push(new Field('Photo', new Attachment(), '', false))
  // TODO: Contact
  // TODO: Communication
  this.fields.push(
    new Field('General Practitioner', new Reference(), '', false)
  )
  this.fields.push(
    new Field('Managing Organization', new Reference(), '', false)
  )
  // TODO: Link

  this.updateResource = (auth, client, signal, requestHeaders, id) => {
    updatePatient(auth, client, signal, requestHeaders, id, this.fhirSchema())
  }
}

export function patientToObject(json) {
  var patient = new Patient()

  patient.getField('active').resource.setValue(_get(json, 'active', true))

  patient.getField('gender').resource.setValue(_get(json, 'gender', 'unknown'))
  patient.getField('birthDate').resource.setValue(_get(json, 'birthDate', ''))

  let names = _get(json, 'name', [])
  let fhirNames = patient.getField('name').resource

  for (const name of names) {
    let fhirName = new HumanName()
    fhirName.getField('family').resource.setValue(_get(name, 'family', ''))
    fhirName.getField('given').resource.setValue(_get(name, 'given', ''))
    fhirName.getField('prefix').resource.setValue(_get(name, 'prefix', []))
    fhirName.getField('suffix').resource.setValue(_get(names, 'suffix', []))

    fhirNames.addItem(fhirName)
  }

  patient.fullResource = json

  return patient
}
