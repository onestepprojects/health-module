import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirBoolean from './datatypes/primitive/FhirBoolean'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirDate from './datatypes/primitive/FhirDate'

import Address from './datatypes/Address'
import Attachment from './datatypes/Attachment'
import CodeableConcept from './datatypes/CodeableConcept'
import ContactPoint from './datatypes/ContactPoint'
import HumanName from './datatypes/HumanName'
import Identifier from './datatypes/Identifier'
import Qualification from './datatypes/Qualification'

export default function Practitioner() {
  this.name = 'Practitioner'

  Resource.call(this)

  this.fields.push(
    new Field('Identifier', new FhirArray(new Identifier()), 'identifier', true)
  )
  this.fields.push(new Field('Active', new FhirBoolean(), 'active', true))
  this.fields.push(new Field('Name', new HumanName(), 'name', true))
  this.fields.push(
    new Field('Telecom', new FhirArray(ContactPoint()), 'phone number', true)
  )
  this.fields.push(new Field('Address', new Address(), '', true))
  this.fields.push(
    new Field(
      'Gender',
      new FhirCode([
        { text: 'Male', value: 'male' },
        { text: 'Female', value: 'female' },
        { text: 'Other', value: 'other' },
        { text: 'Unknown', value: 'unknown' }
      ]),
      'gender',
      true
    )
  )
  this.fields.push(new Field('Birth Date', new FhirDate(), '', true))
  this.fields.push(new Field('Photo', new Attachment(), '', true))
  this.fields.push(
    new Field('Qualification', new Qualification(), 'qualification', true)
  )
  this.fields.push(
    new Field('Communication', new CodeableConcept(), 'communication', false)
  )
}
