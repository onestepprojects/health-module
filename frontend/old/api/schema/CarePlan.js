import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirDateTime from './datatypes/primitive/FhirDateTime'
import FhirString from './datatypes/primitive/FhirString'
import FhirUri from './datatypes/primitive/FhirUri'

import Activity from './datatypes/Activity'
import Annotation from './datatypes/Annotation'
import Canonical from './datatypes/Canonical'
import CodeableConcept from './datatypes/CodeableConcept'
import Identifier from './datatypes/Identifier'
import Period from './datatypes/Period'
import Reference from './datatypes/Reference'

export default function CarePlan() {
  this.name = 'CarePlan'

  Resource.call(this)

  this.field.push(new Field('identifier', new FhirArray(new Identifier())))
  this.field.push(
    new Field('instantiatesCanonical', new FhirArray(new Canonical()))
  )
  this.field.push(
    new Field(
      'Instantiates Uri',
      new FhirArray(new FhirUri()),
      'instantiatesUri',
      false
    )
  )
  this.field.push(
    new Field('Based On', new FhirArray(new Reference()), 'basedOn', false)
  ) // TODO pass Reference
  this.field.push(
    new Field('Replaces', new FhirArray(new Reference()), 'replaces', false)
  ) // TODO pass Reference
  this.field.push(
    new Field('Part Of', new FhirArray(new Reference()), 'partOf', false)
  ) // TODO pass Reference
  this.field.push(new Field('Status', new FhirCode(), 'status', true))
  this.field.push(new Field('Intent', new FhirCode(), 'intent', true))
  this.field.push(
    new Field(
      'Category',
      new FhirArray(new CodeableConcept()),
      'category',
      true
    )
  )
  this.field.push(new Field('Title', new FhirString(), 'title', true))
  this.field.push(
    new Field('Description', new FhirString(), 'description', true)
  )
  this.field.push(new Field('Subject', new Reference(), 'subject', true)) // TODO pass Reference
  this.field.push(new Field('Encounter', new Reference(), 'encounter', true)) // TODO pass Reference
  this.field.push(new Field('Period', new Period(), 'period', true))
  this.field.push(new Field('Created', new FhirDateTime(), 'created', true))
  this.field.push(new Field('Author', new Reference(), 'author', true)) // TODO pass Reference
  this.field.push(
    new Field(
      'Contributor',
      new FhirArray(new Reference()),
      'contributor',
      true
    )
  ) // TODO pass Reference
  this.field.push(
    new Field('Care Team', new FhirArray(new Reference()), 'careTeam', true)
  ) // TODO pass Reference
  this.field.push(
    new Field('Addresses', new FhirArray(new Reference()), 'addresses', true)
  ) // TODO pass Reference
  this.field.push(
    new Field(
      'Supporting Info',
      new FhirArray(new Reference()),
      'supportingInfo',
      true
    )
  ) // TODO pass Reference
  this.field.push(
    new Field('Goal', new FhirArray(new Reference()), 'goal', true)
  ) // TODO pass Reference
  this.field.push(
    new Field('Activity', new FhirArray(new Activity()), 'activity', true)
  )
  this.field.push(
    new Field('Note', new FhirArray(new Annotation()), 'note', true)
  )
}
