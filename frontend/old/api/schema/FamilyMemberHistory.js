import Resource from './Resource'
import Field from './Field'

import FhirAge from './datatypes/primitive/FhirAge'
import FhirArray from './datatypes/primitive/FhirArray'
import FhirBoolean from './datatypes/primitive/FhirBoolean'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirDate from './datatypes/primitive/FhirDate'
import FhirDateTime from './datatypes/primitive/FhirDateTime'
import FhirString from './datatypes/primitive/FhirString'
import FhirUri from './datatypes/primitive/FhirUri'

import Annotation from './datatypes/Annotation'
import Canonical from './datatypes/Canonical'
import CodeableConcept from './datatypes/CodeableConcept'
import Condition from './datatypes/Condition'
import Identifier from './datatypes/Identifier'
import Period from './datatypes/Period'
import Range from './datatypes/Range'
import Reference from './datatypes/Reference'

export default function FamilyMemberHistory() {
  Resource.call(this)

  this.field.push(
    new Field('Identifier', new FhirArray(new Identifier()), 'identifier', true)
  )
  this.field.push(
    new Field(
      'Instantiates Canonical',
      new FhirArray(new Canonical()),
      'instantiatesCanonical',
      true
    ) // TODO Canonical is a stub, needs implementation
  )
  this.field.push(
    new Field(
      'Instantiates Uri',
      new FhirArray(new FhirUri()),
      'instantiatesUri',
      true
    )
  )
  this.field.push(new Field('Status', new FhirCode()), 'status', true)
  this.field.push(
    new Field(
      'Data Absent Reason',
      new CodeableConcept(),
      'dataAbsentReason',
      true
    )
  )
  this.field.push(new Field('Patient', new Reference(), 'patient', true)) // TODO pass reference
  this.field.push(new Field('Date', new FhirDateTime(), 'date', true))
  this.field.push(new Field('Name', new FhirString(), 'name', true))
  this.field.push(
    new Field('Relationship', new CodeableConcept(), 'relationship', true)
  )
  this.field.push(new Field('Sex', new CodeableConcept(), 'sex', true))
  this.field.push(new Field('Born Period', new Period(), 'bornPeriod', true))
  this.field.push(new Field('Born Date', new FhirDate(), 'bornDate', true))
  this.field.push(
    new Field('Born String', new FhirString(), 'bornString', true)
  )
  this.field.push(new Field('Age Age', new FhirAge(), 'age', true)) // TODO FhirAge is a stub, needs implementation
  this.field.push(new Field('Age Range', new Range(), 'ageRange', true))
  this.field.push(new Field('Age String', new FhirString(), 'ageString', true))
  this.field.push(
    new Field('Estimated Age', new FhirBoolean(), 'estimatedAge', true)
  )
  this.field.push(
    new Field('Deceased Boolean', new FhirBoolean(), 'deceasedBoolean', true)
  )
  this.field.push(new Field('Deceased Age', new FhirAge(), 'deceasedAge', true))
  this.field.push(
    new Field('Deceased Range', new Range(), 'deceasedRange', true)
  )
  this.field.push(
    new Field('Deceased String', new FhirString(), 'deceasedString', true)
  )
  this.field.push(
    new Field(
      'Reason Code',
      new FhirArray(new CodeableConcept()),
      'reasonCode',
      true
    )
  )
  this.field.push(
    new Field(
      'Reason Reference',
      new FhirArray(new Reference()),
      'reasonReference',
      true
    )
  ) // TODO pass Reference
  this.field.push(
    new Field('Note', new FhirArray(new Annotation()), 'note', true)
  )
  this.field.push(
    new Field('Condition', new FhirArray(new Condition()), 'condition', true)
  )
}
