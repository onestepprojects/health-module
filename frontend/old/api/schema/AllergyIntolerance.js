import Resource from './Resource'
import Field from './Field'

import FhirAge from './datatypes/primitive/FhirAge'
import FhirArray from './datatypes/primitive/FhirArray'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirDateTime from './datatypes/primitive/FhirDateTime'
import FhirString from './datatypes/primitive/FhirString'

import Annotation from './datatypes/Annotation'
import CodeableConcept from './datatypes/CodeableConcept'
import Identifier from './datatypes/Identifier'
import Period from './datatypes/Period'
import Range from './datatypes/Range'
import Reaction from './datatypes/Reaction'
import Reference from './datatypes/Reference'

export default function AllergyIntolerance() {
  this.name = 'AllergyIntolerance'

  Resource.call(this)

  this.fields.push(new Field('identifier', new FhirArray(new Identifier())))
  this.fields.push(new Field('clinicalStatus', new CodeableConcept()))
  this.fields.push(new Field('verificationStatus', new CodeableConcept()))
  this.fields.push(new Field('type', new FhirCode()))
  this.fields.push(new Field('category', new FhirArray(new FhirCode())))
  this.fields.push(new Field('criticality', new FhirCode()))
  this.fields.push(new Field('code', new CodeableConcept()))
  this.fields.push(new Field('patient', new Reference())) // TODO pass a Resource
  this.fields.push(new Field('encounter', new Reference())) // TODO pass a Resource
  this.fields.push(new Field('onsetDateTime', new FhirDateTime()))
  this.fields.push(new Field('onsetAge', new FhirAge()))
  this.fields.push(new Field('onsetPeriod', new Period()))
  this.fields.push(new Field('onsetRange', new Range()))
  this.fields.push(new Field('onsetString', new FhirString()))
  this.fields.push(new Field('recordedDate', new FhirDateTime()))
  this.fields.push(new Field('recorder', new Reference())) // TODO pass a Resource enumeration
  this.fields.push(new Field('asserter', new Reference())) // TODO pass a Resource enumeration
  this.fields.push(new Field('lastOccurrence', new FhirDateTime()))
  this.fields.push(new Field('note', new FhirArray(new Annotation())))
  this.fields.push(new Field('reaction', new FhirArray(new Reaction())))
}
