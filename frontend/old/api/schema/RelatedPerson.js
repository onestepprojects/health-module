import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirBoolean from './datatypes/primitive/FhirBoolean'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirDate from './datatypes/primitive/FhirDate'

import Address from './datatypes/Address'
import Attachment from './datatypes/Attachment'
import CodeableConcept from './datatypes/CodeableConcept'
import Communication from './datatypes/Communication'
import ContactPoint from './datatypes/ContactPoint'
import HumanName from './datatypes/HumanName'
import Identifier from './datatypes/Identifier'
import Period from './datatypes/Period'
import Reference from './datatypes/Reference'

export default function RelatedPerson() {
  this.name = 'RelatedPerson'

  Resource.call(this)

  this.field.push(
    new Field('identifier', new FhirArray(new Identifier()), 'identifier', true)
  )
  this.field.push(new Field('active', new FhirBoolean(), 'active', true))
  this.field.push(new Field('patient', new Reference(), 'patient', true)) // TODO Reference
  this.field.push(
    new Field(
      'relationship',
      new FhirArray(new CodeableConcept()),
      'relationship',
      true
    )
  )
  this.field.push(
    new Field('name', new FhirArray(new HumanName()), 'name', true)
  )
  this.fields.push(
    new Field(
      'Telecom',
      new FhirArray(new ContactPoint()),
      'phone number',
      true
    )
  )
  this.fields.push(
    new Field(
      'Gender',
      new FhirCode([
        { text: 'Male', value: 'male' },
        { text: 'Female', value: 'female' },
        { text: 'Other', value: 'other' },
        { text: 'Unknown', value: 'unknown' }
      ]),
      '',
      true
    )
  )
  this.fields.push(new Field('Birth Date', new FhirDate(), '', true))
  this.fields.push(new Field('Address', new Address(), '', true))
  this.fields.push(new Field('Photo', new Attachment(), '', true))
  this.field.push(new Field('Period', new Period(), 'period', true))
  this.field.push(
    new Field(
      'Communication',
      new FhirArray(new Communication()),
      'communication',
      true
    )
  )
}
