import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirBoolean from './datatypes/primitive/FhirBoolean'
import FhirString from './datatypes/primitive/FhirString'

import Address from './datatypes/Address'
import CodeableConcept from './datatypes/CodeableConcept'
import Contact from './datatypes/Contact'
import ContactPoint from './datatypes/ContactPoint'
import Identifier from './datatypes/Identifier'
import Reference from './datatypes/Reference'

export default function Organization() {
  this.name = 'Organization'

  Resource.call(this)

  this.fields.push(
    new Field('Identifier', new FhirArray(new Identifier()), 'identifier', true)
  )
  this.fields.push(new Field('Active', new FhirBoolean(), 'active', true))
  this.fields.push(
    new Field('Type', new FhirArray(new CodeableConcept()), 'type', true)
  )
  this.fields.push(new Field('Name', new FhirString(), 'name', true))
  this.fields.push(
    new Field('Alias', new FhirArray(new FhirString()), 'alias', true)
  )
  this.fields.push(
    new Field(
      'Telecom',
      new FhirArray(new ContactPoint()),
      'phone number',
      true
    )
  )
  this.fields.push(
    new Field('Address', new FhirArray(new Address()), 'address', true)
  )
  this.fields.push(new Field('Part Of', new Reference(), 'partOf', true)) // TODO pass reference
  this.fields.push(new Field('Contact', new Contact(), 'contact', true))
  this.fields.push(
    new Field('Endpoint', new FhirArray(new Reference()), 'endpoint', true)
  ) // TODO pass reference
}
