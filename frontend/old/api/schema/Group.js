import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirBoolean from './datatypes/primitive/FhirBoolean'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirString from './datatypes/primitive/FhirString'
import FhirUnsignedInt from './datatypes/primitive/FhirUnsignedInt'
import Characteristic from './datatypes/Characteristic'
import CodeableConcept from './datatypes/CodeableConcept'
import Identifier from './datatypes/Identifier'
import Member from './datatypes/Member'
import Reference from './datatypes/Reference'

export default function Group() {
  this.name = 'Group'

  Resource.call(this)

  this.field.push(new Field('identifier', new FhirArray(new Identifier())))
  this.field.push(new Field('active', new FhirBoolean()))
  this.field.push(new Field('type', new FhirCode()))
  this.field.push(new Field('actual', new FhirBoolean()))
  this.field.push(new Field('code', new CodeableConcept()))
  this.field.push(new Field('name', new FhirString()))
  this.field.push(new Field('quantity', new FhirUnsignedInt()))
  this.field.push(new Field('managingEntity', new Reference())) // TODO pass Reference
  this.field.push(
    new Field('characteristic', new FhirArray(new Characteristic()))
  )
  this.field.push(new Field('member', new FhirArray(new Member())))
}
