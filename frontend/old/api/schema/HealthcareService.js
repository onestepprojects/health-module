import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirBoolean from './datatypes/primitive/FhirBoolean'
import FhirMarkdown from './datatypes/primitive/FhirMarkdown'
import FhirString from './datatypes/primitive/FhirString'

import AvailableTime from './datatypes/AvailableTime'
import Attachment from './datatypes/Attachment'
import CodeableConcept from './datatypes/CodeableConcept'
import ContactPoint from './datatypes/ContactPoint'
import Eligibility from './datatypes/Eligibility'
import Identifier from './datatypes/Identifier'
import NotAvailable from './datatypes/NotAvailable'
import Reference from './datatypes/Reference'

export default function HealthcareService() {
  this.name = 'HealthcareService'

  Resource.call(this)

  this.field.push(new Field('identifier', new FhirArray(new Identifier())))
  this.field.push(new Field('active', new FhirBoolean()))
  this.field.push(new Field('providedBy', new Reference())) // TODO pass Reference
  this.field.push(new Field('category', new FhirArray(new CodeableConcept())))
  this.field.push(new Field('type', new FhirArray(new CodeableConcept())))
  this.field.push(new Field('specialty', new FhirArray(new CodeableConcept())))
  this.field.push(new Field('location', new FhirArray(new Reference()))) // TODO pass Reference
  this.field.push(new Field('name', new FhirString()))
  this.field.push(new Field('comment', new FhirString()))
  this.field.push(new Field('extraDetails', new FhirMarkdown())) // TODO convert string to markdown
  this.field.push(new Field('photo', new Attachment()))
  this.field.push(new Field('telecom', new FhirArray(new ContactPoint())))
  this.field.push(new Field('coverageArea', new FhirArray(new Reference()))) // TODO pass Reference
  this.field.push(
    new Field('serviceProvisionCode', new FhirArray(new CodeableConcept()))
  )
  this.field.push(new Field('eligibility', new FhirArray(new Eligibility())))
  this.field.push(new Field('program', new FhirArray(new CodeableConcept())))
  this.field.push(
    new Field('characteristic', new FhirArray(new CodeableConcept()))
  )
  this.field.push(
    new Field('communication', new FhirArray(new CodeableConcept()))
  )
  this.field.push(
    new Field('referralMethod', new FhirArray(new CodeableConcept()))
  )
  this.field.push(new Field('appointmentRequired', new FhirBoolean()))
  this.field.push(
    new Field('availableTime', new FhirArray(new AvailableTime()))
  )
  this.field.push(new Field('notAvailable', new FhirArray(new NotAvailable())))
  this.field.push(new Field('availabilityExceptions', new FhirString()))
  this.field.push(new Field('endpoint', new FhirArray(new Reference())))
}
