import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirDateTime from './datatypes/primitive/FhirDateTime'
import FhirString from './datatypes/primitive/FhirString'
import FhirUri from './datatypes/primitive/FhirUri'

import CodeableConcept from './datatypes/CodeableConcept'
import Evidence from './datatypes/Evidence'
import Identifier from './datatypes/Identifier'
import Mitigation from './datatypes/Mitigation'
import Period from './datatypes/Period'
import Reference from './datatypes/Reference'

export default function DetectedIssue() {
  this.name = 'DetectedIssue'

  Resource.call(this)

  this.field.push(new Field('identifier', new FhirArray(new Identifier())))
  this.field.push(new Field('status', new FhirCode()))
  this.field.push(new Field('code', new CodeableConcept()))
  this.field.push(new Field('severity', new FhirCode()))
  this.field.push(new Field('patient', new Reference())) // TODO pass Reference
  this.field.push(new Field('identifiedDateTime', new FhirDateTime()))
  this.field.push(new Field('identifiedPeriod', new Period()))
  this.field.push(new Field('author', new Reference())) // TODO pass Reference
  this.field.push(new Field('implicated', new FhirArray(new Reference()))) // TODO pass Reference
  this.field.push(new Field('evidence', new FhirArray(new Evidence())))
  this.field.push(new Field('detail', new FhirString()))
  this.field.push(new Field('reference', new FhirUri()))
  this.field.push(new Field('mitigation', new FhirArray(new Mitigation())))
}
