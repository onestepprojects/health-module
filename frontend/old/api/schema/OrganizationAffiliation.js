import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirBoolean from './datatypes/primitive/FhirBoolean'

import CodeableConcept from './datatypes/CodeableConcept'
import ContactPoint from './datatypes/ContactPoint'
import Identifier from './datatypes/Identifier'
import Period from './datatypes/Period'
import Reference from './datatypes/Reference'

export default function OrganizationAffiliation() {
  this.name = 'OrganizationAffiliation'

  Resource.call(this)

  this.field.push(
    new Field('Identifier', new FhirArray(new Identifier()), 'identifier', true)
  )
  this.field.push(new Field('Active', new FhirBoolean(), 'active', true))
  this.field.push(new Field('Period', new Period(), 'period', true))
  this.field.push(
    new Field('Organization', new Reference(), 'organization', true)
  ) // TODO pass reference
  this.field.push(
    new Field(
      'Participating Organization',
      new Reference(),
      'participatingOrganization',
      true
    )
  ) // TODO pass reference
  this.field.push(new Field('Network', new Reference(), 'network', true)) // TODO pass reference
  this.field.push(
    new Field('Code', new FhirArray(new CodeableConcept()), 'code', true)
  )
  this.field.push(
    new Field(
      'Specialty',
      new FhirArray(new CodeableConcept()),
      'specialty',
      true
    )
  )
  this.field.push(
    new Field('Location', new FhirArray(new Reference()), 'location', true)
  ) // TODO pass reference
  this.field.push(
    new Field(
      'Healthcare Service',
      new FhirArray(new Reference()),
      'healthcareService',
      true
    )
  ) // TODO pass reference
  this.fields.push(
    new Field(
      'Telecom',
      new FhirArray(new ContactPoint()),
      'phone number',
      true
    )
  )
  this.field.push(
    new Field('Endpoint', new FhirArray(new Reference()), 'endpoint', true)
  ) // TODO pass reference
}
