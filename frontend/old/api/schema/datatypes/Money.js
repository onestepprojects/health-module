import Resource from '../Resource'

import Field from '../Field'

import FhirCode from './primitive/FhirCode'
import FhirDecimal from './primitive/FhirDecimal'

export default function Money() {
  Resource.call(this)

  this.fields.push(new Field('value', new FhirDecimal(), 'value', false))
  this.fields.push(new Field('currency', new FhirCode(), 'currency', false))
}
