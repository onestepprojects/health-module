import Resource from '../Resource'
import Field from '../Field'

import FhirBoolean from './primitive/FhirBoolean'

import CodeableConcept from './CodeableConcept'
import Ratio from './Ratio'
import Reference from './Reference'

export default function Ingredient() {
  Resource.call(this)

  this.fields.push(
    new Field(
      'Item Codeable Concept',
      new CodeableConcept(),
      'itemCodeableConcept',
      false
    )
  )
  this.fields.push(
    new Field('Item Reference', new Reference(), 'itemReference', false)
  ) // TODO pass a Resource
  this.fields.push(new Field('Is Active', new FhirBoolean(), 'isActive', false))
  this.fields.push(new Field('strength', new Ratio(), 'strength', false))
}
