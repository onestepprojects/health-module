import Resource from '../Resource'

import Field from '../Field'

import FhirString from './primitive/FhirString'
import Coding from './Coding'

export default function CodeableConcept() {
  // TODO: Work out select for codes

  Resource.call(this)

  this.fields.push(new Field('Coding', new Coding(), 'coding', false))
  this.fields.push(new Field('Text', new FhirString(), 'text', false))
}
