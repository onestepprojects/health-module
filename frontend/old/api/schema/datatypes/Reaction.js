import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'
import FhirDateTime from './primitive/FhirDateTime'
import FhirString from './primitive/FhirString'
import FhirCode from './primitive/FhirCode'

import CodeableConcept from './CodeableConcept'
import Annotation from './Annotation'

export default function Reaction() {
  Resource.call(this)

  this.fields.push(
    new Field(
      'Substance',
      new FhirArray(new CodeableConcept()),
      'substance',
      true
    )
  )
  this.fields.push(
    new Field(
      'Manifestation',
      new FhirArray(new CodeableConcept()),
      'manifestation',
      true
    )
  )
  this.fields.push(
    new Field('Description', new FhirString(), 'description', true)
  )
  this.fields.push(new Field('Onset', new FhirDateTime(), 'onset', true))
  this.fields.push(new Field('Severity', new FhirCode(), 'severity', true))
  this.fields.push(
    new Field('ExposureRate', new CodeableConcept(), 'exposureRate', true)
  )
  this.fields.push(
    new Field('Note', new FhirArray(new Annotation()), 'note', true)
  )
}
