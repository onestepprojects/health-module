import Resource from '../Resource'
import Field from '../Field'

import FhirMarkdown from './primitive/FhirMarkdown'

import CodeableConcept from './CodeableConcept'

export default function Eligibility() {
  Resource.call(this)

  this.fields.push(new Field('Code', new CodeableConcept(), 'code', true))
  this.fields.push(new Field('Comment', new FhirMarkdown(), 'comment', true))
}
