import Resource from '../Resource'

import Field from '../Field'

import FhirString from './primitive/FhirString'
import FhirCode from './primitive/FhirCode'
import Period from './Period'
import FhirPositiveInt from './primitive/FhirPositiveInt'

export default function ContactPoint() {
  Resource.call(this)

  this.fields.push(
    new Field(
      'System',
      new FhirCode([
        { text: 'Phone', value: 'phone' },
        { text: 'Fax', value: 'fax' },
        { text: 'Email', value: 'email' },
        { text: 'Pager', value: 'pager' },
        { text: 'URL', value: 'url' },
        { text: 'SMS', value: 'sms' },
        { text: 'Other', value: 'other' }
      ]),
      '',
      false
    )
  )
  this.fields.push(new Field('Value', new FhirString(), 'value', false))
  this.fields.push(
    new Field(
      'Use',
      new FhirCode([
        { text: 'Home', value: 'home' },
        { text: 'Work', value: 'work' },
        { text: 'Temp', value: 'temp' },
        { text: 'Old', value: 'old' },
        { text: 'Mobile', value: 'mobile' }
      ])
    )
  )
  this.fields.push(new Field('Rank', new FhirPositiveInt(), 'rank', false))
  this.fields.push(new Field('Period', new Period(), 'period', false))
}
