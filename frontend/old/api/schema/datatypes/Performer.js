import Resource from '../Resource'
import Field from '../Field'

import CodeableConcept from './CodeableConcept'
import Reference from './Reference'

export default function Performer() {
  Resource.call(this)

  this.fields.push(
    new Field('function', new CodeableConcept(), 'function', true)
  )
  this.fields.push(new Field('actor', new Reference(), 'actor', true)) // TODO pass reference
}
