import Resource from '../Resource'
import Field from '../Field'

import FhirDateTime from './primitive/FhirDateTime'
import FhirString from './primitive/FhirString'
import FhirUri from './primitive/FhirUri'

export default function Education() {
  Resource.call(this)

  this.fields.push(
    new Field('Document Type', new FhirString(), 'documentType', false)
  )
  this.fields.push(new Field('Reference', new FhirUri(), 'reference', false))
  this.fields.push(
    new Field('Publication Date', new FhirDateTime(), 'publicationDate', false)
  )
  this.fields.push(
    new Field(
      'Presentation Date',
      new FhirDateTime(),
      'presentationDate',
      false
    )
  )
}
