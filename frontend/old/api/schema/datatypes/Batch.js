import Resource from '../Resource'
import Field from '../Field'

import FhirDateTime from './primitive/FhirDateTime'
import FhirString from './primitive/FhirString'

export default function Batch() {
  Resource.call(this)

  this.fields.push(new Field('Lot Number', new FhirString(), 'lotNumber', true))
  this.fields.push(
    new Field('Expiration Date', new FhirDateTime(), 'expirationDate', true)
  )
}
