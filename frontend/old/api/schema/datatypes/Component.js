import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'
import FhirBoolean from './primitive/FhirBoolean'
import FhirDateTime from './primitive/FhirDateTime'
import FhirInteger from './primitive/FhirInteger'
import FhirString from './primitive/FhirString'
import FhirTime from './primitive/FhirTime'

import CodeableConcept from './CodeableConcept'
import Period from './Period'
import Quantity from './Quantity'
import Ratio from './Ratio'
import Range from './Range'
import SampledData from './SampledData'

export default function Component() {
  Resource.call(this)

  this.fields.push(new Field('Code', new CodeableConcept(), 'code', true))
  this.fields.push(
    new Field('Value Quantity', new Quantity(), 'quantity', true)
  )
  this.fields.push(
    new Field(
      'Value Codeable Concept',
      new CodeableConcept(),
      'valueCodeableConcept',
      true
    )
  )
  this.fields.push(
    new Field('Value String', new FhirString(), 'valueString', true)
  )
  this.fields.push(
    new Field('Value Boolean', new FhirBoolean(), 'valueBoolean', true)
  )
  this.fields.push(
    new Field('Value Integer', new FhirInteger(), 'valueInteger', true)
  )
  this.fields.push(new Field('Value Range', new Range(), 'valueRange', true))
  this.fields.push(new Field('Value Ratio', new Ratio(), 'valueRatio', true))
  this.fields.push(
    new Field('Value Sampled Data', new SampledData(), 'valueSampledData', true)
  )
  this.fields.push(new Field('Value Time', new FhirTime(), 'valueTime', true))
  this.fields.push(
    new Field('Value DateTime', new FhirDateTime(), 'valueDateTime', true)
  )
  this.fields.push(new Field('Value Period', new Period(), 'valuePeriod', true))
  this.fields.push(
    new Field(
      'Data Absent Reason',
      new CodeableConcept(),
      'dataAbsentReason',
      true
    )
  )
  this.fields.push(
    new Field(
      'Interpretation',
      new FhirArray(new CodeableConcept()),
      'interpretation',
      true
    )
  )
  this.fields.push(
    new Field(
      'Reference Range',
      new FhirArray(new FhirString()),
      'referenceRange',
      true
    )
  ) // TODO this is not clearly explained in spec
}
