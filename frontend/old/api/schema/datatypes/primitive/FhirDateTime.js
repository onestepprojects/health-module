import Primitive from './Primitive'

export default function FhirDateTime(value) {
  if (value === undefined) {
    this.default = Date.now().toString()
  } else {
    this.default = value
  }

  Primitive.call(this, this.default)

  this.formSchema = () => {
    return {
      type: 'text',
      inputType: 'datetime-local'
    }
  }
}
