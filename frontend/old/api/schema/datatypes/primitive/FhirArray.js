import Primitive from './Primitive'

export default function FhirArray(value) {
  if (value === undefined) {
    this.default = []
  } else {
    this.default = [value]
  }

  Primitive.call(this, this.default)

  this.formSchema = () => {
    let items = []

    for (const item of this.default) {
      items.push(item.formSchema())
    }

    return {
      type: 'paneldynamic',
      templateElements: items
    }
  }

  this.addItem = (item) => {
    this.value.push(item)
  }
}
