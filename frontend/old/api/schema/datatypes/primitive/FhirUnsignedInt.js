import Primitive from './Primitive'

export default function FhirUnsignedInt(value) {
  if (value === undefined) {
    this.default = 0
  } else {
    this.default = value
  }

  Primitive.call(this, this.default)

  this.formSchema = () => {
    return {
      type: 'text',
      inputType: 'number',
      validators: [
        {
          type: 'numeric',
          minValue: 1
        }
      ]
    }
  }
}
