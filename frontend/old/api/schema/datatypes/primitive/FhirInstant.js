import Primitive from './Primitive'

export default function FhirInstant(value) {
  if (value === undefined) {
    this.default = ''
  } else {
    this.default = value
  }

  Primitive.call(this, this.default)

  this.formSchema = () => {
    return {
      type: 'text',
      inputType: 'datetime-local'
    }
  }
}
