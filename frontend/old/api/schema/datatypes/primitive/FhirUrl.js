import Primitive from './Primitive'

export default function FhirUrl(value) {
  if (value === undefined) {
    this.default = ''
  } else {
    this.default = value
  }

  Primitive.call(this, this.default)

  this.formSchema = () => {
    return {
      type: 'text',
      inputType: 'url',
      autocomplete: 'url'
    }
  }
}
