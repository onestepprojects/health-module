import Primitive from './Primitive'

export default function FhirMarkdown(value) {
  if (value === undefined) {
    this.default = ''
  } else {
    this.default = value
  }

  // TODO Support Markdown syntax

  Primitive.call(this, this.default)
}
