import Primitive from './Primitive'

export default function FhirAge(value) {
  if (value === undefined) {
    this.default = ''
    /* TODO
     * (code.exists() or value.empty()) and (system.empty() or system = %ucum) and
     * (value.empty() or value.hasValue().not() or value > 0)
     */
  } else {
    this.default = value
  }

  Primitive.call(this, this.default)

  this.formSchema = () => {
    return {
      type: 'text',
      inputType: 'number',
      validators: [
        {
          type: 'numeric',
          minValue: 1,
          maxValue: 150
        }
      ]
    }
  }
}
