import Primitive from './Primitive'

export default function FhirInteger(value) {
  if (value === undefined) {
    this.default = 0
  } else {
    this.default = value
  }

  Primitive.call(this, this.default)
}
