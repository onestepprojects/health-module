/**
 * Represents datatypes that do not contain any datatypes themselves
 *
 * @param {string} value
 */
export default function Primitive(value) {
  this.value = value

  this.fhirSchema = () => {
    if (Array.isArray(this.value)) {
      var data = []
      for (const item of this.value) {
        try {
          data.push(item.fhirSchema())
        } catch {
          data.push(item)
        }
      }
      return data
    } else {
      return this.value
    }
  }

  this.formSchema = () => {
    // defined at child class
  }

  this.formData = () => {
    if (Array.isArray(this.value)) {
      var data = []
      for (const item of this.value) {
        if (typeof item === 'object') {
          data.push(item.formData())
        } else {
          data.push(item)
        }
      }
      return data
    } else {
      return this.value
    }
  }

  this.setValue = (value) => {
    this.value = value
  }
}
