import Primitive from './Primitive'

export default function FhirTime(value) {
  if (value === undefined) {
    this.default = '00:00:00'
  } else {
    this.default = value
  }

  /* TODO
   * A time during the day, in the format hh:mm:ss. There is no date specified. Seconds
   * must be provided due to schema type constraints but may be zero-filled and may be ignored
   * at receiver discretion. The time "24:00" SHALL NOT be used. A time zone SHALL NOT be
   * present. Times can be converted to a Duration since midnight.
   * xs:time	A JSON string - an xs:time
   * Regex: ([01][0-9]|2[0-3]):[0-5][0-9]:([0-5][0-9]|60)(\.[0-9]+)?
   */

  Primitive.call(this, this.default)

  this.formSchema = () => {
    return {
      type: 'text',
      inputType: 'time'
    }
  }
}
