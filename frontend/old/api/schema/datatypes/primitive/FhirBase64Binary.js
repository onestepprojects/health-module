import Primitive from './Primitive'

export default function FhirBase64Binary(value) {
  if (value === undefined) {
    this.default = null
  } else {
    this.default = value
  }

  Primitive.call(this, this.default)

  this.formSchema = () => {
    return {
      type: 'file',
      waitForUpload: true,
      showPreview: false,
      allowImagesPreview: false,
      maxSize: 0
    }
  }
}
