import Primitive from './Primitive'

export default function FhirPositiveInt(value) {
  if (value === undefined) {
    this.default = 1
  } else {
    this.default = value
  }

  Primitive.call(this, this.default)

  this.formSchema = () => {
    return {
      type: 'text',
      inputType: 'number',
      validators: [
        {
          type: 'numeric',
          minValue: 1
        }
      ]
    }
  }
}
