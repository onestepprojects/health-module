import Primitive from './Primitive'

export default function FhirCode(selection, value) {
  this.selection = selection

  if (value === undefined) {
    this.default = ''
  } else {
    this.default = value
  }

  Primitive.call(this, this.default)

  this.formSchema = () => {
    if (this.selection === undefined) {
      return {
        type: 'text'
      }
    } else {
      return {
        type: 'dropdown',
        choices: this.selection,
        defaultValue: this.default
      }
    }
  }
}
