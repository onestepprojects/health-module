import Primitive from './Primitive'

export default function Duration(value) {
  if (value === undefined) {
    this.default = '00:00:00'
  } else {
    this.default = value
  }

  /* TODO
   * There SHALL be a code if there is a value and it SHALL be an expression of time. If system is present, it SHALL be
   * UCUM.	code.exists() implies ((system = %ucum) and value.exists())
   */

  Primitive.call(this, this.default)
}
