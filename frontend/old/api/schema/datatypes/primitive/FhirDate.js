import Primitive from './Primitive'
import moment from 'moment'

export default function FhirDate(value) {
  if (value === undefined) {
    this.default = Date.now().toString()
  } else {
    this.default = value
  }

  Primitive.call(this, this.default)

  this.formSchema = () => {
    return {
      type: 'text',
      inputType: 'date'
    }
  }

  this.fhirSchema = () => {
    let date = new Date(this.value)

    return moment(date).format('YYYY-MM-DD')
  }
}
