import Primitive from './Primitive'

export default function FhirBoolean(value) {
  if (value === undefined) {
    this.default = false
  } else {
    this.default = value
  }

  Primitive.call(this, this.default)

  this.formSchema = () => {
    return {
      type: 'boolean',
      defaultValue: 'true'
    }
  }

  this.initialValues = () => false
}
