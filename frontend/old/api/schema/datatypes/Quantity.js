import Resource from '../Resource'

import Field from '../Field'

import FhirCode from './primitive/FhirCode'
import FhirString from './primitive/FhirString'
import FhirUri from './primitive/FhirUri'
import FhirDecimal from './primitive/FhirDecimal'

export default function Quantity() {
  Resource.call(this)

  this.fields.push(new Field('Value', new FhirDecimal(), 'value', true))
  this.fields.push(new Field('Comparator', new FhirCode(), 'comparator', true))
  this.fields.push(new Field('Unit', new FhirString(), 'unit', true))
  this.fields.push(new Field('System', new FhirUri(), 'system', true))
  this.fields.push(new Field('Code', new FhirCode(), 'code', true))
}
