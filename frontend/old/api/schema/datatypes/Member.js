import Resource from '../Resource'
import Field from '../Field'

import FhirBoolean from './primitive/FhirBoolean'
import Period from './Period'
import Reference from './Reference'

export default function Member() {
  Resource.call(this)

  this.field.push(new Field('Entity', new Reference(), 'entity', true))
  this.field.push(new Field('Period', new Period(), 'period', true))
  this.field.push(new Field('Inactive', new FhirBoolean(), 'inactive', true))
}
