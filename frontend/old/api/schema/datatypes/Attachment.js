import Resource from '../Resource'

import Field from '../Field'

import FhirBase64Binary from './primitive/FhirBase64Binary'
import FhirCode from './primitive/FhirCode'
import FhirUrl from './primitive/FhirUrl'
import FhirUnsignedInt from './primitive/FhirUnsignedInt'
import FhirString from './primitive/FhirString'
import FhirDateTime from './primitive/FhirDateTime'

export default function Attachment() {
  Resource.call(this)

  this.fields.push(
    new Field('Content Type', new FhirCode(), 'contentType', true)
  )
  this.fields.push(new Field('Language', new FhirCode(), 'language', true))
  this.fields.push(new Field('Data', new FhirBase64Binary(), 'data', true))
  this.fields.push(new Field('Url', new FhirUrl(), 'url', true))
  this.fields.push(new Field('Size', new FhirUnsignedInt(), 'size', true))
  this.fields.push(new Field('Hash', new FhirBase64Binary(), 'hash', true))
  this.fields.push(new Field('Title', new FhirString(), 'title', true))
  this.fields.push(new Field('Time', new FhirDateTime(), 'time', true))
}
