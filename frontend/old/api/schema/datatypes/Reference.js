import Resource from '../Resource'

import Field from '../Field'

import FhirString from './primitive/FhirString'
import FhirUri from './primitive/FhirUri'
import FhirArray from './primitive/FhirArray'
import Identifier from './Identifier'

export default function Reference() {
  Resource.call(this)

  this.fields.push(new Field('Reference', new FhirString(), 'reference', false))
  this.fields.push(new Field('Type', new FhirUri(), 'type', false))
  this.fields.push(
    new Field(
      'Identifier',
      new FhirArray(new Identifier()),
      'identifier',
      false
    )
  )
  this.fields.push(new Field('Display', new FhirString(), 'display', false))
}
