import Resource from '../Resource'
import Field from '../Field'

import FhirDecimal from './primitive/FhirDecimal'

export default function Position() {
  Resource.call(this)

  this.field.push(new Field('Longitude', new FhirDecimal(), 'longitude', true))
  this.field.push(new Field('Latitude', new FhirDecimal(), 'latitude', true))
  this.field.push(new Field('Altitude', new FhirDecimal(), 'altitude', true))
}
