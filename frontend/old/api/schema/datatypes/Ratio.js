import Resource from '../Resource'

import Field from '../Field'

import Quantity from './Quantity'

export default function Ratio() {
  Resource.call(this)

  this.fields.push(new Field('Numerator', new Quantity(), 'numerator', false))
  this.fields.push(
    new Field('Denominator', new Quantity(), 'denominator', false)
  )
}
