import Resource from '../Resource'
import Field from '../Field'

import Causality from './Causality'
import Reference from './Reference'

export default function SuspectEntity() {
  Resource.call(this)

  this.fields.push(new Field('Instance', new Reference(), 'instance', true)) // TODO pass reference
  this.fields.push(new Field('Causality', new Causality(), 'causality', true))
}
