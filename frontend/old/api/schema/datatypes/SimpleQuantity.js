import Resource from '../Resource'

import Field from '../Field'

import FhirCode from './primitive/FhirCode'
import FhirString from './primitive/FhirString'
import FhirUri from './primitive/FhirUri'
import FhirDecimal from './primitive/FhirDecimal'

export default function SimpleQuantity() {
  Resource.call(this)

  this.fields.push(new Field('Value', new FhirDecimal(), 'value', false))
  this.fields.push(new Field('Unit', new FhirString(), 'unit', false))
  this.fields.push(new Field('System', new FhirUri(), 'system', false))
  this.fields.push(new Field('Code', new FhirCode(), 'code', false))
}
