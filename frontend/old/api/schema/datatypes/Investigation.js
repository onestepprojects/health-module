import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'

import CodeableConcept from './CodeableConcept'
import Reference from './Reference'

export default function Investigation() {
  Resource.call(this)

  this.field.push(new Field('Code', new CodeableConcept(), 'code', true))
  this.field.push(
    new Field('Item', new FhirArray(new Reference()), 'item', true)
  ) // TODO pass Reference
}
