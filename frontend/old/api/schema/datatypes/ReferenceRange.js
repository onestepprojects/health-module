import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'
import FhirString from './primitive/FhirString'

import CodeableConcept from './CodeableConcept'
import Quantity from './Quantity'
import Range from './Range'

export default function ReferenceRange() {
  Resource.call(this)

  this.field.push(new Field('Low', new Quantity(), 'low', true))
  this.field.push(new Field('High', new Quantity(), 'high', true))
  this.field.push(new Field('Type', new CodeableConcept(), 'type', true))
  this.field.push(
    new Field(
      'AppliesTo',
      new FhirArray(new CodeableConcept()),
      'appliesTo',
      true
    )
  )
  this.field.push(new Field('Age', new Range(), 'age', true))
  this.field.push(new Field('Text', new FhirString(), 'text', true))
}
