import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'
import FhirBoolean from './primitive/FhirBoolean'
import FhirCode from './primitive/FhirCode'
import FhirString from './primitive/FhirString'
import FhirUri from './primitive/FhirUri'

import Canonical from './Canonical'
import CodeableConcept from './CodeableConcept'
import Period from './Period'
import Quantity from './Quantity'
import Reference from './Reference'
import Timing from './Timing'

export default function Detail() {
  Resource.call(this)

  this.fields.push(new Field('Kind', new FhirCode(), 'kind', true))
  this.fields.push(
    new Field(
      'Instantiates Canonical',
      new FhirArray(new Canonical()),
      'intantiatesCanonical',
      true
    )
  )
  this.fields.push(
    new Field(
      'Instantiates Uri',
      new FhirArray(new FhirUri()),
      'instantiatesUri',
      true
    )
  )
  this.fields.push(new Field('Code', new CodeableConcept(), 'code', true))
  this.fields.push(
    new Field('reasonCode', new FhirArray(new CodeableConcept()))
  )
  this.fields.push(
    new Field(
      'Reason Reference',
      new FhirArray(new Reference()),
      'reasonReference',
      true
    )
  ) // TODO pass Reference
  this.fields.push(
    new Field('Goal', new FhirArray(new Reference()), 'goal', true)
  ) // TODO pass GoalReference
  this.fields.push(new Field('Status', new FhirCode(), 'status', true))
  this.fields.push(
    new Field('Status Reason', new CodeableConcept(), 'statusReason', true)
  )
  this.fields.push(
    new Field('Do Not Perform', new FhirBoolean(), 'doNotPerform', true)
  )
  this.fields.push(
    new Field('Scheduled Timing', new Timing(), 'scheduledTiming', true)
  )
  this.fields.push(
    new Field('Scheduled Period', new Period(), 'scheduledPeriod', true)
  )
  this.fields.push(
    new Field('Scheduled String', new FhirString(), 'scheduledString', true)
  )
  this.fields.push(new Field('Location', new Reference(), 'location', true)) // TODO pass Reference
  this.fields.push(
    new Field('Performer', new FhirArray(new Reference()), 'performer', true)
  ) // TODO pass Reference
  this.fields.push(
    new Field(
      'Product Codeable Concept',
      new CodeableConcept(),
      'productCodeableConcept',
      true
    )
  )
  this.fields.push(
    new Field('Product Reference', new Reference(), 'productReference', true)
  ) // TODO pass Reference
  this.fields.push(
    new Field('Daily Amount', new Quantity(), 'dailyAmount', true)
  )
  this.fields.push(
    new Field('Description', new FhirString(), 'description', true)
  )
}
