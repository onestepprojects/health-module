import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'

import Address from './Address'
import CodeableConcept from './CodeableConcept'
import ContactPoint from './ContactPoint'
import HumanName from './HumanName'

export default function Contact() {
  Resource.call(this)

  this.fields.push(
    new Field('Purpose', new CodeableConcept(), 'purpose', false)
  )
  this.fields.push(new Field('Name', new HumanName(), 'name', false))
  this.fields.push(
    new Field(
      'Telecom',
      new FhirArray(new ContactPoint()),
      'phone number',
      false
    )
  )
  this.fields.push(new Field('Address', new Address(), '', false))
}
