import Resource from '../Resource'
import Field from '../Field'

import FhirString from './primitive/FhirString'

import CodeableConcept from './CodeableConcept'

export default function Reference() {
  Resource.call(this)

  this.fields.push(
    new Field('Assessment', new CodeableConcept(), 'assessment', false)
  )
  this.fields.push(
    new Field(
      'Product Relatedness',
      new FhirString(),
      'productRelatedness',
      false
    )
  )
  this.fields.push(new Field('Author', new Reference(), 'authorAuthor', false))
  this.fields.push(new Field('Method', new CodeableConcept(), 'method', false))
}
