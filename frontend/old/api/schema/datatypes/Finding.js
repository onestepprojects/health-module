import Resource from '../Resource'
import Field from '../Field'

import FhirString from './primitive/FhirString'

import CodeableConcept from './CodeableConcept'
import Reference from './Reference'

export default function Finding() {
  Resource.call(this)

  this.field.push(
    new Field(
      'Item Codeable Concept',
      new CodeableConcept(),
      'itemCodeableConcept',
      true
    )
  )
  this.field.push(
    new Field('Item Reference', new Reference(), 'itemReference', true)
  ) // TODO pass Reference
  this.field.push(new Field('Basis'), new FhirString(), 'basis', true)
}
