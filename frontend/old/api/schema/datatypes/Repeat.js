import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'
import FhirCode from './primitive/FhirCode'
import FhirDecimal from './primitive/FhirDecimal'
import FhirDuration from './primitive/FhirDuration'
import FhirPositiveInt from './primitive/FhirPositiveInt'
import FhirTime from './primitive/FhirTime'
import FhirUnsignedInt from './primitive/FhirUnsignedInt'

import Period from './Period'
import Range from './Range'

export default function Repeat() {
  Resource.call(this)

  this.fields.push(
    new Field('Bounds Duration', new FhirDuration(), 'boundsDuration', true)
  )
  this.fields.push(new Field('Bounds Range', new Range(), 'boundsRange', true))
  this.fields.push(
    new Field('Bounds Period', new Period(), 'boundsPeriod', true)
  )
  this.fields.push(new Field('Count', new FhirPositiveInt(), 'count', true))
  this.fields.push(
    new Field('Count Max', new FhirPositiveInt(), 'countMax', true)
  )
  this.fields.push(new Field('Duration', new FhirDecimal(), 'duration', true))
  this.fields.push(
    new Field('Duration Max', new FhirDecimal(), 'durationMax', true)
  )
  this.fields.push(
    new Field('Duration Unit', new FhirCode(), 'durationUnit', true)
  )
  this.fields.push(
    new Field('Frequency', new FhirPositiveInt(), 'frequency', true)
  )
  this.fields.push(
    new Field('Frequency Max', new FhirPositiveInt(), 'frequencyMax', true)
  )
  this.fields.push(new Field('Period', new FhirDecimal(), 'period', true))
  this.fields.push(
    new Field('Period Max', new FhirDecimal(), 'periodMax', true)
  )
  this.fields.push(new Field('Period Unit', new FhirCode(), 'periodUnit', true))
  this.fields.push(
    new Field('Day of Week', new FhirArray(new FhirCode([])), 'dayOfWeek', true)
  )
  this.fields.push(
    new Field('Time Of Day', new FhirArray(new FhirTime()), 'timeOfDay', true)
  )
  this.fields.push(
    new Field('When', new FhirArray(new FhirCode()), 'when', true)
  )
  this.fields.push(new Field('Offset', new FhirUnsignedInt(), 'offset', true))
}
