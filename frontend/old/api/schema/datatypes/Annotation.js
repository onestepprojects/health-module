import Resource from '../Resource'
import Field from '../Field'

import Reference from './Reference'
import FhirTime from './primitive/FhirTime'
import FhirMarkdown from './primitive/FhirMarkdown'

export default function Annotation() {
  Resource.call(this)

  this.fields.push(new Field('Author', new Reference(), 'author', false))
  this.fields.push(new Field('Time', new FhirTime(), 'time', false))
  this.fields.push(new Field('Text', new FhirMarkdown(), 'text', false))
}
