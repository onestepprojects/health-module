import Resource from '../Resource'

export default function Instant() {
  Resource.call(this)

  /* TODO
   * An instant in time in the format YYYY-MM-DDThh:mm:ss.sss+zz:zz (e.g. 2015-02-07T13:28:17.239+02:00 or
   * 2017-01-01T00:00:00Z). The time SHALL specified at least to the second and SHALL include a time zone. Note: This is
   * intended for when precisely observed times are required (typically system logs etc.), and not human-reported times
   * - for those, use date or dateTime (which can be as precise as instant, but is not required to be). instant is a
   * more constrained dateTime	xs:dateTime	A JSON string - an xs:dateTime Note: This type is for system times, not human
   * times (see date and dateTime below). Regex:
   * ([0-9]([0-9]([0-9][1-9]|[1-9]0)|[1-9]00)|[1-9]000)-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])T([01][0-9]|2[0-3]):[0-5][0-9]:([0-5][0-9]|60)(\.[0-9]+)?(Z|(\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))
   * */
}
