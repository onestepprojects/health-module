import Resource from '../Resource'
import Field from '../Field'

import FhirBoolean from './primitive/FhirBoolean'

import CodeableConcept from './CodeableConcept'
import Quantity from './Quantity'
import Period from './Period'
import Range from './Range'
import Reference from './Reference'

export default function Characteristic() {
  Resource.call(this)

  this.fields.push(new Field('Code', new CodeableConcept(), 'code', true))
  this.fields.push(
    new Field(
      'Value Codeable Concept',
      new CodeableConcept(),
      'valueCodeableConcept',
      true
    )
  )
  this.fields.push(
    new Field('Value Boolean', new FhirBoolean(), 'valueBoolean', true)
  )
  this.fields.push(
    new Field('Value Quantity', new Quantity(), 'valueQuantity', true)
  )
  this.fields.push(new Field('Value Range', new Range(), 'valueRange', true))
  this.fields.push(
    new Field('Value Reference', new Reference(), 'valueReference', true)
  )
  this.fields.push(new Field('Exclude', new FhirBoolean(), 'exclude', true))
  this.fields.push(new Field('Period', new Period(), 'period', true))
}
