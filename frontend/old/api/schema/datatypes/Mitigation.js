import Resource from '../Resource'
import Field from '../Field'

import FhirDateTime from './primitive/FhirDateTime'

import CodeableConcept from './CodeableConcept'
import Reference from './Reference'

export default function Mitigation() {
  Resource.call(this)

  this.field.push(new Field('Action', new CodeableConcept(), 'action', true))
  this.field.push(new Field('Date', new FhirDateTime(), 'date', true))
  this.field.push(new Field('Author', new Reference(), 'author', true))
}
