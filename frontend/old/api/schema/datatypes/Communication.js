import Resource from '../Resource'
import Field from '../Field'

import FhirBoolean from './primitive/FhirBoolean'

import CodeableConcept from './CodeableConcept'

export default function Reaction() {
  Resource.call(this)

  this.fields.push(
    new Field('Language', new CodeableConcept(), 'language', true)
  )
  this.fields.push(new Field('Preferred', new FhirBoolean(), 'preferred', true))
}
