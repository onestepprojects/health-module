import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'
import FhirBoolean from './primitive/FhirBoolean'
import FhirCode from './primitive/FhirCode'
import FhirTime from './primitive/FhirTime'

export default function AvailableTime() {
  Resource.call(this)

  this.field.push(
    new Field('Days of Week', new FhirArray(new FhirCode()), 'daysOfWeek', true)
  )
  this.field.push(new Field('All Day', new FhirBoolean(), 'allDay', true))
  this.field.push(
    new Field(
      'Available Start Time',
      new FhirTime(),
      'availableStartTime',
      true
    )
  )
  this.field.push(
    new Field('Available End Time', new FhirTime(), 'availableEndTime', true)
  )
}
