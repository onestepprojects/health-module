import Resource from '../Resource'
import Field from '../Field'

import FhirString from './primitive/FhirString'

import Period from './Period'

export default function NotAvailable() {
  Resource.call(this)

  this.fields.push(
    new Field('description', new FhirString(), 'description', true)
  )
  this.fields.push(new Field('during', new Period(), 'during', true))
}
