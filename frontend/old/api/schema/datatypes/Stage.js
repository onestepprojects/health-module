import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'

import CodeableConcept from './CodeableConcept'
import Reference from './Reference'

export default function Stage() {
  Resource.call(this)

  this.field.push(new Field('Summary', new CodeableConcept(), 'summary', true))
  this.field.push(
    new Field('Assessment', new FhirArray(new Reference()), 'assessment', true)
  )
  this.field.push(new Field('Type', new CodeableConcept(), 'type', true))
}
