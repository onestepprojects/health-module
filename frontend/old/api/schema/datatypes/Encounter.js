import Resource from '../Resource'
import Field from '../Field'
import Identifier from './Identifier'
import FhirCode from './primitive/FhirCode'
import FhirDuration from './primitive/FhirDuration'
import CodeableConcept from './CodeableConcept'
import Period from './Period'

export default function Encounter() {
  Resource.call(this)

  this.field.push(
    new Field('Identifier', new Identifier(), 'identifier', false, 'identifier')
  )
  this.field.push(new Field('Status', new FhirCode(), '', false, 'status'))

  // TODO: Status history

  this.field.push(new Field('Class', new FhirCode(), '', false, 'class'))

  // TODO: Class History

  this.field.push(new Field('Type', new CodeableConcept(), '', false, 'type'))

  this.field.push(
    new Field('Service Type', new CodeableConcept(), '', false, 'serviceType')
  )

  this.field.push(
    new Field('Priority', new CodeableConcept(), '', false, 'priority')
  )

  // TODO: subject
  // TODO: episode of care
  // TODO: basedOn
  // TODO: participant
  // TODO: appointment

  this.field.push(new Field('Period', new Period(), '', false, 'period'))

  this.field.push(new Field('Length', new FhirDuration(), '', false, 'length'))

  this.field.push(
    new Field('Reason Code', new CodeableConcept(), '', false, 'reasonCode')
  )

  // TODO: reason reference
  // TODO: diagnosis
  // TODO: account
  // TODO: hospitalization
  // TODO: location
  // TODO: serviceProvider
  // TODO: partOf
}
