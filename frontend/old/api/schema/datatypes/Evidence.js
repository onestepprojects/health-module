import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'

import CodeableConcept from './CodeableConcept'
import Reference from './Reference'

export default function Evidence() {
  Resource.call(this)

  this.field.push(
    new Field('Code', new FhirArray(new CodeableConcept()), 'code', false)
  )
  this.field.push(
    new Field('Detail', new FhirArray(new Reference()), 'detail', false)
  ) // TODO pass Reference
}
