import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'

import CodeableConcept from './CodeableConcept'
import Detail from './Detail'
import Reference from './Reference'

export default function Signature() {
  Resource.call(this)

  this.fields.push(
    new Field(
      'Outcome Codeable Concept',
      new FhirArray(new CodeableConcept()),
      'outcomeCodeableConcept',
      true
    )
  )
  this.fields.push(
    new Field(
      'Outcome Reference',
      new FhirArray(new Reference()),
      'outcomReference',
      true
    )
  ) // TODO pass Reference
  this.fields.push(new Field('Progress', new Reference(), 'progress', true)) // TODO pass Reference
  this.fields.push(new Field('Reference', new Reference(), 'reference', true)) // TODO pass Reference
  this.fields.push(new Field('Detail', new Detail(), 'detail', true))
}
