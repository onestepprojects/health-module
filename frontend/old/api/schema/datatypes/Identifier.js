import Resource from '../Resource'

import Field from '../Field'

import FhirCode from './primitive/FhirCode'
import CodeableConcept from './CodeableConcept'
import FhirUri from './primitive/FhirUri'
import FhirString from './primitive/FhirString'
import Period from './Period'

export default function Identifier() {
  Resource.call(this)

  this.fields.push(
    new Field(
      'Use',
      new FhirCode([
        { text: 'Usual', value: 'usual' },
        { text: 'Official', value: 'official' },
        { text: 'Temp', value: 'temp' },
        { text: 'Secondary', value: 'secondary' },
        { text: 'Old', value: 'old' }
      ]),
      false
    )
  )
  this.fields.push(new Field('Type', new CodeableConcept(), 'type', false))
  this.fields.push(new Field('System', new FhirUri(), 'system', false))
  this.fields.push(new Field('Value', new FhirString(), 'value', false))
  this.fields.push(new Field('Period', new Period(), 'period', false))
}
