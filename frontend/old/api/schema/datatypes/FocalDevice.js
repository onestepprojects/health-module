import Resource from '../Resource'
import Field from '../Field'

import CodeableConcept from './CodeableConcept'
import Reference from './Reference'

export default function FocalDevice() {
  Resource.call(this)

  this.fields.push(new Field('Action', new CodeableConcept(), 'action', true))
  this.field.push(
    new Field('Manipulated', new Reference(), 'manipulated', true)
  ) // TODO pass Reference
}
