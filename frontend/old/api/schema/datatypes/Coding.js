import Resource from '../Resource'

import Field from '../Field'

import FhirCode from './primitive/FhirCode'
import FhirString from './primitive/FhirString'
import FhirUri from './primitive/FhirUri'
import FhirBoolean from './primitive/FhirBoolean'

export default function Attachment() {
  Resource.call(this)

  this.fields.push(new Field('System', new FhirUri(), 'system', true))
  this.fields.push(new Field('Version', new FhirString(), 'version', true))
  this.fields.push(new Field('Code', new FhirCode(), 'code', true))
  this.fields.push(new Field('Display', new FhirString(), 'display', true))
  this.fields.push(
    new Field('User Selected', new FhirBoolean(true), 'userSelected', true)
  )
}
