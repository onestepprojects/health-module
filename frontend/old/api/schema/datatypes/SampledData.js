import Resource from '../Resource'

import Field from '../Field'

import SimpleQuantity from './SimpleQuantity'
import FhirDecimal from './primitive/FhirDecimal'
import FhirPositiveInt from './primitive/FhirPositiveInt'
import FhirString from './primitive/FhirString'

export default function SampledData() {
  Resource.call(this)

  this.fields.push(new Field('Origin', new SimpleQuantity(), 'origin', true))
  this.fields.push(new Field('Period', new FhirDecimal(), 'period', true))
  this.fields.push(new Field('Factor', new FhirDecimal(), 'factor', true))
  this.fields.push(
    new Field('Lower Limit', new FhirDecimal(), 'lowerLimit', true)
  )
  this.fields.push(
    new Field('Upper Limit', new FhirDecimal(), 'upperLimit', true)
  )
  this.fields.push(
    new Field('Dimensions', new FhirPositiveInt(), 'dimensions', true)
  )
  this.fields.push(new Field('Data', new FhirString(), 'data', true))
}
