import Resource from '../Resource'

import Field from '../Field'

import FhirDateTime from './primitive/FhirDateTime'

export default function Period() {
  Resource.call(this)

  this.fields.push(new Field('Start', new FhirDateTime(), 'start', false))
  this.fields.push(new Field('End', new FhirDateTime(), 'end', false))
}
