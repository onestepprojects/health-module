import Resource from '../Resource'

import Field from '../Field'

import FhirString from './primitive/FhirString'
import FhirCode from './primitive/FhirCode'
import Period from './Period'

export default function Address() {
  Resource.call(this)

  this.fields.push(
    new Field(
      'Use',
      new FhirCode([
        { text: 'Home', value: 'home' },
        { text: 'Work', value: 'work' },
        { text: 'Temp', value: 'temp' },
        { text: 'Old', value: 'old' },
        { text: 'Billing', value: 'billing' }
      ]),
      '',
      true
    )
  )
  this.fields.push(
    new Field(
      'Type',
      new FhirCode([
        { text: 'Postal', value: 'postal' },
        { text: 'Physical', value: 'physical' },
        { text: 'Both', value: 'both' }
      ]),
      '',
      true
    )
  )
  this.fields.push(new Field('Text', new FhirString(), 'text', true))
  this.fields.push(new Field('Line', new FhirString(), 'line', true))
  this.fields.push(new Field('City', new FhirString(), 'city', true))
  this.fields.push(new Field('District', new FhirString(), 'district', true))
  this.fields.push(new Field('State', new FhirString(), 'state', true))
  this.fields.push(
    new Field('Postal Code', new FhirString(), 'postal code', true)
  )
  this.fields.push(new Field('Country', new FhirString(), 'country', true))
  this.fields.push(new Field('Period', new Period(), 'period', true))
}
