import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'
import FhirPositiveInt from './primitive/FhirPositiveInt'
import FhirString from './primitive/FhirString'

import CodeableConcept from './CodeableConcept'
import Reference from './Reference'

export default function ProtocolApplied() {
  Resource.call(this)

  this.field.push(new Field('Series', new FhirString(), 'series', false))
  this.field.push(new Field('Authority', new Reference(), 'authority', false)) // TODO pass reference
  this.field.push(
    new Field(
      'Target Disease',
      new FhirArray(new CodeableConcept()),
      'targetDisease',
      false
    )
  )
  this.field.push(
    new Field(
      'Dose Number Positive Int',
      new FhirPositiveInt(),
      'doseNumberPositiveInt',
      false
    )
  )
  this.field.push(
    new Field('Dose Number String', new FhirString(), 'doseNumberString', true)
  )
  this.field.push(
    new Field(
      'Series Doses Positive Int',
      new FhirPositiveInt(),
      'seriesDosesPositiveInt',
      true
    )
  )
  this.field.push(
    new Field(
      'Series Doses String',
      new FhirString(),
      'seriesDosesString',
      true
    )
  )
}
