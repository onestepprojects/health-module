import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'

import CodeableConcept from './CodeableConcept'
import Identifier from './Identifier'
import Period from './Period'
import Reference from './Reference'

export default function Qualification() {
  Resource.call(this)

  this.fields.push(
    new Field(
      'Identifier',
      new FhirArray(new Identifier()),
      'identifier',
      false
    )
  )
  this.fields.push(new Field('Code', new CodeableConcept(), 'code', false))
  this.fields.push(new Field('Period', new Period(), 'period', false))
  this.fields.push(new Field('Issuer', new Reference(), 'issuer', false)) // TODO pass a Resource
}
