import Resource from '../Resource'

import Field from '../Field'

import FhirCode from './primitive/FhirCode'
import Reference from './Reference'
import Coding from './Coding'
import FhirInstant from './primitive/FhirInstant'
import FhirBase64Binary from './primitive/FhirBase64Binary'

export default function Signature() {
  Resource.call(this)

  this.fields.push(new Field('Type', new Coding(), 'type', false))
  this.fields.push(new Field('Instant', new FhirInstant(), 'instant', false))
  this.fields.push(new Field('Who', new Reference(), 'who', false))
  this.fields.push(
    new Field('On Behalf Of', new Reference(), 'onBehalfOf', false)
  )
  this.fields.push(
    new Field('Target Format', new FhirCode(), 'targetFormat', false)
  )
  this.fields.push(new Field('Sig Format', new FhirCode(), 'sigFormat', false))
  this.fields.push(new Field('Data', new FhirBase64Binary(), 'data', false))
}
