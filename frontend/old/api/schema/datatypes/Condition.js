import Resource from '../Resource'
import Field from '../Field'

import FhirAge from './primitive/FhirAge'
import FhirArray from './primitive/FhirArray'
import FhirBoolean from './primitive/FhirBoolean'
import FhirString from './primitive/FhirString'

import Annotation from './Annotation'
import CodeableConcept from './CodeableConcept'
import Period from './Period'
import Range from './Range'

export default function Condition() {
  Resource.call(this)

  this.field.push(new Field('Code', new CodeableConcept(), 'code', false))
  this.field.push(new Field('Outcome', new CodeableConcept(), 'outcome', false))
  this.field.push(
    new Field(
      'Contributed To Death',
      new FhirBoolean(),
      'contributedToDeath',
      false
    )
  )
  this.field.push(new Field('Onset Age', new FhirAge(), 'onsetAge', false))
  this.field.push(new Field('Onset Range', new Range(), 'onsetRange', false))
  this.field.push(new Field('Onset Period', new Period(), 'onsetPeriod', false))
  this.field.push(
    new Field('Onset String', new FhirString(), 'onsetString', false)
  )
  this.field.push(
    new Field('Note', new FhirArray(new Annotation()), 'note', false)
  )
}
