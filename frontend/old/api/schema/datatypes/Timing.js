import Resource from '../Resource'
import Field from '../Field'

import FhirArray from './primitive/FhirArray'
import FhirDateTime from './primitive/FhirDateTime'

import CodeableConcept from './CodeableConcept'
import Repeat from './Repeat'

export default function Timing() {
  Resource.call(this)

  this.fields.push(
    new Field('event', new FhirArray(new FhirDateTime()), 'event', true)
  )
  this.fields.push(new Field('repeat', new Repeat(), 'repeat', true))
  this.fields.push(new Field('code', new CodeableConcept(), 'code', true))
}
