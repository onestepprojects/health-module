import Resource from '../Resource'

import Field from '../Field'

import SimpleQuantity from './SimpleQuantity'

export default function Range() {
  Resource.call(this)

  this.fields.push(new Field('Low', new SimpleQuantity(), 'low', true))
  this.fields.push(new Field('High', new SimpleQuantity(), 'high', true))
}
