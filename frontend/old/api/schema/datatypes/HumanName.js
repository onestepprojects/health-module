import Resource from '../Resource'

import Field from '../Field'

import FhirString from './primitive/FhirString'
import FhirCode from './primitive/FhirCode'
import Period from './Period'

export default function HumanName() {
  Resource.call(this)

  this.fields.push(
    new Field(
      'Use',
      new FhirCode([
        { text: 'Usual', value: 'usual' },
        { text: 'Official', value: 'official' },
        { text: 'Temp', value: 'temp' },
        { text: 'Nickname', value: 'nickname' },
        { text: 'Anonymous', value: 'anonymous' },
        { text: 'Old', value: 'old' },
        { text: 'Maiden', value: 'maiden' }
      ]),
      '',
      false,
      'use'
    )
  )
  this.fields.push(new Field('Text', new FhirString(), 'text', false, 'text'))
  this.fields.push(
    new Field('Family', new FhirString(), 'family', true, 'family')
  )
  this.fields.push(new Field('Given', new FhirString(), 'given', true, 'given'))
  this.fields.push(
    new Field('Prefix', new FhirString(), 'prefix', true, 'prefix')
  )
  this.fields.push(
    new Field('Suffix', new FhirString(), 'suffix', true, 'suffix')
  )
  this.fields.push(new Field('Period', new Period(), 'period', true, 'period'))
}
