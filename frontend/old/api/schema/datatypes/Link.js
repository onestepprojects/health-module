import Resource from '../Resource'
import Field from '../Field'

import FhirCode from './primitive/FhirCode'

import Reference from './Reference'

export default function Link() {
  Resource.call(this)

  this.field.push(new Field('Target', new Reference(), 'target', true)) // TODO pass reference
  this.field.push(new Field('Assurance', new FhirCode(), 'assurance', true))
}
