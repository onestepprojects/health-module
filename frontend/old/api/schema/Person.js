import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirBoolean from './datatypes/primitive/FhirBoolean'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirDate from './datatypes/primitive/FhirDate'

import Address from './datatypes/Address'
import Attachment from './datatypes/Attachment'
import ContactPoint from './datatypes/ContactPoint'
import HumanName from './datatypes/HumanName'
import Identifier from './datatypes/Identifier'
import Link from './datatypes/Link'
import Reference from './datatypes/Reference'

export default function Person() {
  this.name = 'Person'

  Resource.call(this)

  this.fields.push(
    new Field(
      'Identifier',
      new FhirArray(new Identifier()),
      'identifier',
      false
    )
  )
  this.fields.push(
    new Field('Name', new FhirArray(new HumanName()), 'name', false)
  )
  this.fields.push(
    new Field(
      'Telecom',
      new FhirArray(new ContactPoint()),
      'phone number',
      false
    )
  )
  this.fields.push(
    new Field(
      'Gender',
      new FhirCode([
        { text: 'Male', value: 'male' },
        { text: 'Female', value: 'female' },
        { text: 'Other', value: 'other' },
        { text: 'Unknown', value: 'unknown' }
      ]),
      '',
      false
    )
  )
  this.fields.push(new Field('Birth Date', new FhirDate(), '', false))
  this.fields.push(new Field('Address', new Address(), '', false))
  this.fields.push(new Field('Photo', new Attachment(), '', false))
  this.fields.push(
    new Field('Managing Organization', new Reference(), '', false)
  )
  this.field.push(new Field('Active', new FhirBoolean(), 'active', false))
  this.field.push(new Field('Link', new FhirArray(new Link()), 'link', false))
}
