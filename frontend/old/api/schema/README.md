# FHIR Resource Builder

[[_TOC_]]

The FHIR Resource Schema Builder builds a FHIR Schema Object, as well as a SurveyJS form definition. Eventually, it will also be used to generate SurveyJS Custom Widgets.

Important References:

- [FHIR Schema](http://www.hl7.org/fhir/resourcelist.html): Refer to this in order to determine fields and datatypes, as well as validation constraints
- [SurveyJS Creator](https://surveyjs.io/create-survey): Use the sample creator to figure out what the json should look like for the `formSchema` function.

## Health Form

The [HealthForm](HealthForm.js) object creates a form that includes various Resource definitions. The following is an example of specifying json for a SurveyJS form. You can test definitions using the TestEditor component.

```javascript
const TestEditor = () => {
  var data = new HealthForm(new Patient()).createForm()

  return <Survey.Survey json={data} />
}
```

This definition includes only a Patient form object.

## Resource

The top-level [Resource](Resource.js) object defines a resource, from which resources inherit. The Resource object implements functions for updating field values, getting field values, generating the FHIR schema for a resource, and generating the SurveyJS form schema.

Top-Level Resources implement the Resource object. A Resource defines fields and types for the Resource, as a "fields" array of type [Field](Field.js). Following is an example of implementing the Patient Resource.

```javascript
export default function Patient() {
  this.name = 'Patient'

  Resource.call(this)

  this.fields.push(
    new Field('Identifier', new FhirArray(new Identifier()), 'identifier', true)
  )
  this.fields.push(new Field('Active', new FhirBoolean(), '', true))
  this.fields.push(
    new Field('Name', new FhirArray(new HumanName()), 'name', true)
  )
  this.fields.push(
    new Field(
      'Telecom',
      new FhirArray(new ContactPoint()),
      'phone number',
      true
    )
  )
  this.fields.push(
    new Field(
      'Gender',
      new FhirCode([
        { text: 'Male', value: 'male' },
        { text: 'Female', value: 'female' },
        { text: 'Other', value: 'other' },
        { text: 'Unknown', value: 'unknown' }
      ]),
      '',
      true
    )
  )
  this.fields.push(new Field('Birth Date', new FhirDate(), '', true))
  // TODO: Deceased
  this.fields.push(new Field('Address', new Address(), '', true))
  this.fields.push(new Field('Marital Status', new CodeableConcept(), '', true))
  // TODO: Multiple Birth
  this.fields.push(new Field('Photo', new Attachment(), '', true))
  // TODO: Contact
  // TODO: Communication
  this.fields.push(new Field('General Practicioner', new Reference(), '', true))
  this.fields.push(
    new Field('Managing Organization', new Reference(), '', true)
  )
  // TODO: Link
}
```

While we should ultimately define all fields, it may be unnecessary to display all fields. Set the last parameter in a `Field` to `false` to prevent it from being displayed. Ignore validation for now, we will complete this in a second pass.

## Datatypes

Composite datatypes, which reside under `datatypes/` act much like other resources, although they are shared among Resources, and wil never be included in a Health Form.

### Primitives

Primitives reside in `datatypes/primitives`. The [Primitive](datatypes/Primitive.js) represents the top-level primitive, from which all other primitives inherit. These represent the smallest unit. Many of them will need to be updated to use the new form schema for SurveyJS. Primitives define a `formSchema` function that returns the json representation of a SurveyJS form element. Feel free to add validation here, as long as the validation is always true of that primitive.
