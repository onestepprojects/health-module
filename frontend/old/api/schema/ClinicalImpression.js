import Resource from './Resource'
import Field from './Field'

import FhirArray from './datatypes/primitive/FhirArray'
import FhirCode from './datatypes/primitive/FhirCode'
import FhirDate from './datatypes/primitive/FhirDate'
import FhirDateTime from './datatypes/primitive/FhirDateTime'
import FhirString from './datatypes/primitive/FhirString'
import FhirUri from './datatypes/primitive/FhirUri'

import Annotation from './datatypes/Annotation'
import CodeableConcept from './datatypes/CodeableConcept'
import Finding from './datatypes/Finding'
import Identifier from './datatypes/Identifier'
import Investigation from './datatypes/Investigation'
import Period from './datatypes/Period'
import Reference from './datatypes/Reference'

export default function ClinicalImpression() {
  this.name = 'ClinicalImpression'

  Resource.call(this)

  this.field.push(
    new Field('Identifier', new FhirArray(new Identifier()), 'identifier', true)
  )
  this.field.push(new Field('Status', new FhirCode(), 'status', true))
  this.field.push(
    new Field('Status Reason', new CodeableConcept(), 'statusReason', true)
  )
  this.field.push(
    new Field('Category', new CodeableConcept(), 'category', true)
  )
  this.field.push(new Field('Code', new CodeableConcept(), 'code', true))
  this.field.push(
    new Field('Description', new FhirString(), 'description', true)
  )
  this.field.push(new Field('Subject', new Reference(), 'subject', true)) // TODO pass Reference
  this.field.push(new Field('Encounter', new Reference(), 'encounter', true)) // TODO pass Reference
  this.field.push(
    new Field(
      'Effective Date Time',
      new FhirDateTime(),
      'effectiveDateTime',
      true
    )
  )
  this.field.push(
    new Field('Effective Period', new Period(), 'effectivePeriod', true)
  )
  this.field.push(new Field('Date', new FhirDate(), 'date', true))
  this.field.push(new Field('Assessor', new Reference(), 'assessor', true)) // TODO pass Reference
  this.field.push(new Field('Previous', new Reference(), 'previous', true)) // TODO pass Reference
  this.field.push(
    new Field('Problem', new FhirArray(new Reference()), 'problem', true)
  ) // TODO pass Reference
  this.field.push(
    new Field(
      'Investigation',
      new FhirArray(new Investigation()),
      'investigation',
      true
    )
  )
  this.field.push(
    new Field('Protocol', new FhirArray(new FhirUri()), 'protocol', true)
  )
  this.field.push(
    new Field('Finding', new FhirArray(new Finding()), 'finding', true)
  )
  this.field.push(
    new Field(
      'Prognosis Codeable Concept',
      new FhirArray(new CodeableConcept()),
      'prognosisCodeableConcept',
      true
    )
  )
  this.field.push(
    new Field(
      'Prognosis Reference',
      new FhirArray(new Reference()),
      'prognosisReference',
      true
    )
  ) // TODO pass Reference
  this.field.push(
    new Field(
      'Supporting Info',
      new FhirArray(new Reference()),
      'supportingInfo',
      true
    )
  ) // TODO pass Reference
  this.field.push(
    new Field('Note', new FhirArray(new Annotation()), 'note', true)
  )
}
