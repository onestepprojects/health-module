export async function getComposition(auth, client, signal, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    //TODO: Change to use authToken when backend uses project auth
    url: `Composition`,
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    //pageLimit: 1,
    signal: signal,
  })
  return result
}

export async function compositionsByPatient(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    //TODO: Change to use authToken when backend uses project auth
    url: `Composition?patient=Patient/${id}`,
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })
  return result
}

export async function getCompositionByID(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    //TODO: Change to use authToken when backend uses project auth
    url: `Composition/${id}`,
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })

  return result
}

export async function createComposition(auth, client, signal, requestHeaders, compositionData) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  const composition = {
    resourceType: 'Composition',
    meta: {
      lastUpdated: dateString,
    },
    status: 'final',
    title: `${compositionData.title}`,
    type: {
      text: 'Health Module Case',
    },
    subject: {
      reference: `Patient/${compositionData.patientId}`,
    },
    encounter: {
      reference: `Encounter/${compositionData.encounterId}`,
    },
    date: dateString,
    author: [
      {
        reference: `Practitioner/${compositionData.provider}`,
      },
    ],
    section: [
      {
        title: 'Reason for admission',
        code: {
          coding: [
            {
              system: 'http://loinc.org',
              code: '29299-5',
              display: 'Reason for visit Narrative',
            },
          ],
        },
        entry: [
          {
            reference: `Observation/${compositionData.observationId}`,
          },
        ],
      },
      {
        title: 'Document Reference',
        code: {
          coding: [
            {
              system: 'http://loinc.org',
              code: '11488–4',
              display: 'consultation note (generic)',
            },
          ],
        },
        entry: [
          {
            reference: `DocumentReference/${compositionData.documentReferenceId}`,
          },
        ],
      },
    ],
  }

  //TODO: Change to use authToken when backend uses project auth
  //const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: 'Composition',
    //headers: { ...requestHeaders, Authorization: token },
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.create(composition, requestOptions, { signal })

  return result
}

export async function updateComposition(
  auth,
  client,
  signal,
  requestHeaders,
  compositionId,
  compositionData
) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  //TODO: Change to use authToken when backend uses project auth
  //const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: `Composition/${compositionId}`,
    //headers: { ...requestHeaders, Authorization: token },
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.update(compositionData, requestOptions, { signal })

  return result
}
