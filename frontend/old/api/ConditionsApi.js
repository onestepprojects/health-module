export async function getConditions(auth, client, signal, resources, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'Condition',
    headers: { ...requestHeaders, Authorization: token },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })

  const conditions = []

  for (const v of result) {
    for (let i = 0; i < resources.length; i++) {
      if (resources[i].resourceId === v.id) {
        conditions.push(v.code.text)
      }
    }
  }

  return conditions
}

export async function conditionsByPatient(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    //url: 'Observation?patient=' + id,
    //headers: { ...requestHeaders, Authorization: token }
    //TODO: Change to use authToken when backend uses project auth
    url: 'Condition?patient=Patient/' + id,
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })

  result.sort((a, b) => {
    if (a.effectiveDateTime === undefined) {
      return 1
    }

    if (b.effectiveDateTime === undefined) {
      return -1
    }

    const aDate = new Date(a.effectiveDateTime)
    const bDate = new Date(b.effectiveDateTime)

    if (aDate < bDate) {
      return 1
    }

    if (aDate > bDate) {
      return -1
    }

    return 0
  })

  return result
}

export async function createCondition(
  auth,
  client,
  signal,
  requestHeaders,
  conditionData,
  patientId
) {
  const date = new Date()
  const dateString = JSON.stringify(date).slice(1, -6) + '+00:00'

  const condition = {
    resourceType: 'Condition',
    recordedDate: dateString,
    subject: {
      reference: `Patient/${patientId}`,
    },
    code: {
      text: conditionData.condition,
    },
    note: [
      {
        text: conditionData.note,
      },
    ],
  }

  const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: 'Condition',
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  return client.create(condition, requestOptions, { signal })
}
