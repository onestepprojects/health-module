export async function immunizationsByPatient(auth, client, signal, id, requestHeaders) {
  const token = await auth.GetUserAccessToken()

  const requestOptions = {
    url: 'Immunization?patient=Patient/' + id,
    //headers: { ...requestHeaders, Authorization: token }
    //TODO: Change to use authToken when backend uses project auth
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  const result = await client.request(requestOptions, {
    flat: true,
    signal: signal,
  })

  result.sort((a, b) => {
    if (a.occurrenceDateTime === undefined) {
      return 1
    }

    if (b.occurrenceDateTime === undefined) {
      return -1
    }

    const aDate = new Date(a.occurrenceDateTime)
    const bDate = new Date(b.occurrenceDateTime)

    if (aDate < bDate) {
      return 1
    }

    if (aDate > bDate) {
      return -1
    }

    return 0
  })

  return result
}

export async function createImmunization(
  auth,
  client,
  signal,
  requestHeaders,
  immunizationData,
  patientId
) {
  const immunization = {
    resourceType: 'Immunization',
    status: 'completed',
    vaccineCode: {
      text: `${immunizationData.text}`,
    },
    occurrenceDateTime: `${immunizationData.date}`,
    patient: {
      reference: `Patient/${patientId}`,
    },
  }

  const token = await auth.GetUserAccessToken()
  const requestOptions = {
    url: 'Immunization',
    //TODO: Change to use authToken when backend uses project auth
    //headers: { ...requestHeaders, Authorization: token }
    headers: {
      ...requestHeaders,
      Authorization: `Basic ${btoa(
        process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_USERNAME +
          ':' +
          process.env.REACT_APP_HEALTH_MODULE_FHIR_HEALTHLAKE_PASSWORD
      )}`,
    },
  }

  return client.create(immunization, requestOptions, { signal })
}
