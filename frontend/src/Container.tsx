import { type FC } from 'react'
import { Route, Routes } from 'react-router-dom'
import type { Person } from './types'
import {
  CarePlanPage,
  HomePage,
  MedicalHistoryPage,
  PatientFormPage,
  PatientPage,
  PersonalInformationPage,
  ProjectPage,
  SearchPatients,
  VisitsPage,
} from './views'
import { TabTableContent } from './views/Patient/TabTable/TabTableContent'

export interface ContainerProps {
  auth: {
    token?: string
    person?: Person
    roles?: string[]
  }
}

export const Container: FC<ContainerProps> = ({ auth }) => {
  return (
    <Routes>
      <Route path='/' element={<HomePage />} />
      <Route path='search-patients' element={<SearchPatients />} />
      <Route path='projects/:projectId' element={<ProjectPage />} />
      <Route path='patients/new' element={<PatientFormPage />} />
      <Route path='patients/:patientId' element={<PatientPage />}>
        <Route path='personal-information' element={<PersonalInformationPage />} />
        <Route path='visits' element={<VisitsPage />} />
        <Route path='care-plan' element={<CarePlanPage />} />
        <Route path='medical-history' element={<MedicalHistoryPage />} />
        <Route path='*' element={<TabTableContent />} />
      </Route>
      <Route path='/*' element={<HomePage />} />
    </Routes>
  )
}
