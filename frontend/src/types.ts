export type { HealthcareProjectEntity as HealthcareProject } from '../../backend/src/healthcare-projects'
export type {
  FindAllergyIntolerancesDto,
  FindCarePlansDto,
  FindEncountersDto,
  FindImmunizationsDto,
  FindMedicationRequestsDto,
  FindMedicationStatementsDto,
  FindObservationsDto,
  FindPatientsDto,
  IAllergyIntolerance,
  IBundle,
  ICarePlan,
  ICareTeam,
  IEncounter,
  IImmunization,
  IMedicationRequest,
  IMedicationStatement,
  IObservation,
  IPatient,
  R4,
} from '../../backend/src/fhir'

export interface Person {
  uuid: string
  name: string
  currentAddress: Address
  email: string
  phones: {
    phone: string
    mobile?: string
    sat_phone?: string
  }
  birthdate: string
  profilePicture: Picture
  lastActivity: string
  history: any[]
  homeAddress: Address
  ecUUID: string
  ecName: any
  ecPhones: {
    phone: string
  }
  ecAddress: Address
  paymentAccount: {
    payID: string
    blockchainAddress: string
    status: string
    assets: any
  }
  location: any
  gender: string
  socialMedia: SocialMedia
}

interface Address {
  address: string
  city: string
  country: string
  district: string
  postalCode: string
  state: string
}

interface Picture {
  name?: string // unique name as id
  size?: number
  format?: 'JPG' | 'PNG'
  body?: string
  caption?: string
  bucketName?: string
  creator?: string
  url?: string
}

interface SocialMedia {
  twitter: string
  facebook: string
  linkedin: string
  youtube: string
}

export interface Medication {
  id: string // Assume to medicationStatementId for deletion
  medicationRequestId: string
  medicationStatementId: string
  medic: string
  status: string // 'not-taken' | 'taken'
}
