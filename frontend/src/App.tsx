import { type FC } from 'react'
import { BrowserRouter } from 'react-router-dom'
import './App.css'
import { Container, type ContainerProps } from './Container'
import { api } from './services'

interface HealthModuleProps extends ContainerProps {
  /** Module router base name. */
  mountPath: string
}

const HealthModule: FC<HealthModuleProps> = ({ mountPath, auth }) => {
  if (!auth) return null

  /** Set the axios auth header immediately and keep it updated. */
  api.defaults.headers['Authorization'] = `Bearer ${auth.token}`

  return (
    <BrowserRouter basename={mountPath}>
      <Container auth={auth} />
    </BrowserRouter>
  )
}

export default HealthModule
