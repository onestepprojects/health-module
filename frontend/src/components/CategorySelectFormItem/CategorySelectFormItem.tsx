import { Form, Select, type FormItemProps } from 'antd'
import { type FC } from 'react'
import { type R4 } from '../../types'

interface CategorySelectFormItemProps extends FormItemProps {
  options: { label: string; value: string }[]
}

export const CategorySelectFormItem: FC<CategorySelectFormItemProps> = ({
  options,
  ...formItemProps
}) => {
  return (
    <Form.Item
      label='Category'
      name='category'
      {...formItemProps}
      getValueProps={([category]: R4.ICodeableConcept[] = []) => ({
        value: options.find((option) => option.value === category?.coding?.[0]?.code),
      })}
      getValueFromEvent={({ value, label }): R4.ICodeableConcept[] => [
        {
          coding: [
            {
              system: 'http://snomed.info/sct',
              code: value,
              display: label,
            },
          ],
          text: label,
        },
      ]}
    >
      <Select showSearch labelInValue optionFilterProp='label' options={options} />
    </Form.Item>
  )
}
