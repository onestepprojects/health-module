import axios, { type AxiosInstance } from 'axios'
import type {
  FindAllergyIntolerancesDto,
  FindCarePlansDto,
  FindEncountersDto,
  FindImmunizationsDto,
  FindMedicationStatementsDto,
  FindObservationsDto,
  FindPatientsDto,
  HealthcareProject,
  IAllergyIntolerance,
  IBundle,
  ICarePlan,
  IEncounter,
  IImmunization,
  IMedicationRequest,
  IMedicationStatement,
  IObservation,
  IPatient,
} from './types'

declare global {
  interface Window {
    onestep?: {
      health?: {
        api?: AxiosInstance
      }
    }
  }
}

window.onestep ||= {}
window.onestep.health ||= {}

export const api = (window.onestep.health.api = axios.create({
  baseURL: process.env.REACT_APP_HEALTH_MODULE_API_URL,
}))

export const getHealthcareProjects = async () => {
  const { data } = await api.get<HealthcareProject[]>(`projects`)
  return data
}

export const getHealthcareProject = async (id: string) => {
  const { data } = await api.get<HealthcareProject>(`projects/${id}`)
  return data
}

export const getPatients = async (params: FindPatientsDto) => {
  const { data } = await api.get<IBundle<IPatient>>(
    `fhir/patient?${new URLSearchParams(params as any)}`
  )
  return data
}

export const getPatient = async (id: string) => {
  const { data } = await api.get<IPatient>(`fhir/patient/${id}`)
  return data
}

export const createPatient = async (params: IPatient) => {
  const { data } = await api.post<IPatient>(`fhir/patient`, params)
  return data
}

export const updatePatient = async (params: IPatient) => {
  const { data } = await api.put<IPatient>(`fhir/patient/${params.id}`, params)
  return data
}

export const getEncounters = async (params: FindEncountersDto) => {
  const { data } = await api.get<IBundle<IEncounter>>(
    `fhir/encounter?${new URLSearchParams(params as any)}`
  )
  return data
}

export const createEncounter = async (params: IEncounter) => {
  const { data } = await api.post<IEncounter>(`fhir/encounter`, params)
  return data
}

export const getMedicationStatements = async (params: FindMedicationStatementsDto) => {
  const { data } = await api.get<IBundle<IMedicationStatement>>(
    `fhir/medicationstatement?${new URLSearchParams(params as any)}`
  )
  return data
}

export const getMedicationRequest = async (id: string) => {
  const { data } = await api.get<IMedicationRequest>(`fhir/medicationrequest/${id}`)
  return data
}

export const getAllergyIntolerances = async (params: FindAllergyIntolerancesDto) => {
  const { data } = await api.get<IBundle<IAllergyIntolerance>>(
    `fhir/allergyintolerance?${new URLSearchParams(params as any)}`
  )
  return data
}

export const getImmunizations = async (params: FindImmunizationsDto) => {
  const { data } = await api.get<IBundle<IImmunization>>(
    `fhir/immunization?${new URLSearchParams(params as any)}`
  )
  return data
}

export const getCarePlans = async (params: FindCarePlansDto) => {
  const { data } = await api.get<IBundle<ICarePlan>>(
    `fhir/careplan?${new URLSearchParams(params as any)}`
  )
  return data
}

export const getAllCarePlansByPatientId = async (patientId: string) => {
  const bundle = await getCarePlans({ subject: `Patient/${patientId}`, _count: 100 })
  return bundle.entry
    ?.filter(({ search }) => search.mode === 'match')
    .map(({ resource }) => resource)
}

export const createCarePlan = async (params: ICarePlan) => {
  const { data } = await api.post<ICarePlan>(`fhir/careplan`, params)
  return data
}

export const updateCarePlan = async (params: ICarePlan) => {
  const { data } = await api.put<ICarePlan>(`fhir/careplan/${params.id}`, params)
  return data
}

export const deleteCarePlan = async (id: string) => {
  const { data } = await api.delete<void>(`fhir/careplan/${id}`)
  return data
}

export const getObservations = async (params: FindObservationsDto) => {
  const { data } = await api.get<IBundle<IObservation>>(
    `fhir/observation?${new URLSearchParams(params as any)}`
  )
  return data
}

export const getAllObservationsByPatientId = async (patientId: string) => {
  const bundle = await getObservations({ subject: `Patient/${patientId}`, _count: 100 })
  return bundle.entry
    ?.filter(({ search }) => search.mode === 'match')
    .map(({ resource }) => resource)
}

export const createObservation = async (params: IObservation) => {
  const { data } = await api.post<IObservation>(`fhir/observation`, params)
  return data
}

export const deleteObservation = async (id: string) => {
  const { data } = await api.delete<void>(`fhir/observation/${id}`)
  return data
}
