import { PlusOutlined } from '@ant-design/icons'
import { useRequest } from 'ahooks'
import { Button, Input, List, Typography } from 'antd'
import { type FC } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { getHealthcareProjects } from '../../services'

export const HomePage: FC = () => {
  const navigate = useNavigate()

  const { loading, data: projects } = useRequest(getHealthcareProjects)

  const onSearch = (value: string) => {
    // TODO: some validation
    if (!value) return
    navigate(`/search-patients/?query=${value}`)
  }

  return (
    <div className='m-16'>
      <div className='flex gap-2'>
        <Input.Search
          className='w-full'
          size='large'
          allowClear
          placeholder='Patient Search'
          onSearch={onSearch}
        />
        <Link to='./patients/new'>
          <Button size='large' icon={<PlusOutlined />} />
        </Link>
      </div>

      <div className='my-8'>
        <List
          size='large'
          itemLayout='vertical'
          header={<Typography.Title level={4}>Healthcare Projects</Typography.Title>}
          loading={loading && !projects}
          dataSource={projects}
          renderItem={(project) => (
            <List.Item
              key={project.id}
              actions={[
                <div>
                  Patients (
                  {
                    project.fhirResources?.filter(({ resourceType }) => resourceType === 'Patient')
                      .length
                  }
                  )
                </div>,
                <div>
                  Questionnaires (
                  {
                    project.fhirResources?.filter(
                      ({ resourceType }) => resourceType === 'Questionnaire'
                    ).length
                  }
                  )
                </div>,
              ]}
            >
              <List.Item.Meta
                title={<Link to={`/projects/${project.id}`}>{project.name}</Link>}
                description={project.description}
              />
            </List.Item>
          )}
        />
      </div>
    </div>
  )
}
