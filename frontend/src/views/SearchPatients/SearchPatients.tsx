import {
  HomeOutlined,
  ManOutlined,
  PlusOutlined,
  ProfileOutlined,
  QuestionOutlined,
  WomanOutlined,
} from '@ant-design/icons'
import { useRequest } from 'ahooks'
import { Breadcrumb, Button, Col, Input, Row, Space, Table, Typography } from 'antd'
import { useEffect, type FC } from 'react'
import { Link, useSearchParams } from 'react-router-dom'
import { getPatients } from '../../services'

const { Search } = Input

const QUERY_PARAM = 'query'
const MAX_QUERY_RESULT = 100

export const SearchPatients: FC = () => {
  const [searchParams, setSearchParams] = useSearchParams()

  const {
    loading: searching,
    data: searchResult,
    runAsync: searchPatientsByName,
  } = useRequest(
    (name = '') =>
      getPatients({
        name: searchParams.get('query'),
        _count: MAX_QUERY_RESULT,
      }),
    {
      manual: true,
    }
  )

  const onSearch = (value: string) => {
    // TODO: some validation
    if (!value) return
    setSearchParams({ [QUERY_PARAM]: value })
  }

  useEffect(() => {
    if (searchParams.has(QUERY_PARAM)) {
      searchPatientsByName(searchParams.get(QUERY_PARAM))
    }
  }, [searchParams, searchPatientsByName])

  return (
    <div className='m-16'>
      <Breadcrumb className='my-4'>
        <Breadcrumb.Item>
          <Link to='..'>
            <HomeOutlined />
            <span className='ml-1'>Health</span>
          </Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Search Projects</Breadcrumb.Item>
      </Breadcrumb>
      <Typography.Title>Search Patients</Typography.Title>
      <Row>
        <Col span='23'>
          <Search
            placeholder='Type the patient name'
            allowClear
            onSearch={onSearch}
            size='large'
            style={{ width: '100%' }}
            defaultValue={searchParams.get(QUERY_PARAM)}
          />
        </Col>
        <Col span={1}>
          <div className='flex justify-end'>
            <Button size='large' icon={<PlusOutlined />} href='../patients/new' />
          </div>
        </Col>
      </Row>

      <Table
        loading={searching}
        dataSource={searchResult?.entry}
        columns={[
          {
            key: 'Name',
            title: 'Name',
            dataIndex: 'resource',
            render(resource) {
              const officialName = resource.name.find(({ use }) => use === 'official')
              return (
                <Link
                  to={`/patients/${resource.id}/personal-information`}
                >{`${officialName.given.join(' ')} ${officialName.family}`}</Link>
              )
            },
          },
          {
            key: 'gender',
            title: 'Gender',
            dataIndex: ['resource', 'gender'],
            render(value: string) {
              switch (value) {
                case 'male':
                  return <ManOutlined title='Male' style={{ color: 'blue' }} />
                case 'female':
                  return <WomanOutlined title='Female' style={{ color: 'purple' }} />
                default:
                  return <QuestionOutlined title='Unknown' style={{ color: 'gray' }} />
              }
            },
            filters: [
              {
                text: 'Female',
                value: 'female',
              },
              {
                text: 'Male',
                value: 'male',
              },
            ],
            onFilter: (value: string, record) => record.resource.gender === value,
          },
          {
            key: 'birthDate',
            title: 'Birth date',
            dataIndex: ['resource', 'birthDate'],
            render: (value: string) => new Date(value).toLocaleDateString(),
            sorter: (a, b) =>
              new Date(a.resource.birthDate).getTime() - new Date(b.resource.birthDate).getTime(),
          },
          {
            key: 'actions',
            title: 'Actions',
            render: (value, record) => (
              <Space size='middle'>
                <Link to={`/patients/${record.resource.id}/personal-information`}>
                  <ProfileOutlined />
                </Link>
              </Space>
            ),
          },
        ]}
      />
      {searchResult?.entry?.length >= MAX_QUERY_RESULT ? (
        <Typography.Text type='secondary'>
          The search results only show the first 100 items, if there are no desired results, more
          precise search criteria is required.
        </Typography.Text>
      ) : null}
    </div>
  )
}
