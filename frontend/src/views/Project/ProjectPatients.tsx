import {
  ManOutlined,
  ProfileOutlined,
  QuestionOutlined,
  SearchOutlined,
  WomanOutlined,
} from '@ant-design/icons'
import { Button, Card, Col, Input, Space, Table, type InputRef } from 'antd'
import type { ColumnType } from 'antd/es/table'
import type { FilterConfirmProps } from 'antd/es/table/interface'
import dayjs from 'dayjs'
import { useEffect, useRef, useState, type FC } from 'react'
import Highlighter from 'react-highlight-words'
import { Link } from 'react-router-dom'
import { getPatient } from '../../services'
import { type IPatient } from '../../types'

/** Compute age from birthday */
function computeAge(birthday: string) {
  const birthObj = dayjs(birthday)
  return dayjs().diff(birthObj, 'years')
}

interface ProjectProjects {
  patientIds: string[]
}

interface DataType {
  key: string
  name: string
  age: number
  gender: string
}

type DataIndex = keyof DataType

export const ProjectPatients: FC<ProjectProjects> = ({ patientIds }) => {
  const [patients, setPatients] = useState<IPatient[]>([])
  const [searchText, setSearchText] = useState('')
  const [searchedColumn, setSearchedColumn] = useState('')
  const searchInput = useRef<InputRef>(null)

  const handleSearch = (
    selectedKeys: string[],
    confirm: (param?: FilterConfirmProps) => void,
    dataIndex: DataIndex
  ) => {
    confirm()
    setSearchText(selectedKeys[0])
    setSearchedColumn(dataIndex)
  }

  const handleReset = (clearFilters: () => void) => {
    clearFilters()
    setSearchText('')
  }

  const getColumnSearchProps = (dataIndex: DataIndex): ColumnType<DataType> => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type='primary'
            onClick={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
            icon={<SearchOutlined />}
            size='small'
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size='small'
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            type='link'
            size='small'
            onClick={() => {
              confirm({ closeDropdown: false })
              setSearchText((selectedKeys as string[])[0])
              setSearchedColumn(dataIndex)
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes((value as string).toLowerCase()),
    onFilterDropdownVisibleChange: (visible: boolean) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100)
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  })

  useEffect(() => {
    ;(async () => {
      const patients = await Promise.all(
        patientIds.map(async (patientId) => {
          try {
            return await getPatient(patientId)
          } catch (err) {
            console.error('getPatient error', err)
            return null
          }
        })
      )
      setPatients(patients.filter((n) => n))
    })()
  }, [patientIds])

  if (patients.length === 0) return null

  return (
    <Col span={12}>
      <Card title='Patients' hoverable>
        <Table
          dataSource={patients.map((patient) => ({
            key: patient.id,
            name: `${patient.name[0].given.join(' ')} ${patient.name[0].family}`,
            age: computeAge(patient.birthDate),
            gender: patient.gender,
          }))}
          columns={[
            {
              key: 'name',
              title: 'Name',
              dataIndex: 'name',
              ...getColumnSearchProps('name'),
            },
            {
              key: 'gender',
              title: 'Gender',
              dataIndex: 'gender',
              filters: [
                {
                  text: 'Female',
                  value: 'female',
                },
                {
                  text: 'Male',
                  value: 'male',
                },
              ],
              onFilter: (value: string, record) => record.gender === value,
              render(value: string) {
                switch (value) {
                  case 'male':
                    return <ManOutlined title='Male' style={{ color: 'blue' }} />
                  case 'female':
                    return <WomanOutlined title='Female' style={{ color: 'purple' }} />
                  default:
                    return <QuestionOutlined title='Unknown' style={{ color: 'gray' }} />
                }
              },
            },
            {
              key: 'age',
              title: 'Age',
              dataIndex: 'age',
              sorter: (a, b) => a.age - b.age,
              ...getColumnSearchProps('age'),
            },
            {
              key: 'actions',
              title: 'Actions',
              render: (value, record) => (
                <Space size='middle'>
                  <Link to={`/patients/${record.key}/personal-information`}>
                    <ProfileOutlined />
                  </Link>
                </Space>
              ),
            },
          ]}
        />
      </Card>
    </Col>
  )
}
