import { HomeOutlined } from '@ant-design/icons'
import { useRequest } from 'ahooks'
import { Breadcrumb, Card, Col, Descriptions, Row, Spin } from 'antd'
import { type FC } from 'react'
import { Link, useParams } from 'react-router-dom'
import { getHealthcareProject } from '../../services'
import { ProjectPatients } from './ProjectPatients'

export const ProjectPage: FC = () => {
  const { projectId } = useParams()

  const { data: project } = useRequest(() => getHealthcareProject(projectId), {
    ready: !!projectId,
  })

  if (!project) return <Spin />

  const patientIds = project.fhirResources
    .filter((n) => n.resourceType === 'Patient')
    .map((n) => n.resourceId)

  return (
    <div className='mx-6'>
      <Breadcrumb className='my-4'>
        <Breadcrumb.Item>
          <Link to='..'>
            <HomeOutlined />
            <span className='ml-1'>Health</span>
          </Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to='../projects'>Projects</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>{project.name}</Breadcrumb.Item>
      </Breadcrumb>
      <div>
        <Row gutter={4}>
          <Col span={12}>
            <Card title={project.name} hoverable>
              <Descriptions layout='horizontal' column={{ xs: 1, sm: 1, md: 1 }}>
                <Descriptions.Item label='Description'>{project.description}</Descriptions.Item>
                <Descriptions.Item label='Manager'>{project.manager}</Descriptions.Item>
              </Descriptions>
            </Card>
          </Col>
          {<ProjectPatients patientIds={patientIds} />}
        </Row>
      </div>
    </div>
  )
}
