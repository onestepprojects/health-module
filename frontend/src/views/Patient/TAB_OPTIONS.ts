// TODO: TAB_OPTIONS should generate from the key of ./TabTable/tableSpecifications.ts
export const TAB_OPTIONS = [
  { label: 'Personal Information', value: 'personal-information' },
  { label: 'Visits', value: 'visits' },
  { label: 'Medications', value: 'medications' },
  { label: 'Allergies', value: 'allergies' },
  { label: 'Immunizations', value: 'immunizations' },
  { label: 'Care Plan', value: 'care-plan' },
  { label: 'Medical History', value: 'medical-history' },
]
