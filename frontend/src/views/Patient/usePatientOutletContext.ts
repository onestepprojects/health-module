import { useOutletContext } from 'react-router-dom'
import { type IPatient } from '../../types'

interface OutletContextType {
  patient: IPatient
  refreshPatient: () => Promise<IPatient>
}

export function usePatientOutletContext() {
  return useOutletContext<OutletContextType>()
}
