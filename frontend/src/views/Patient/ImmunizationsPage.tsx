import { type FC } from 'react'
import { usePatientOutletContext } from './usePatientOutletContext'

export const ImmunizationsPage: FC = () => {
  const { patient } = usePatientOutletContext()

  return <div>Immunizations Page</div>
}
