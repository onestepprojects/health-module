import { HomeOutlined } from '@ant-design/icons'
import { useRequest } from 'ahooks'
import {
  Breadcrumb,
  Button,
  DatePicker,
  Divider,
  Form,
  Input,
  Select,
  Typography,
  message,
} from 'antd'
import dayjs from 'dayjs'
import { type FC } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { createPatient } from '../../services'
import { type IPatient } from '../../types'

const genderOptions = [
  { value: 'male', label: 'Male' },
  { value: 'female', label: 'Female' },
  { value: 'other', label: 'Other' },
  { value: 'unknown', label: 'Unknown' },
]
const countryOptions = [{ value: 'us', label: 'US' }]

const initialValues = {
  givenName: '',
  familyName: '',
  birthDate: dayjs(),
  gender: 'male',
  phone: '',
  country: 'US',
  state: '',
  city: '',
  postalCode: '',
  line: '',
}

type FormData = typeof initialValues

export const PatientFormPage: FC = () => {
  const navigate = useNavigate()

  const { loading: submitting, run: submit } = useRequest(
    async (values: FormData) => {
      try {
        const newPatient = {
          name: [
            {
              family: values.familyName,
              given: [values.givenName],
            },
          ],
          address: [
            {
              country: values.country,
              state: values.state,
              city: values.city,
              postalCode: values.postalCode,
              line: [values.line],
            },
          ],
          telecom: [
            {
              system: 'phone',
              use: 'home',
              value: values.phone,
            },
          ],
          birthDate: values.birthDate?.format('YYYY-MM-DD'),
          gender: values.gender,
          resourceType: 'Patient',
        }
        const newPatientCreated = await createPatient(newPatient as IPatient)
        navigate(`/patients/${newPatientCreated.id}/personal-information`)
      } catch (err) {
        console.error(err)
        message.error('Failed to create patient', err?.message)
      }
    },
    { manual: true }
  )

  return (
    <div className='m-16'>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to='..'>
            <HomeOutlined />
            <span className='ml-1'>Health</span>
          </Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Create New Patient</Breadcrumb.Item>
      </Breadcrumb>
      <Typography.Title className='mt-2' level={2}>
        Create New Patient
      </Typography.Title>
      <Form
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 16 }}
        initialValues={initialValues}
        disabled={submitting}
        onFinish={submit}
      >
        <Form.Item label='Given Name' name='givenName' rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item label='Family Name' name='familyName' rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item label='Birth Date' name='birthDate' rules={[{ required: true }]}>
          <DatePicker />
        </Form.Item>
        <Form.Item label='Birth Sex' name='gender' rules={[{ required: true }]}>
          <Select options={genderOptions} />
        </Form.Item>
        <Form.Item label='Phone Number' name='phone' rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Divider>Address</Divider>
        <Form.Item label='Country' name='country' rules={[{ required: true }]}>
          <Select options={countryOptions} />
        </Form.Item>
        <Form.Item label='State' name='state' rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item label='City' name='city' rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item label='Postal Code' name='postalCode' rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item label='Street' name='line' rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 4 }}>
          <Button type='primary' htmlType='submit' loading={submitting}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}
