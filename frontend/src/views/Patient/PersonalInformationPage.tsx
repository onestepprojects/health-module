import { useMemoizedFn } from 'ahooks'
import { Button, DatePicker, Descriptions, Form, Input, Modal, Select, Space } from 'antd'
import dayjs from 'dayjs'
import { produce } from 'immer'
import { useMemo, useState, type FC } from 'react'
import { updatePatient } from '../../services'
import { usePatientOutletContext } from './usePatientOutletContext'

export const PersonalInformationPage: FC = () => {
  const { patient, refreshPatient } = usePatientOutletContext()

  const [editing, setEditing] = useState(false)

  const initialValues = useMemo(
    () => ({
      givenName: patient.name[0].given[0],
      familyName: patient.name[0].family,
      birthdate: dayjs(patient.birthDate),
      gender: patient.gender,
      phone: patient.telecom[0].value,
    }),
    [patient]
  )
  const [form] = Form.useForm<typeof initialValues>()
  const [submiting, setSubmiting] = useState(false)

  const handleSave = useMemoizedFn(async () => {
    try {
      setSubmiting(true)
      const values = await form.validateFields()
      const newPatient = produce(patient, (draft) => {
        draft.name[0].given[0] = values.givenName
        draft.name[0].family = values.familyName
        draft.birthDate = values.birthdate.format('YYYY-MM-DD')
        draft.gender = values.gender
        draft.telecom[0].value = values.phone
      })
      if (newPatient !== patient) {
        await updatePatient(newPatient)
        refreshPatient()
      }
      setEditing(false)
    } finally {
      setSubmiting(false)
    }
  })

  return (
    <div>
      <Space direction='vertical'>
        <Descriptions title='Patient'>
          <Descriptions.Item label='Full Name'>
            {patient.name?.[0].given[0]} {patient.name?.[0].family}
          </Descriptions.Item>
          <Descriptions.Item label='Birthdate'>{patient.birthDate}</Descriptions.Item>
          <Descriptions.Item label='Birth Sex'>{patient.gender}</Descriptions.Item>
          <Descriptions.Item label='Preferred Language'>
            {patient.communication?.[0].language.text}
          </Descriptions.Item>
          <Descriptions.Item label='Phone Number'>{patient.telecom?.[0].value}</Descriptions.Item>
          <Descriptions.Item label='Address'>
            {patient.address?.[0].line}, {patient.address?.[0].city}, {patient.address?.[0].state},{' '}
            {patient.address?.[0].country}
          </Descriptions.Item>
        </Descriptions>

        <div className='flex justify-end'>
          <Button type='primary' onClick={() => setEditing(true)}>
            Edit
          </Button>
        </div>
      </Space>

      <Modal
        title='Edit Patient'
        visible={editing}
        okText='Save'
        confirmLoading={submiting}
        onOk={handleSave}
        onCancel={() => setEditing(false)}
      >
        <Form
          labelCol={{ span: 6 }}
          form={form}
          initialValues={initialValues}
          onFinish={console.log}
        >
          <Form.Item label='Given Name' name='givenName'>
            <Input />
          </Form.Item>
          <Form.Item label='Family Name' name='familyName'>
            <Input />
          </Form.Item>
          <Form.Item label='Birthdate' name='birthdate'>
            <DatePicker />
          </Form.Item>
          <Form.Item label='Birth Sex' name='gender'>
            <Select>
              {['male', 'female', 'other', 'unknown'].map((value) => (
                <Select.Option key={value} value={value}>
                  {value}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label='Phone Number' name='phone'>
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}
