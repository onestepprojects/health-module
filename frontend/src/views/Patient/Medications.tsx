import { type FC } from 'react'
import { usePatientOutletContext } from './usePatientOutletContext'

export const MedicationsPage: FC = () => {
  const { patient } = usePatientOutletContext()

  return <div>Medications Page</div>
}
