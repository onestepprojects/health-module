import { PlusOutlined } from '@ant-design/icons'
import { useMemoizedFn, useRequest } from 'ahooks'
import {
  Button,
  DatePicker,
  Form,
  Input,
  Modal,
  Select,
  Table,
  Typography,
  type FormInstance,
} from 'antd'
import dayjs from 'dayjs'
import { useRef, type FC } from 'react'
import { createEncounter, getEncounters } from '../../services'
import { type IEncounter } from '../../types'
import { usePatientOutletContext } from './usePatientOutletContext'

export const VisitsPage: FC = () => {
  const { patient } = usePatientOutletContext()

  const { loading: isEncountersLoading, data: encounters } = useRequest(
    () =>
      getEncounters({ subject: `Patient/${patient.id}`, _count: 100 }).then((bundle) =>
        bundle.entry?.map(({ resource }) => resource)
      ),
    {}
  )

  const addVisitFormRef = useRef<
    FormInstance<{
      datetime: dayjs.Dayjs
      provider: string
      location: string
      status: IEncounter['status']
      reason: string
    }>
  >()
  const handleAddVisitClick = useMemoizedFn(() => {
    Modal.confirm({
      title: <Typography.Title level={4}>New Visit</Typography.Title>,
      icon: false,
      closable: true,
      maskClosable: false,
      content: (
        <Form
          ref={addVisitFormRef}
          labelCol={{ span: 8 }}
          initialValues={{ datetime: dayjs(), status: 'finished' }}
        >
          <Form.Item label='Date Of Service' name='datetime'>
            {/* FIXME: Should add a `format={(datetime) => datetime.toLocaleString()}` to the <DatePicker> but the appearance is different than `new Date().tolocaleString()` */}
            <DatePicker showTime allowClear={false} />
          </Form.Item>
          <Form.Item label='Provider' name='provider' required>
            <Input />
          </Form.Item>
          <Form.Item label='Location' name='location' required>
            <Input />
          </Form.Item>
          <Form.Item label='Status' name='status' required>
            <Select>
              {[
                'planned',
                'arrived',
                'triaged',
                'in-progress',
                'onleave',
                'finished',
                'cancelled',
                'entered-in-error',
                'unknown',
              ].map((value) => (
                <Select.Option key={value} value={value}>
                  {value}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          {/* FIXME: To investigate why the reason can't be saved in to FHIR */}
          <Form.Item label='Reason for Visit' name='reason'>
            <Input.TextArea rows={5} />
          </Form.Item>
        </Form>
      ),
      okText: 'Save',
      onOk: async () => {
        const values = await addVisitFormRef.current.validateFields()
        const datetime = values.datetime.toJSON()
        const encounter: IEncounter = {
          resourceType: 'Encounter',
          class: {
            system: 'http://terminology.hl7.org/CodeSystem/v3-ActCode',
            code: 'AMB',
          },
          participant: [
            {
              individual: {
                reference: `Practitioner/${values.provider}`,
              },
            },
          ],
          period: {
            start: datetime,
            end: datetime,
          },
          reasonCode: [
            {
              coding: [
                {
                  system: 'http://snomed.info/sct',
                  code: '36971009',
                  display: 'reason',
                },
              ],
            },
          ],
          serviceProvider: {
            reference: `Organization/${values.location}`,
          },
          status: values.status,
          subject: {
            reference: `Patient/${patient.id}`,
          },
          type: [
            {
              coding: [
                {
                  system: 'http://snomed.info/sct',
                  code: '185345009',
                  display: 'reason',
                },
              ],
              text: values.reason,
            },
          ],
        }
        await createEncounter(encounter)
      },
    })
  })

  return (
    <Table
      loading={isEncountersLoading}
      rowKey='id'
      title={() => (
        <div className='flex justify-end'>
          <Button type='primary' icon={<PlusOutlined />} onClick={handleAddVisitClick}>
            Add Visit
          </Button>
        </div>
      )}
      columns={[
        { title: 'Type', dataIndex: ['type', 0, 'text'] },
        { title: 'Location', dataIndex: ['location', 0, 'location', 'display'] },
        {
          title: 'Date',
          dataIndex: ['period', 'start'],
          render: (value) => new Date(value).toLocaleString(),
          sorter: (a, b) => (new Date(a.period.start) > new Date(b.period.start) ? -1 : 1),
          defaultSortOrder: 'ascend',
        },
        { title: 'Status', dataIndex: ['status'] },
      ]}
      dataSource={encounters}
    />
  )
}
