import { ArrowLeftOutlined } from '@ant-design/icons'
import { useRequest } from 'ahooks'
import { Button, Segmented, Spin } from 'antd'
import dayjs from 'dayjs'
import { type FC } from 'react'
import { Outlet, useMatch, useNavigate } from 'react-router-dom'
import { getPatient } from '../../services'
import { TAB_OPTIONS } from './TAB_OPTIONS'

export const PatientPage: FC = () => {
  const navigate = useNavigate()
  const {
    params: { patientId, tabValue },
  } = useMatch('patients/:patientId/:tabValue')

  const {
    loading: isPatientLoading,
    data: patient,
    refreshAsync: refreshPatient,
  } = useRequest(() => getPatient(patientId))

  const patientFullName = patient ? `${patient.name[0].given[0]} ${patient.name[0].family}` : '...'
  const patientDescription = patient
    ? `${patient.gender}, Age ${dayjs().diff(patient.birthDate, 'year')}, ${
        patient.address[0].city
      }, ${patient.address[0].state}`
    : ''

  return (
    <div>
      <div className='m-6 flex items-center gap-4'>
        <Button icon={<ArrowLeftOutlined />} onClick={() => history.back()} />
        {isPatientLoading ? (
          <Spin />
        ) : (
          <div className='flex items-center gap-4'>
            <div className='text-2xl'>{patientFullName}</div>
            <div className='text-sm'>{patientDescription}</div>
          </div>
        )}
      </div>
      <Segmented
        className='m-6'
        block
        size='large'
        options={TAB_OPTIONS}
        defaultValue={tabValue}
        onChange={navigate}
      />
      <div className='mx-6'>{!!patient && <Outlet context={{ patient, refreshPatient }} />}</div>
    </div>
  )
}
