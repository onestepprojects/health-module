import dayjs from 'dayjs'
import {
  api,
  getAllergyIntolerances,
  getImmunizations,
  getMedicationRequest,
  getMedicationStatements,
} from '../../../services'
import {
  type IAllergyIntolerance,
  type IImmunization,
  type IMedicationRequest,
  type IMedicationStatement,
  type Medication,
} from '../../../types'

export const PROTOCOL_PREFIX = 'https://'

/** Get now JSON string */
function now(): string {
  return new Date().toJSON()
}

export async function getAllAllergiesByPatientId(
  patientId: string
): Promise<IAllergyIntolerance[]> {
  const bundle = await getAllergyIntolerances({ subject: `Patient/${patientId}`, _count: 100 })
  return bundle.entry
    ?.filter(({ search }) => search.mode === 'match')
    .map(({ resource }) => resource)
}

export async function createAllergy(
  values: { category: string; code: string; text: string; system?: string },
  patientId: string
): Promise<IAllergyIntolerance> {
  // FIXME: Should define as the IAllergyIntolerance type but some enum can't export as a const.
  const body = {
    resourceType: 'AllergyIntolerance',
    category: [values.category],
    clinicalStatus: {
      coding: [
        {
          system: 'http://terminology.hl7.org/CodeSystem/allergyintolerance-clinical',
          code: 'active',
        },
      ],
    },
    verificationStatus: {
      coding: [
        {
          system: 'http://terminology.hl7.org/CodeSystem/allergyintolerance-verification',
          code: 'confirmed',
        },
      ],
    },
    type: 'allergy',
    criticality: 'low',
    code: {
      text: `${values.text}`,
      coding: [
        {
          code: values.code,
          display: values.text,
          system: values.system ? `${PROTOCOL_PREFIX}${values.system}` : undefined,
        },
      ],
    },
    patient: {
      reference: `Patient/${patientId}`,
    },
    recordedDate: now(),
  }
  const { data } = await api.post<IAllergyIntolerance>(`fhir/allergyintolerance`, body)
  return data
}

export async function updateAllergy(
  allergyId: string,
  record: IAllergyIntolerance,
  values: { category: string; code: string; text: string; system?: string }
): Promise<IAllergyIntolerance> {
  const newRecord = {
    ...record,
    category: [values.category],
    code: {
      text: `${values.text}`,
      coding: [
        {
          code: values.code,
          display: values.text,
          system: values.system ? `${PROTOCOL_PREFIX}${values.system}` : undefined,
        },
      ],
    },
  }
  const { data } = await api.put<IAllergyIntolerance>(
    `fhir/allergyintolerance/${allergyId}`,
    newRecord
  )
  return data
}

export async function deleteAllergy(allergyId: string) {
  const { data } = await api.delete<void>(`fhir/allergyintolerance/${allergyId}`)
  return data
}

async function getMedicationRequestById(medicationRequestId: string) {
  const bundle = await getMedicationRequest(medicationRequestId)
  return bundle
}

async function getMedicationStatementsByPatientId(patientId: string) {
  const bundle = await getMedicationStatements({ subject: `Patient/${patientId}`, _count: 100 })
  return bundle.entry
    ?.filter(({ search }) => search.mode === 'match')
    .map(({ resource }) => resource)
}

export async function getMedicationsByPatientId(patientId: string): Promise<Medication[]> {
  const medicationStatements = await getMedicationStatementsByPatientId(patientId)
  if (!Array.isArray(medicationStatements) || medicationStatements.length === 0) return []

  const availableMedicationStatements = medicationStatements.filter((ms) =>
    ms.basedOn?.[0].reference.startsWith('MedicationRequest/')
  )

  const medicationRequestsIds = availableMedicationStatements.map((medicationStatement) => {
    const { reference } = medicationStatement.basedOn[0]
    return reference.slice('MedicationRequest/'.length, reference.length)
  })

  // FIXME: Use batch api to get all medication requests
  const medicationRequests = await Promise.all(medicationRequestsIds.map(getMedicationRequestById))

  return medicationRequestsIds.map((_, i) => ({
    id: availableMedicationStatements[i].id,
    medicationRequestId: medicationRequests[i].id,
    medicationStatementId: availableMedicationStatements[i].id,
    medic: medicationRequests[i].medicationCodeableConcept.text,
    status: availableMedicationStatements[i].status,
  }))
}

async function createMedicationRequest(
  values: { medicName: string; strength: string },
  patientId: string
): Promise<IMedicationRequest> {
  const text = `${values.medicName} ${values.strength}`
  const body: IMedicationRequest = {
    resourceType: 'MedicationRequest',
    status: 'active',
    intent: 'order',
    medicationCodeableConcept: {
      coding: [
        {
          system: 'http://www.nlm.nih.gov/research/umls/rxnorm',
          code: '309362',
          display: text,
        },
      ],
      text,
    },
    subject: {
      reference: `Patient/${patientId}`,
    },
    authoredOn: now(),
  }
  const { data } = await api.post<IMedicationRequest>(`fhir/medicationrequest`, body)
  return data
}

async function createMedicationStatement(
  medicationRequestId: string,
  patientId: string
): Promise<IMedicationStatement> {
  const body: IMedicationStatement = {
    resourceType: 'MedicationStatement',
    basedOn: [
      {
        reference: `MedicationRequest/${medicationRequestId}`,
      },
    ],
    medicationReference: {
      reference: `MedicationRequest/${medicationRequestId}`,
    },
    status: 'not-taken',
    subject: {
      reference: `Patient/${patientId}`,
    },
    effectiveDateTime: dayjs().format('YYYY-MM-DD'),
    dateAsserted: now(),
  }
  const { data } = await api.post<IMedicationStatement>(`fhir/medicationstatement`, body)
  return data
}

export async function createMedication(
  values: { medicName: string; strength: string },
  patientId: string
): Promise<Medication> {
  const medicationRequest = await createMedicationRequest(values, patientId)
  const medicationStatement = await createMedicationStatement(medicationRequest.id, patientId)

  return {
    id: medicationStatement.id,
    medicationRequestId: medicationRequest.id,
    medicationStatementId: medicationStatement.id,
    medic: medicationRequest.medicationCodeableConcept.text,
    status: medicationStatement.status,
  }
}

export async function deleteMedicationStatement(medicationStatementId: string) {
  const { data } = await api.delete<void>(`fhir/medicationstatement/${medicationStatementId}`)
  return data
}

export async function createImmunization(
  values: { text: string; code: string },
  patientId: string
): Promise<IImmunization> {
  const body: IImmunization = {
    resourceType: 'Immunization',
    status: 'completed',
    occurrenceDateTime: now(),
    vaccineCode: {
      coding: [
        {
          code: values.code,
          display: values.text,
          system: 'http://hl7.org/fhir/sid/cvx',
        },
      ],
      text: values.text,
    },
    patient: {
      reference: `Patient/${patientId}`,
    },
  }
  const { data } = await api.post<IImmunization>(`fhir/immunization`, body)
  return data
}

export async function getImmunizationsByPatientId(patientId: string): Promise<IImmunization[]> {
  const bundle = await getImmunizations({ subject: `Patient/${patientId}`, _count: 100 })
  return bundle.entry
    ?.filter(({ search }) => search.mode === 'match')
    .map(({ resource }) => resource)
}

export async function deleteImmunization(immunizationId: string) {
  const { data } = await api.delete<void>(`fhir/immunization/${immunizationId}`)
  return data
}

/**
 * Search Medics with clinicaltables.nlm.nih.gov
 *
 * @param query {string} - search query
 * @returns Promise<Record<medicName, strengths[]>>
 */
export async function searchMedic(query: string): Promise<Record<string, string[]>> {
  const url = `https://clinicaltables.nlm.nih.gov/api/rxterms/v3/search?ef=STRENGTHS_AND_FORMS&maxList=20&terms=${encodeURIComponent(
    query
  )}`
  const { data } = await api.get(url)
  const [, medicNames, { STRENGTHS_AND_FORMS: strengths }] = data
  return medicNames.reduce(
    (obj, current, index) => ({
      ...obj,
      [current]: strengths[index],
    }),
    {}
  )
}
