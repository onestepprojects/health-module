import { Form, Input, Select, type TableColumnType } from 'antd'
import dayjs from 'dayjs'
import startCase from 'lodash/startCase'
import type React from 'react'
import { useEffect, useState } from 'react'
import { type IAllergyIntolerance } from '../../../types'
import {
  PROTOCOL_PREFIX,
  createAllergy,
  createImmunization,
  createMedication,
  deleteAllergy,
  deleteImmunization,
  deleteMedicationStatement,
  getAllAllergiesByPatientId,
  getImmunizationsByPatientId,
  getMedicationsByPatientId,
  searchMedic,
  updateAllergy,
} from './tabServices'

export interface TabSpecification {
  singular?: string
  fetchRecords: (patientId: string) => Promise<any>
  createRecord: (values: any, patientId: string) => Promise<any> | null
  updateRecord?: (recordId: string, record: any, values: any) => Promise<any> | null
  deleteRecord: (id: string) => Promise<any> | null
  modalContent: () => React.ReactNode
  getInitialValues?: (record: any) => any
  tableColumns: () => TableColumnType<any>
}

export const tabSpecifications: Record<string, TabSpecification> = {
  allergies: {
    singular: 'Allergy',
    fetchRecords: getAllAllergiesByPatientId,
    createRecord: createAllergy,
    updateRecord: updateAllergy,
    deleteRecord: deleteAllergy,
    modalContent: () => (
      <>
        <Form.Item label='Category' name='category' required>
          <Select
            showSearch
            options={['food', 'medication', 'environment', 'biologic'].map((value) => ({
              label: startCase(value),
              value,
            }))}
          />
        </Form.Item>
        <Form.Item label='Text' name='text' required>
          <Input />
        </Form.Item>
        <Form.Item label='Code' name='code'>
          <Input />
        </Form.Item>
        <Form.Item label='System' name='system'>
          <Input addonBefore={PROTOCOL_PREFIX} />
        </Form.Item>
      </>
    ),
    getInitialValues: (record: IAllergyIntolerance) => ({
      category: record.category[0],
      text: record.code.text,
      code: record.code.coding[0].code,
      system: record.code.coding[0].system?.replace(/(^\w+:|^)\/\//, ''), // Remove the protocol from url
    }),
    tableColumns: () => [
      {
        title: 'Category',
        dataIndex: ['category', 0],
        fixed: 'left',
        filters: ['food', 'medication', 'environment', 'biologic'].map((value) => ({
          text: startCase(value),
          value,
        })),
        onFilter: (value: string, record) => record.category.indexOf(value) >= 0,
        sorter: (a, b) => a.category[0].localeCompare(b.category[0]),
      },
      {
        title: 'Text',
        dataIndex: ['code', 'text'],
        sorter: (a, b) => a.code.text.localeCompare(b.code.text),
      },
      {
        title: 'Code',
        dataIndex: ['code', 'coding', 0, 'code'],
        sorter: (a, b) => a.code.coding[0]?.code?.localeCompare(b.code.coding[0].code),
      },
      {
        title: 'System',
        dataIndex: ['code', 'coding', 0, 'system'],
        sorter: (a, b) => a.code.coding[0]?.system?.localeCompare(b.code.coding[0].system),
      },
      {
        title: 'Create Date',
        dataIndex: ['recordedDate'],
        sorter: (a, b) => dayjs(a.recordedDate).diff(b.recordedDate),
        defaultSortOrder: 'descend',
        render: (data) => dayjs(data).format('ll'),
      },
    ],
  },
  medications: {
    singular: 'Medication',
    fetchRecords: getMedicationsByPatientId,
    createRecord: createMedication,
    deleteRecord: deleteMedicationStatement,
    modalContent: () => {
      // Returns a customized form item component
      return () => {
        const [medicResults, setMedicResults] = useState<Awaited<ReturnType<typeof searchMedic>>>(
          {}
        )
        const [medicOptions, setMedicOptions] = useState<React.ReactElement[]>([])
        const [strengthOptions, setStrengthOptions] = useState<React.ReactElement[]>([])
        const [medic, setMedic] = useState<string>()
        const [strength, setStrength] = useState<string>()

        const handleMedicSearch = async (newValue: string) => {
          if (newValue) {
            const medicsResults = await searchMedic(newValue)
            const medicOptions = Object.keys(medicsResults).map((text) => (
              <Select.Option key={text}>{text}</Select.Option>
            ))
            setMedicResults(medicsResults)
            setMedicOptions(medicOptions)
          } else {
            setMedicOptions([])
          }
        }

        const handleMedicChange = (newMedic: string) => setMedic(newMedic)

        const handleStrengthChange = (newStrength: string) => {
          setStrength(newStrength)
        }

        useEffect(() => {
          // Clear old selected value and options
          setStrength('')
          setStrengthOptions([])

          // Set new options
          if (medic)
            setStrengthOptions(
              medicResults[medic].map((text) => <Select.Option key={text}>{text}</Select.Option>)
            )
        }, [medic, medicResults])

        return (
          <>
            <Form.Item label='Medication' name='medicName' required>
              <Select
                showSearch
                value={medic}
                defaultActiveFirstOption={false}
                onSearch={handleMedicSearch}
                onChange={handleMedicChange}
                notFoundContent={null}
              >
                {medicOptions}
              </Select>
            </Form.Item>
            <Form.Item label='Strength' name='strength' required>
              <Select
                value={strength}
                defaultActiveFirstOption={false}
                filterOption={false}
                onChange={handleStrengthChange}
              >
                {strengthOptions}
              </Select>
            </Form.Item>
          </>
        )
      }
    },
    tableColumns: () => [
      { title: 'Medication', dataIndex: ['medic'], fixed: 'left' },
      { title: 'Status', dataIndex: ['status'] },
    ],
  },
  immunizations: {
    singular: 'Immunization',
    fetchRecords: getImmunizationsByPatientId,
    createRecord: createImmunization,
    deleteRecord: deleteImmunization,
    modalContent: () => (
      <>
        <Form.Item label='Immunization' name='text' required>
          <Input />
        </Form.Item>
        <Form.Item label='Code' name='code' required>
          <Input type='number' />
        </Form.Item>
      </>
    ),
    tableColumns: () => [
      { title: 'Immunization', dataIndex: ['vaccineCode', 'coding', 0, 'display'], fixed: 'left' },
      { title: 'Code', dataIndex: ['vaccineCode', 'coding', 0, 'code'] },
      { title: 'System', dataIndex: ['vaccineCode', 'coding', 0, 'system'] },
    ],
  },
}
