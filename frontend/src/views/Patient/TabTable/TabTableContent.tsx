import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons'
import { useMemoizedFn, useRequest } from 'ahooks'
import {
  Button,
  Col,
  Form,
  Modal,
  Popconfirm,
  Row,
  Space,
  Table,
  Typography,
  message,
  type FormInstance,
} from 'antd'
import startCase from 'lodash/startCase'
import { useEffect, useRef, useState, type FC } from 'react'
import { useMatch, useNavigate } from 'react-router-dom'
import { TAB_OPTIONS } from '../TAB_OPTIONS'
import { tabSpecifications, type TabSpecification } from './tabSpecifications'

export const TabTableContent: FC = () => {
  const navigate = useNavigate()
  const {
    params: { patientId, tabValue },
  } = useMatch('patients/:patientId/:tabValue')

  // Set the tab specification for the current tab, if the tab is not found, navigate to the first tab
  const [tabSpecification, setTabSpecification] = useState<TabSpecification | null>(null)

  // Request data
  const {
    loading: isLoading,
    data: tableContent,
    mutate: mutation,
    refresh,
  } = useRequest(() => {
    const tabSpecification = tabSpecifications[tabValue]

    // If tableValue is not exist replace to first tab
    if (tabSpecification === undefined)
      navigate(`../${TAB_OPTIONS[0].value}`, {
        replace: true,
      })

    setTabSpecification(tabSpecification)
    return tabSpecification.fetchRecords(patientId)
  })

  useEffect(() => {
    mutation([])
    refresh()
  }, [mutation, refresh, tabValue])

  const createFormRef = useRef<FormInstance>()

  // PascalCase the tab name
  const title = startCase(tabValue)

  // Create new record modal
  const handleCreateClick = useMemoizedFn(() => {
    Modal.confirm({
      title: (
        <Typography.Title level={4}>New {tabSpecification.singular ?? title}</Typography.Title>
      ),
      icon: false,
      closable: true,
      maskClosable: false,
      content: (
        <Form ref={createFormRef} labelCol={{ span: 8 }}>
          {tabSpecification.modalContent()}
        </Form>
      ),
      okText: 'Save',
      onOk: async () => {
        const values = await createFormRef.current.validateFields()
        try {
          const record = await tabSpecification.createRecord(values, patientId)
          mutation((prev = []) => [record, ...prev])
        } catch (err) {
          message.error(err?.message)
        }
      },
    })
  })

  const handleEditClick = useMemoizedFn((record) => {
    Modal.confirm({
      title: (
        <Typography.Title level={4}>Edit {tabSpecification.singular ?? title}</Typography.Title>
      ),
      icon: false,
      closable: true,
      maskClosable: false,
      content: (
        <Form
          ref={createFormRef}
          labelCol={{ span: 8 }}
          initialValues={tabSpecification.getInitialValues(record)}
        >
          {tabSpecification.modalContent(record)}
        </Form>
      ),
      okText: 'Save',
      onOk: async () => {
        const values = await createFormRef.current.validateFields()
        try {
          const newRecord = await tabSpecification.updateRecord(record.id, record, values)
          mutation((prev = []) => prev.map((item) => (item.id === newRecord.id ? newRecord : item)))
        } catch (err) {
          message.error(err?.message)
        }
      },
    })
  })

  // Waiting for the tabSpecification specified
  if (!tabSpecification) return <></>

  // Render the records table
  return (
    <Table
      loading={isLoading}
      rowKey='id'
      title={() => (
        <Row>
          <Col span={12}>
            <Typography.Title>{title}</Typography.Title>
          </Col>
          {tabSpecification.createRecord ? (
            <Col span={12}>
              <div className='flex justify-end'>
                <Button type='primary' icon={<PlusOutlined />} onClick={handleCreateClick}>
                  Add {tabSpecification.singular ?? title}
                </Button>
              </div>
            </Col>
          ) : null}
        </Row>
      )}
      columns={[
        ...tabSpecification.tableColumns(),
        ...(tabSpecification.updateRecord || tabSpecification.deleteRecord
          ? [
              {
                title: 'Action',
                render: (_, record) => (
                  <Space>
                    {tabSpecification.updateRecord ? (
                      <Button
                        type='link'
                        icon={<EditOutlined />}
                        onClick={() => handleEditClick(record)}
                      />
                    ) : null}
                    {tabSpecification.deleteRecord ? (
                      <Popconfirm
                        title='Are you sure?'
                        okText='Delete'
                        okButtonProps={{ danger: true }}
                        onConfirm={async () => {
                          try {
                            await tabSpecification.deleteRecord(record.id)
                            mutation((prev = []) => prev.filter((item) => item.id !== record.id))
                          } catch (err) {
                            message.error(err?.message)
                          }
                        }}
                      >
                        <Button type='text' danger icon={<DeleteOutlined />} />
                      </Popconfirm>
                    ) : null}
                  </Space>
                ),
              },
            ]
          : []),
      ]}
      dataSource={tableContent}
    />
  )
}
