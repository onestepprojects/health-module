import { DeleteOutlined, PlusOutlined } from '@ant-design/icons'
import { useMemoizedFn, useRequest } from 'ahooks'
import {
  Button,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Modal,
  Popconfirm,
  Select,
  Space,
  Table,
  Typography,
  type FormInstance,
} from 'antd'
import dayjs from 'dayjs'
import { useRef, type FC } from 'react'
import {
  createObservation,
  deleteObservation,
  getAllObservationsByPatientId,
} from '../../../services'
import { type IObservation } from '../../../types'
import { usePatientOutletContext } from '../usePatientOutletContext'
import { observationCategoryCodingCodes } from './options'

export const MedicalHistoryPage: FC = () => {
  const { patient } = usePatientOutletContext()

  const {
    loading: isObservationsLoading,
    data: observations,
    mutate: mutateObservations,
  } = useRequest(() => getAllObservationsByPatientId(patient.id))

  const createObservationFormRef = useRef<FormInstance<ObservationFormData>>()
  const handleCreateObservationClick = useMemoizedFn(() => {
    Modal.confirm({
      title: <Typography.Title level={4}>New Medical History</Typography.Title>,
      icon: false,
      closable: true,
      maskClosable: false,
      content: (
        <Form
          ref={createObservationFormRef}
          labelCol={{ span: 8 }}
          initialValues={
            {
              effectiveDateTime: dayjs(),
              categoryCodingCode: observationCategoryCodingCodes[1],
            } as ObservationFormData
          }
        >
          <Form.Item label='Effective Date' name='effectiveDateTime'>
            <DatePicker showTime allowClear={false} />
          </Form.Item>
          <Form.Item label='Category' name='categoryCodingCode' required>
            <Select
              showSearch
              options={observationCategoryCodingCodes.map((value) => ({ label: value, value }))}
            />
          </Form.Item>
          <Form.Item label='Code' name='codeText' required>
            <Input />
          </Form.Item>
          <Form.Item label='Quantity'>
            <Space>
              <Form.Item noStyle name='valueQuantityValue'>
                <InputNumber />
              </Form.Item>
              <Form.Item noStyle name='valueQuantityUnit'>
                <Input placeholder='Unit' />
              </Form.Item>
            </Space>
          </Form.Item>
        </Form>
      ),
      okText: 'Save',
      onOk: async () => {
        const values = await createObservationFormRef.current.validateFields()
        const observation = await createObservation(
          buildObservationBody({ ...values, patientId: patient.id })
        )
        mutateObservations((prev = []) => [observation, ...prev])
      },
    })
  })

  return (
    <Table
      loading={isObservationsLoading}
      rowKey='id'
      title={() => (
        <div className='flex justify-end'>
          <Button type='primary' icon={<PlusOutlined />} onClick={handleCreateObservationClick}>
            Add Medical History
          </Button>
        </div>
      )}
      columns={[
        { title: 'Category', dataIndex: ['category', 0, 'coding', 0, 'display'], fixed: 'left' },
        { title: 'Code', dataIndex: ['code', 'text'] },
        {
          title: 'Quantity',
          dataIndex: ['valueQuantity'],
          render: (data) => (data ? `${data.value} ${data.unit}` : ''),
        },
        {
          title: 'Effective Date',
          dataIndex: ['effectiveDateTime'],
          sorter: (a, b) => dayjs(a.effectiveDateTime).diff(b.effectiveDateTime),
          defaultSortOrder: 'descend',
          render: (data) => dayjs(data).format('ll'),
        },
        {
          title: 'Action',
          render: (_, record) => (
            <Space>
              <Popconfirm
                title='Are you sure?'
                okText='Delete'
                okButtonProps={{ danger: true }}
                onConfirm={async () => {
                  await deleteObservation(record.id)
                  mutateObservations((prev = []) => prev.filter((item) => item.id !== record.id))
                }}
              >
                <Button type='text' danger icon={<DeleteOutlined />} />
              </Popconfirm>
            </Space>
          ),
        },
      ]}
      dataSource={observations}
    />
  )
}

interface ObservationFormData {
  effectiveDateTime: dayjs.Dayjs
  categoryCodingCode: string
  codeText: string
  valueQuantityValue: number
  valueQuantityUnit: string
  patientId: string
}

function buildObservationBody(data: ObservationFormData): IObservation {
  return {
    resourceType: 'Observation',
    // id: '12b9e914-0fe5-48f2-bd3a-b2654833c555',
    // meta: {
    //   lastUpdated: '2022-07-26T04:46:48.572Z',
    // },
    status: 'final' as IObservation['status'],
    category: [
      {
        coding: [
          {
            system: 'http://terminology.hl7.org/CodeSystem/observation-category',
            code: data.categoryCodingCode,
            display: data.categoryCodingCode,
          },
        ],
      },
    ],
    code: {
      // coding: [
      //   {
      //     system: 'http://loinc.org',
      //     code: '789-8',
      //     display: 'Erythrocytes [#/volume] in Blood by Automated count',
      //   },
      // ],
      text: data.codeText,
    },
    subject: {
      reference: `Patient/${data.patientId}`,
    },
    // encounter: {
    //   reference: 'Encounter/0fce7a0a-3035-40c8-a129-50487fc75409',
    // },
    effectiveDateTime: data.effectiveDateTime.format(),
    // issued: '2020-01-25T02:37:32.457-08:00',
    valueQuantity: {
      value: data.valueQuantityValue,
      unit: data.valueQuantityUnit,
      system: 'http://unitsofmeasure.org',
      code: data.valueQuantityUnit,
    },
  }
}
