import { startCase } from 'lodash'

/**
 * All codes in this table are from the system http://terminology.hl7.org/CodeSystem/observation-category
 *
 * @see https://www.hl7.org/fhir/valueset-observation-category.html#expansion
 */
export const observationCategoryCodingCodes = [
  'social-history',
  'vital-signs',
  'imaging',
  'laboratory',
  'procedure',
  'survey',
  'exam',
  'therapy',
  'activity',
]

export const observationCategoryCodingOptions = observationCategoryCodingCodes.map((code) => ({
  label: startCase(code),
  value: code,
}))
