import { type ICarePlan } from '../../../types'

export const defaultCarePlan: ICarePlan = {
  resourceType: 'CarePlan',
  // id: '5d154d27-8c4a-432f-8b5f-280d9ab884bf',
  // meta: {
  //   lastUpdated: '2022-07-26T04:46:48.573Z',
  // },
  // text: {
  //   status: 'generated' as ICarePlan['text']['status'],
  //   div: '<div xmlns="http://www.w3.org/1999/xhtml">Care Plan for Infectious disease care plan (record artifact).<br/>Activities: <ul><li>Infectious disease care plan (record artifact)</li><li>Infectious disease care plan (record artifact)</li></ul><br/>Care plan is meant to treat COVID-19.</div>',
  // },
  status: 'completed',
  intent: 'order',
  // category: [
  //   {
  //     coding: [
  //       {
  //         system: 'http://snomed.info/sct',
  //         code: '',
  //         display: '',
  //       },
  //     ],
  //     text: '',
  //   },
  // ],
  subject: {
    reference: `Patient/`,
  },
  // encounter: {
  //   reference: 'Encounter/2349e4b5-5163-425d-a444-b0e459a0260d',
  // },
  // period: {
  //   start: '2020-03-10T05:00:32-07:00',
  //   end: '2020-04-02T05:00:32-07:00',
  // },
  // careTeam: [
  //   {
  //     reference: 'CareTeam/dd48547c-79dd-4e74-8c16-bc453ed3003c',
  //   },
  // ],
  // addresses: [
  //   {
  //     reference: 'Condition/0fb38c67-87dd-40e1-b950-f65d1a91ba85',
  //   },
  // ],
  // activity: [
  //   {
  //     detail: {
  //       code: {
  //         coding: [
  //           {
  //             system: 'http://snomed.info/sct',
  //             code: '409524006',
  //             display: 'Airborne precautions (procedure)',
  //           },
  //         ],
  //         text: 'Airborne precautions (procedure)',
  //       },
  //       status: 'completed',
  //       location: {
  //         reference: 'Location/1d496c75-3b14-41a9-91de-26b22c0edb87',
  //         display: 'NORWOOD HOSPITAL',
  //       },
  //     },
  //   },
  //   {
  //     detail: {
  //       code: {
  //         coding: [
  //           {
  //             system: 'http://snomed.info/sct',
  //             code: '361235007',
  //             display: 'Isolation of infected patient (procedure)',
  //           },
  //         ],
  //         text: 'Isolation of infected patient (procedure)',
  //       },
  //       status: 'completed',
  //       location: {
  //         reference: 'Location/1d496c75-3b14-41a9-91de-26b22c0edb87',
  //         display: 'NORWOOD HOSPITAL',
  //       },
  //     },
  //   },
  // ],
}

/**
 * All codes in this table are from the system http://snomed.info/sct
 *
 * @see https://www.hl7.org/fhir/valueset-care-plan-category.html
 */
export const carePlanCategoryOptions = [
  { value: '734163000', label: 'Care plan (record artifact)' },
  { value: '718347000', label: 'Mental health care plan (record artifact)' },
  { value: '735321000', label: 'Surgical inpatient care plan (record artifact)' },
  { value: '735322007', label: 'Termination of pregnancy inpatient care plan (record artifact)' },
  { value: '735323002', label: 'Thoracic surgery inpatient care plan (record artifact)' },
  { value: '735324008', label: 'Treatment escalation plan (record artifact)' },
  { value: '735325009', label: 'Unplanned inpatient care plan (record artifact)' },
  {
    value: '735326005',
    label: 'Upper gastrointestinal surgery inpatient care plan (record artifact)',
  },
  {
    value: '735327001',
    label: 'Upper gastrointestinal tract endoscopy care plan (record artifact)',
  },
  { value: '735328006', label: 'Urological surgery inpatient care plan (record artifact)' },
  { value: '735329003', label: 'Vascular surgery inpatient care plan (record artifact)' },
  { value: '735330008', label: 'Vulnerable adult care plan (record artifact)' },
  { value: '735984001', label: 'Heart failure self management plan' },
  { value: '735985000', label: 'Diabetes self management plan (record artifact)' },
  { value: '735986004', label: 'Patient written birth plan (record artifact)' },
  { value: '736054002', label: 'Pressure ulcer care plan (record artifact)' },
  { value: '736055001', label: 'Rehabilitation care plan (record artifact)' },
  { value: '736056000', label: 'Asthma clinical management plan' },
  { value: '736057009', label: 'Respiratory disorder inpatient care plan (record artifact)' },
  { value: '736058004', label: 'Mental health personal health plan' },
  { value: '736059007', label: 'Angina self management plan' },
  { value: '736234007', label: 'Pediatric community care plan (record artifact)' },
  { value: '736235008', label: 'Pediatric inpatient care plan (record artifact)' },
  { value: '736241001', label: 'Pediatric outpatient care plan (record artifact)' },
  { value: '736245005', label: 'Pediatric community complex care plan (record artifact)' },
  { value: '736246006', label: 'Pediatric surgery inpatient care plan (record artifact)' },
  { value: '736248007', label: 'Pain control care plan (record artifact)' },
  { value: '736249004', label: 'Radiotherapy care plan (record artifact)' },
  { value: '736250004', label: 'Respite care plan (record artifact)' },
  { value: '736251000', label: 'Stroke care plan (record artifact)' },
  { value: '736252007', label: 'Cancer care plan' },
  { value: '736253002', label: 'Mental health crisis plan' },
  { value: '736254008', label: 'Psychiatry care plan (record artifact)' },
  { value: '736271009', label: 'Outpatient care plan (record artifact)' },
  { value: '736282001', label: 'Neonatal critical care plan (record artifact)' },
  {
    value: '736283006',
    label: 'Chronic obstructive pulmonary disease clinical management plan (record artifact)',
  },
  { value: '736284000', label: 'Diabetes clinical management plan' },
  { value: '736285004', label: 'Hyperlipidemia clinical management plan (record artifact)' },
  { value: '736286003', label: 'Hypertension clinical management plan (record artifact)' },
  { value: '736287007', label: 'Hypothyroidism clinical management plan' },
  {
    value: '736288002',
    label: 'Transient ischemic attack clinical management plan (record artifact)',
  },
  { value: '736336006', label: 'Miscarriage inpatient care plan (record artifact)' },
  { value: '736337002', label: 'Neurosurgery inpatient care plan (record artifact)' },
  { value: '736338007', label: 'Ophthalmic surgery inpatient care plan (record artifact)' },
  { value: '736339004', label: 'Orthopedic surgery inpatient care plan (record artifact)' },
  {
    value: '736340002',
    label: 'Ovarian hyperstimulation syndrome inpatient care plan (record artifact)',
  },
  { value: '736351002', label: 'Breast surgery inpatient care plan (record artifact)' },
  { value: '736352009', label: 'Cardiac surgery inpatient care plan (record artifact)' },
  { value: '736353004', label: 'Inpatient care plan (record artifact)' },
  {
    value: '736355006',
    label: 'Gynecology bladder training inpatient care plan (record artifact)',
  },
  { value: '736356007', label: 'Gynecology inpatient care plan (record artifact)' },
  { value: '736357003', label: 'Minor gynecology surgery inpatient care plan (record artifact)' },
  { value: '736358008', label: 'Minor ophthalmic surgery inpatient care plan (record artifact)' },
  { value: '736359000', label: 'Minor surgery inpatient care plan (record artifact)' },
  { value: '736361009', label: 'Minor thoracic surgery inpatient care plan (record artifact)' },
  { value: '736362002', label: 'Minor urologic surgery inpatient care plan (record artifact)' },
  { value: '736363007', label: 'Minor plastic surgery inpatient care plan (record artifact)' },
  { value: '736364001', label: 'Minor orthopedic surgery inpatient care plan (record artifact)' },
  {
    value: '736365000',
    label:
      'Acute exacerbation of chronic obstructive pulmonary disease care plan (record artifact)',
  },
  { value: '736366004', label: 'Advance care plan (record artifact)' },
  { value: '736367008', label: 'Anticipatory care plan (record artifact)' },
  { value: '736368003', label: 'Coronary heart disease care plan (record artifact)' },
  { value: '736369006', label: 'Day case care plan (record artifact)' },
  { value: '736370007', label: 'Day case surgery care plan (record artifact)' },
  { value: '736371006', label: 'Dementia care plan (record artifact)' },
  { value: '736372004', label: 'Discharge care plan (record artifact)' },
  { value: '736373009', label: 'End of life care plan (record artifact)' },
  { value: '736374003', label: 'Gynecology unplanned inpatient care plan (record artifact)' },
  { value: '736375002', label: 'Hemodialysis care plan (record artifact)' },
  { value: '736376001', label: 'Infectious disease care plan (record artifact)' },
  { value: '736377005', label: 'Maternity care plan (record artifact)' },
  { value: '736378000', label: 'Medication management plan (record artifact)' },
  { value: '736379008', label: 'Miscarriage care plan (record artifact)' },
  { value: '736381005', label: 'Major surgery inpatient care plan (record artifact)' },
  {
    value: '736382003',
    label: 'Major head and neck surgery inpatient care plan (record artifact)',
  },
  { value: '736383008', label: 'Major neurosurgery inpatient care plan (record artifact)' },
  { value: '736384002', label: 'Major orthopedic surgery inpatient care plan (record artifact)' },
  { value: '736385001', label: 'Major plastic surgery inpatient care plan (record artifact)' },
  { value: '736386000', label: 'Major vascular surgery inpatient care plan (record artifact)' },
  { value: '736387009', label: 'Major urologic surgery inpatient care plan (record artifact)' },
  { value: '736389007', label: 'Colorectal surgery inpatient care plan (record artifact)' },
  { value: '736390003', label: 'Gynecology major surgery inpatient care plan (record artifact)' },
  {
    value: '736391004',
    label: 'Hepatobiliary and pancreatic surgery inpatient care plan (record artifact)',
  },
  { value: '736392006', label: 'Bronchoscopy care plan (record artifact)' },
  { value: '736393001', label: 'Colposcopy care plan (record artifact)' },
  { value: '736394007', label: 'Cystoscopy care plan (record artifact)' },
  { value: '736395008', label: 'Hysteroscopy care plan (record artifact)' },
  { value: '736396009', label: 'Interventional cardiology inpatient care plan (record artifact)' },
  {
    value: '736397000',
    label: 'Lower gastrointestinal tract endoscopy care plan (record artifact)',
  },
  { value: '736399002', label: 'Burn inpatient care plan (record artifact)' },
  { value: '736400009', label: 'Cardiology inpatient care plan (record artifact)' },
  { value: '736401008', label: 'Chemotherapy care plan (record artifact)' },
  { value: '736402001', label: 'Hip fracture inpatient care plan (record artifact)' },
  { value: '736403006', label: 'Hyperemesis in pregnancy inpatient care plan (record artifact)' },
  { value: '736453000', label: 'Laryngectomy surgery inpatient care plan (record artifact)' },
  { value: '736482004', label: 'Major burn inpatient care plan (record artifact)' },
  { value: '736690008', label: 'Dialysis care plan (record artifact)' },
  { value: '736990006', label: 'Plastic surgery inpatient care plan (record artifact)' },
  { value: '737426005', label: 'Ankle brachial pressure index management plan (record artifact)' },
  { value: '737427001', label: 'Clinical management plan' },
  { value: '737428006', label: 'Arthritis clinical management plan (record artifact)' },
  {
    value: '737429003',
    label: 'Chronic kidney disease clinical management plan (record artifact)',
  },
  {
    value: '737430008',
    label: 'Congestive heart failure clinical management plan (record artifact)',
  },
  { value: '737431007', label: 'Coronary heart disease risk clinical management plan' },
  { value: '737432000', label: 'Hyperglycemia clinical management plan' },
  { value: '737433005', label: 'Hypoglycemia clinical management plan (record artifact)' },
  { value: '737434004', label: 'Major depressive disorder clinical management plan' },
  { value: '737435003', label: 'Malnutrition clinical management plan (record artifact)' },
  { value: '737436002', label: 'Hypoglycemia self management plan (record artifact)' },
  { value: '737437006', label: 'Hyperglycemia self management plan' },
  { value: '737438001', label: 'Myocardial infarction clinical management plan (record artifact)' },
  { value: '737439009', label: 'Seizure disorder clinical management plan' },
  { value: '744980002', label: 'Medical critical care plan' },
  {
    value: '763249000',
    label: 'Mental health care program approach contingency plan (record artifact)',
  },
  { value: '763321009', label: 'Mental health care program approach crisis plan' },
  { value: '770576004', label: 'Prevention of delirium care plan (record artifact)' },
  { value: '771082000', label: 'Acute medicine care plan' },
  { value: '773130005', label: 'Nursing care plan (record artifact)' },
  { value: '773430001', label: 'Obstetric care plan (record artifact)' },
  { value: '773431002', label: 'Obstetric perinatal care plan (record artifact)' },
  { value: '773432009', label: 'Obstetric postnatal care plan' },
  { value: '773433004', label: 'Obstetric antenatal care plan' },
  { value: '773435006', label: 'Surgical care plan' },
  { value: '773436007', label: 'Plastic surgery care plan (record artifact)' },
  { value: '773437003', label: 'Trauma surgery care plan (record artifact)' },
  { value: '773438008', label: 'Oral surgery care plan (record artifact)' },
  { value: '773439000', label: 'Hepatobiliary surgical care plan (record artifact)' },
  { value: '773440003', label: 'Dermatological surgery care plan' },
  { value: '773441004', label: 'Hand surgery care plan (record artifact)' },
  { value: '773442006', label: 'Podiatric surgery care plan' },
  { value: '773443001', label: 'Ophthalmic surgery care plan' },
  { value: '773444007', label: 'Pancreatic surgery care plan (record artifact)' },
  { value: '773445008', label: 'Gastrointestinal surgery care plan' },
  { value: '773446009', label: 'Rehabilitation psychiatry care plan (record artifact)' },
  { value: '773447000', label: 'Forensic psychiatry care plan' },
  { value: '773448005', label: 'Psychiatric intensive care plan (record artifact)' },
  { value: '773449002', label: 'Old age psychiatry care plan (record artifact)' },
  { value: '773450002', label: 'Liaison psychiatry care plan' },
  { value: '773451003', label: 'Psychotherapy care plan (record artifact)' },
  { value: '773452005', label: 'Psychology care plan (record artifact)' },
  { value: '773506001', label: 'Gynecology care plan (record artifact)' },
  { value: '773507005', label: 'Gynecology oncology care plan (record artifact)' },
  { value: '773508000', label: 'Interventional radiology care plan' },
  { value: '773509008', label: 'Ophthalmology care plan (record artifact)' },
  { value: '773511004', label: 'Occupational therapy care plan (record artifact)' },
  { value: '773512006', label: 'Genitourinary medicine care plan (record artifact)' },
  { value: '773513001', label: 'Physiotherapy care plan (record artifact)' },
  { value: '773514007', label: 'Speech and language therapy care plan (record artifact)' },
  { value: '773516009', label: 'Head injury rehabilitation care plan (record artifact)' },
  { value: '773517000', label: 'Osteopathic manipulative medicine care plan' },
  { value: '773590002', label: 'Dietetic care plan (record artifact)' },
  { value: '773591003', label: 'Podiatry care plan (record artifact)' },
  { value: '773592005', label: 'Occupational health care plan (record artifact)' },
  { value: '773593000', label: 'Urology care plan (record artifact)' },
  { value: '773594006', label: 'Orthodontic care plan' },
  { value: '773595007', label: 'Respiratory medicine care plan' },
  { value: '773596008', label: 'Care of elderly care plan' },
  { value: '773597004', label: 'Orthopedic care plan' },
  { value: '773598009', label: 'Gastroenterology care plan (record artifact)' },
  { value: '773599001', label: 'Hemophilia care plan' },
  { value: '773976008', label: 'Dermatology care plan' },
  { value: '773977004', label: 'Hepatology care plan' },
  { value: '773978009', label: 'Endocrinology care plan (record artifact)' },
  { value: '773979001', label: 'Prosthetic care plan' },
  { value: '773980003', label: 'Orthotic care plan' },
  { value: '773981004', label: 'Palliative care plan (record artifact)' },
  { value: '773982006', label: 'Rheumatology care plan' },
  { value: '773983001', label: 'ENT (ear, nose, throat) care plan' },
  { value: '774202005', label: 'Special care baby care plan (record artifact)' },
]
