import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons'
import { useMemoizedFn, useRequest } from 'ahooks'
import {
  Button,
  Form,
  Input,
  Modal,
  Popconfirm,
  Space,
  Table,
  Typography,
  type FormInstance,
} from 'antd'
import { useRef, type FC } from 'react'
import { CategorySelectFormItem } from '../../../components'
import {
  createCarePlan,
  deleteCarePlan,
  getAllCarePlansByPatientId,
  updateCarePlan,
} from '../../../services'
import { type ICarePlan } from '../../../types'
import { usePatientOutletContext } from '../usePatientOutletContext'
import { carePlanCategoryOptions, defaultCarePlan } from './constants'

export const CarePlanPage: FC = () => {
  const { patient } = usePatientOutletContext()

  const {
    loading: isCarePlansLoading,
    data: carePlans,
    mutate: mutateCarePlans,
  } = useRequest(() => getAllCarePlansByPatientId(patient.id))

  const carePlanFormRef = useRef<FormInstance<ICarePlan>>()

  const openCarePlanFormModal = useMemoizedFn((carePlan?: ICarePlan) => {
    Modal.confirm({
      title: <Typography.Title level={4}>{carePlan ? 'Edit' : 'New'} Care Plan</Typography.Title>,
      icon: false,
      closable: true,
      maskClosable: false,
      content: (
        <Form ref={carePlanFormRef} labelCol={{ span: 8 }} initialValues={carePlan}>
          <CategorySelectFormItem required options={carePlanCategoryOptions} />
          <Form.Item label='Title' name='title'>
            <Input />
          </Form.Item>
          <Form.Item label='Description' name='description'>
            <Input />
          </Form.Item>
        </Form>
      ),
      okText: 'Save',
      onOk: async () => {
        const values = await carePlanFormRef.current.validateFields()
        if (carePlan) {
          const newCarePlan = await updateCarePlan({ ...carePlan, ...values })
          mutateCarePlans((prev = []) =>
            prev.map((item) => (item.id === newCarePlan.id ? newCarePlan : item))
          )
        } else {
          const newCarePlan = await createCarePlan({
            ...defaultCarePlan,
            ...values,
            subject: {
              reference: `Patient/${patient.id}`,
            },
          })
          mutateCarePlans((prev = []) => [newCarePlan, ...prev])
        }
      },
    })
  })

  return (
    <Table
      loading={isCarePlansLoading}
      rowKey='id'
      title={() => (
        <div className='flex justify-end'>
          <Button type='primary' icon={<PlusOutlined />} onClick={() => openCarePlanFormModal()}>
            Add Care Plan
          </Button>
        </div>
      )}
      columns={[
        { title: 'Category', dataIndex: ['category', 0, 'text'] },
        { title: 'Title', dataIndex: ['title'] },
        { title: 'Description', dataIndex: ['description'] },
        {
          title: 'Action',
          render: (_, record) => (
            <Space>
              <Button
                type='link'
                icon={<EditOutlined />}
                onClick={() => openCarePlanFormModal(record)}
              />
              <Popconfirm
                title='Are you sure?'
                okText='Delete'
                okButtonProps={{ danger: true }}
                onConfirm={async () => {
                  await deleteCarePlan(record.id)
                  mutateCarePlans((prev = []) => prev.filter((item) => item.id !== record.id))
                }}
              >
                <Button type='text' danger icon={<DeleteOutlined />} />
              </Popconfirm>
            </Space>
          ),
        },
      ]}
      dataSource={carePlans}
    />
  )
}
