import { type FC } from 'react'
import { usePatientOutletContext } from './usePatientOutletContext'

export const AllergiesPage: FC = () => {
  const { patient } = usePatientOutletContext()

  return <div>Allergies Page</div>
}
