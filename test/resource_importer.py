import json 
import os
import requests

# sample data file
sample_data_file = "/Users/Madhan/Downloads/Questionnaire.json"

# open the file and trucate the values
# the current FHIR service supports uploading not more than 25 entries
with open(sample_data_file) as json_file:
    data = json.load(json_file)
    print(len(data['entry']))
    data['entry'] = data['entry'][:25]

# set the FHIR service parameters
# oauth token is copied from Postman
fhir_url = "https://02d7okvn3e.execute-api.us-east-1.amazonaws.com/dev"
api_key = "iexfi2Ue8S25DVJPDzpUc3afTnVjXBNi5cKUtMps"
oauth_token = "eyJraWQiOiJJbklUTUFQcDhodG93Q0xrNnlRMXhxeFNSYldoaW5ybVwvQ01pTzJCRCtaND0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJlNTlhOGRkZS03NWMyLTRlMjctYWRlOS1hMzA1MTY0MTVmMzUiLCJjb2duaXRvOmdyb3VwcyI6WyJwcmFjdGl0aW9uZXIiXSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tXC91cy1lYXN0LTFfWTRKVGs1WGdUIiwidmVyc2lvbiI6MiwiY2xpZW50X2lkIjoiNWNkaDduZzI0cWZka290OXJpNDFlYjJlMHYiLCJldmVudF9pZCI6IjAzYWJkMWVkLWY1YjUtNGJmMi04ZGY3LTVmYWYzMjA2MGE2YSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUiLCJhdXRoX3RpbWUiOjE2MTQ3MjA4NTgsImV4cCI6MTYxNDczMTY1OCwiaWF0IjoxNjE0NzIwODU4LCJqdGkiOiI2MDYyMmE2ZS1kNmYzLTRhYzItYjdkYy1kNzlhNjU0YzgzNGYiLCJ1c2VybmFtZSI6Im1hZGhhbi5tYW5vaGFyYW5AaGV4YWdvbi5jb20ifQ.w4F7Tv2PNOHfPCMyU0wGDVfmmB20dYhSCUsnAQS8_lEA33c3fwk8nosymQEMuV-cXb7OV1DOpG2aSLtFQT5qjbxCCxemDWCi71Yg1Yga9I45EKHlnDqKnHvEwoGw9a51XTkaB4nWzs5Cdg1D7JnxZSSebv_0TBhrHUy6I-rs5tHsFnKtcHsuTpitJ8NUGVzBBSd1QAp_B_vv2G35EQFTnwaul_nzlgAT6akeVZOsIZ5tol4pqJnIdNOqeTzu_J5f5rGajShYBg-E3ePGXuUGvFuAmmHwjoOeBdiB26nba5bp2Fh7OPSZERkI28rzHFh9_BiB_UiCqly7XpGB18p0kQ"

# setup the headers
headers = {
    'x-api-key': api_key,
    'Authorization': 'Bearer ' + oauth_token
}

# post the data
result = requests.post(fhir_url, json=data, headers=headers)
print(result)