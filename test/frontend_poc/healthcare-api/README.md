# Healthcare Project Management API - PoC

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).\
This sample demonstrates how to fetch the Healthcare projects and respective FHIR resources.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


# Set variables in environment (.env) file
The .env contains all the API related variables.
Only variable which you have to change is 'REACT_APP_OAUTH_TOKEN'.\
Use Postman to get an OAuth token and use the value in this variable.