import React, { useState, useEffect } from "react";
import FhirContext from "./FhirContext";
import FHIR from "fhirclient";

const FhirProvider = ({ children }) => {
  const [client, setClient] = useState(null);
  const [ready, setReady] = useState(false);
  const [requestHeaders, setRequestHeaders] = useState(null);

  useEffect(() => {
    setClient(FHIR.client(process.env.REACT_APP_FHIR_API));
    setReady(true);
    setRequestHeaders({
      "content-type": "application/json",
      //"x-api-key": process.env.REACT_APP_FHIR_API_KEY,
      //Authorization: process.env.REACT_APP_OAUTH_TOKEN,
      "Authorization": `Basic ${btoa(process.env.REACT_APP_USERNAME + ":" + process.env.REACT_APP_PASSWORD)}`
    });
  }, []);

  return (
    <FhirContext.Provider
      value={{ ready: ready, client: client, requestHeaders: requestHeaders }}
    >
      {children}
    </FhirContext.Provider>
  );
};

export default FhirProvider;
