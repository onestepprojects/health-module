import React from 'react'
import FHIR from 'fhirclient'

const FhirContext = React.createContext(
    {
        ready: false,
        /*client: FHIR.client(process.env.REACT_APP_FHIR_API), 
        requestHeaders: { 
            "content-type": "application/json",
            "x-api-key": process.env.REACT_APP_FHIR_API_KEY,
            "Authorization": process.env.REACT_APP_OAUTH_TOKEN
          }*/
        //HALEY TODO: fix this
        client: FHIR.client(process.env.REACT_APP_FHIR_API),
        requestHeaders: {
            "content-type": "application/json",
            //"Authorization": process.env.REACT_APP_OAUTH_TOKEN,
            "Authorization": `Basic ${btoa(process.env.REACT_APP_USERNAME + ":" + process.env.REACT_APP_PASSWORD)}`
        }
    }
)

export default FhirContext