import React, { Component } from "react";
import FhirContext from "./../context/FhirContext";

class FhirResourceLink extends Component {
  constructor(props) {
    super(props);

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);

    this.state = {
      fhirResourceObject: null,
    };
  }

  async componentDidMount() { }

  async handleClick(e) {
    e.preventDefault();

    /*const fhirContext = this.context;
    const client = fhirContext.client;
    var requestOptions = {
      url:
        this.props.fhirResource.resourceType +
        "/" +
        this.props.fhirResource.resourceId,
      headers: fhirContext.requestHeaders,
    };

    console.log(requestOptions);
    const resourceObject = await client.request(requestOptions);
    const resourceObjectJson = await resourceObject.json();
    console.log(resourceObjectJson);*/

    console.log(`btoa: ${btoa(process.env.REACT_APP_USERNAME + ":" + process.env.REACT_APP_PASSWORD)}`)

    const headers = {
      'Content-type': 'application/json',
      'Authorization': `Basic ${btoa(process.env.REACT_APP_USERNAME + ":" + process.env.REACT_APP_PASSWORD)}`,
    }

    const url = `${process.env.REACT_APP_FHIR_API}/Patient/cbfcdd59-8b4f-419f-aeb2-2d9070230db4`;
    console.log(`url: ${url}`)

    try {
      let resourceObject = await fetch(url, {
        method: 'GET',
        headers: headers,
      });

      let resourceObjectJson = await resourceObject.json();
      console.log(resourceObjectJson);
      this.setState({ fhirResourceObject: JSON.stringify(resourceObjectJson) });
    } catch (e) {
      console.error(e);
    }

  }

  render() {
    return (
      <div>
        <a href="#" onClick={this.handleClick}>
          ResourceType: {this.props.fhirResource.resourceType}, Id:{" "}
          {this.props.fhirResource.resourceId}
        </a>
        <div className="box-container">{this.state.fhirResourceObject}</div>
      </div>
    );
  }
}

FhirResourceLink.contextType = FhirContext;

export default FhirResourceLink;
