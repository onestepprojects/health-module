import React from 'react';
import FhirResourceLink from './fhirResourceLink';

const Projects = ({projects}) => {
    return (
        <div>
            <center><h1>Healthcare Projects</h1></center>
            {projects.map((project, i) => (
                <div key={i} className="card">
                    <div key={i} className="card-body">
                        <h5 key={i} className="card-title">{project.name}</h5>
                        <h6 key={i+1} className="card-subtitle mb-2 text-muted">Id: {project.id}</h6>
                        <p key={i+2} className="card-text">Description: {project.description}</p>
                        <p key={i+3} className="card-text">StartDate: {new Date(project.startDate).toString()}</p>
                        <p key={i+4} className="card-text">EndDate: {new Date(project.endDate).toString()}</p>
                        <div key={i+5}> {project.fhirResources.map((resource, j) => (
                                <div key={j}>
                                    <FhirResourceLink fhirResource={resource} />
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            ))}
        </div>
    )
};

export default Projects;