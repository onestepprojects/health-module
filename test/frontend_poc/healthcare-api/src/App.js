import logo from './logo.svg';
import './App.css';
import React, { Component, useContext } from 'react';
import Projects from './components/projects';
import FHIR from "fhirclient";
import FhirContext from './context/FhirContext';

class App extends Component {

  render() {
    return (
      <Projects projects={this.state.projects} />
    )
  }

  state = {
    projects: []
  }

  async componentDidMount() {

    // fetching from Project API
    /*fetch(`${process.env.REACT_APP_PROJECTS_API}/Patient/cbfcdd59-8b4f-419f-aeb2-2d9070230db4`, {
      headers: {
        //'x-api-key': process.env.REACT_APP_PROJECTS_API_KEY,
        'Authorization': process.env.REACT_APP_OAUTH_TOKEN,
      }
    })*/
    /*fetch(process.env.REACT_APP_PROJECTS_API, { 
        headers: {
          'x-api-key': process.env.REACT_APP_PROJECTS_API_KEY,
          'Authorization': process.env.REACT_APP_OAUTH_TOKEN
          }})
        .then(response => response.json())
        .then((data) => {
          this.setState({ projects: data });
        })
        .catch(console.log)
        */

    console.log(`btoa: ${btoa(process.env.REACT_APP_USERNAME + ":" + process.env.REACT_APP_PASSWORD)}`)

    const headers = {
      'Content-type': 'application/json',
      'Authorization': `Basic ${btoa(process.env.REACT_APP_USERNAME + ":" + process.env.REACT_APP_PASSWORD)}`,
    }

    //HALEY TODO: Change this to pull all projects
    const url = `${process.env.REACT_APP_PROJECTS_API}/a166cb9d-e413-41f5-afdd-632c1c5ffbf4`;
    console.log(`url: ${url}`);

    try {
      let resourceObject = await fetch(url, {
        method: 'GET',
        headers: headers,
      });

      let resourceObjectJson = await resourceObject.json();

      console.log([resourceObjectJson])

      //HALEY TODO: change this from array to just resourceObjectJson
      this.setState({
        projects: [resourceObjectJson]
      })

    } catch (e) {
      console.error(e);
    }

  }
}

App.contextType = FhirContext;

export default App;
