# Module 6: Health Module 
Health Module for the OneStep Relief Platform

## Project Information

* [Introduction](#introduction)
* [Team Members](#team-members)
* [Product Owners](#product-ownsers)
* [Faculty](#faculty)
* [Technical Details](#technical-details)
* [Development Tools](#development-tools)
* [Project Roadmap](#project-outline)


### Introduction

The Health Module constitutes the backbone of all public health-related projects within the Relief Platform. It enables Health Workers to survey and assess the health needs of a community in order to provide targeted health services to individuals and communities in need.

### Team Members

* Eric Ding
* Eric S Moon
* Gustavo Varo
* Madhan Manoharan
* Michaela DeForest
* Oscar Anchondo
* Haley Huang

### Product Owners

* Nitya Timalsina
* Vijay Bhatt

### Faculty
* Annie Kamlang
* Eric Gieseke
* Hannah Riggs

### Agile Coach
* Hannah Riggs

### Technical Details

* Programming language(s)
    * Java
* API

### Development Tools

* [Requirements Document](https://docs.google.com/document/d/1WI-hCE8qT9K_pb4NDFzy0JvX27c1wNoR-6d54VN1QGw/edit)
* [OneStep Relief Platform System Architecture](https://docs.google.com/document/d/1jLGySl6OOGIB6STxu-LDSxUQ1Z6pus0Vp5dqvp4tyLY/edit#heading=h.odpaxmoutbnv)
* [Design Document](https://docs.google.com/document/d/1050KSeyXGI2PqLeY3nh7Zon6tYV0F4_Fa59BsMvPgEE/edit?usp=sharing)
* UML


## UI Development

The frontend portion of this project is contained within the `frontend/` directory. The project is set up as an npm module that can be packaged and pulled in by the OneStep UI Application. This module is also capable of functioning on it's own, in an effort to encourage modularity.

To begin developing the UI, you must have node installed and at least two terminal windows open.

1. The first terminal window will allow you to make changes to the project (under `frontend/src`) and have them reload in the browser window after they are packaged into `frontend/dist/`.
Run `npm start` from the `frontend/` directory and keep this running.

2. The second terminal window will be used for running the App server for the example app (under `frontend/example`). This "example" app pulls in our health module from the generated files as an npm package (see `frontend/example/App.js`), so that you can see the module in the browser. This example app will not be deployed with the module.
Run `npm start` from the `frontend/example` directory and keep this running as well.

All UI work should be done in the `frontend/src` directory. NPM packages can be installed from `frontend/` and used in the module.

Components are organized in the `frontend/src/components` directory by page and tests are located under `frontend/src/tests`. The Module is exported from `frontend/src/index/js` and this is the starting point.




